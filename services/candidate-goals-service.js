onetoone.Services.factory('candidateGoals', function ($http, config, utils) {
    'use strict';

    /**
     * Obtains overview of goals to render goals page
     */
    this.getOverview = function (candidate_program_id) {
        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.GOALS.ROOT + config.URLS.GOALS.OVERVIEW,
            method: 'POST',
            data: {
                candidate_program_id: candidate_program_id
            }
        }));
    };


    /**
     * POSTs new step data
     */
    this.addStep = function (candidateProgramId, stepId) {
        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.GOALS.ROOT + config.URLS.GOALS.ADD_STEP,
            method: 'POST',
            data: {
                candidate_program_id: candidateProgramId,
                step_id: stepId
            }
        }));
    };


    /**
     * Removes step by id
     */
    this.removeStep = function (candidateProgramId, stepId) {
        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.GOALS.ROOT + config.URLS.GOALS.REMOVE_STEP,
            method: 'POST',
            data: {
                candidate_program_id: candidateProgramId,
                step_id: stepId
            }
        }));
    };


    /**
     * Obtains details of current step and action plan list
     */
    this.fetchActionPlan = function (candidateProgramId, stepId) {
        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.GOALS.ROOT + config.URLS.GOALS.FETCH_ACTION_PLAN,
            method: 'POST',
            data: {
                candidate_program_id: candidateProgramId,
                step_id: stepId
            }
        }));
    };

    this.saveStep = function (candidateProgramId, stepId, data) {
        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.GOALS.ROOT + config.URLS.GOALS.SAVE_STEP,
            method: 'POST',
            data: {
                candidate_program_id: candidateProgramId,
                step_id: stepId,
                data: data
            }
        }));
    };

    this.saveActionPlan = function (candidateProgramId, stepId, data) {
        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.GOALS.ROOT + config.URLS.GOALS.SAVE_ACTION_PLAN,
            method: 'POST',
            data: {
                candidate_program_id: candidateProgramId,
                step_id: stepId,
                data: data
            }
        }));
    };

    this.removeActionPlan = function (candidateProgramId, actionPlanId) {
        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.GOALS.ROOT + config.URLS.GOALS.REMOVE_ACTION_PLAN,
            method: 'POST',
            data: {
                candidate_program_id: candidateProgramId,
                action_plan_id: actionPlanId
            }
        }));
    };

    this.setGoalsPriorities = function (candidateProgramId, actionPlanId) {
        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.GOALS.ROOT + config.URLS.GOALS.UPDATE_PRIORITIES,
            method: 'POST',
            data: {
                candidate_program_id: candidateProgramId,
                action_plan_id: actionPlanId
            }
        }));
    };

    /**
     * picks only fields that should be passed to back-end, omitting service fields
     */
    this.cleanStepPresentationData = function (step) {
        return _(step).pick('id', 'name', 'action_plan_description', 'priority', 'difficult_to_do', 'other_comments', 'confidence', 'target_completion_date', 'status');
    };

    return this;
});