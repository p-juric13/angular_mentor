'use strict';

onetoone.Services
  .service('fullcalendarHelper', function (_) {

    var service = {

      init: function (elm, options) {
        //call fullCalendar from an empty html tag, to keep angular happy.
        elm.html('').fullCalendar(options);
      },

      destroy: function (elm) {
        elm.fullCalendar('destroy');
      },

      render: function (elm) {
        elm.fullCalendar('render');
      },

      reset: function (elm) {
        service.removeEvents(elm);
        service.refetchEvents(elm);
      },

      removeEvents: function (elm) {
        elm.fullCalendar('removeEvents');
      },

      refetchEvents: function (elm) {
        elm.fullCalendar('refetchEvents');
      },

      triggerNext: function (elm) {
        elm.fullCalendar('next');
      },

      triggerPrev: function (elm) {
        elm.fullCalendar('prev');
      },

      clientEvents: function (elm) {
        return elm.fullCalendar('clientEvents');
      },
			
			renderEvent: function(elm, event){
				elm.fullCalendar('renderEvent', event);
			},
			
			renderEvents: function(elm, events, otherslot , editable, bgcolor){
				angular.forEach(events, function (event) {
					event.editable = editable;
					event.otherslot = otherslot;
					if(bgcolor){
						event.color  = bgcolor;
					}
					elm.fullCalendar('renderEvent', event);
				});
			},

      highlightAvailableTimeslot: function (elm, event, color) {
				if(color == null){
					color = '#33CC33';
				}
        elm.fullCalendar('select', event.start, event.end, false, color, event.title + '\n' + event.start, true);
      },
			
			/*otherAvailableSlots: function(elm, event, color = null){
				if(color)
				elm.fullCalendar('select', event.start, event.end, false, '#ff7700', event.title + '\n' + event.start, true);
      },*/

      defaultOptions: {
        defaultView: 'agendaWeek',
        defaultEventMinutes: 30,
        minTime: 6,
        maxTime: 23,
        allDaySlot: false,
        allDayDefault: false,
        selectable: true,
        selectHelper: true,
        editable: true,
        eventColor: '#0e88af',
        disableResizing: true,
        availInfo: [],
        header: {
          left: '',
          center: '',
          right: ''
        },
        eventSources: [
          {
            events: [],
            backgroundColor: 'green',
            borderColor: 'green',
            textColor: 'yellow'
          }
        ]
      },

      /**
       * * @TODO test!
       * @param date
       * @param calendarElm
       * @param calendarEvents
       * @returns {boolean}
       */
      dayClickHandler: function (date, calendarElm, calendarEvents) {
        var clickedTime = date.getTime(),
          thirtyMinutes = 1800000,
          validTime = false,
          ambassadorsIds = [];

        angular.forEach(calendarEvents, function (event) {
          if (validTime) return;

          if (clickedTime >= event.start.getTime() && clickedTime < event.end.getTime()) {
            validTime = true;
            ambassadorsIds = ambassadorsIds.concat(event.ambassadorsIds);
          }
        });

        if (validTime === false) return false;

        var eventCountElm = angular.element('#eventCount');
        // update the #eventCount input to ensure new events have unique IDs
        var eventID = eventCountElm.val();
        eventCountElm.attr('value', 1 + (eventID * 1));
        // if there are less than 6 events, add this new one with the following details
        var allevents = calendarElm.fullCalendar('clientEvents');

        if ($(allevents).length < 20) {
          var end = new Date(date.getTime() + thirtyMinutes);//30 mins
          calendarElm.fullCalendar('renderEvent', {
            id: eventID,
            title: 'Selected',
            start: date,
            end: end,
            candidateIds: _.uniq(ambassadorsIds)
          });
        } else {
          alert('Sorry, you cannot add more than 20 time blocks.');
        }
      },
			
			otherAvailableTimeClickHandler: function (date, calendarElm, calendarEvents) {
        var clickedTime = date.getTime(),
          thirtyMinutes = 1800000,
					validTime = false;
				//if (validTime === false) return false;
				
        var eventCountElm = angular.element('#eventCount');
        // update the #eventCount input to ensure new events have unique IDs
        var eventID = eventCountElm.val();
        eventCountElm.attr('value', 1 + (eventID * 1));
        // if there are less than 6 events, add this new one with the following details
        var allevents = calendarElm.fullCalendar('clientEvents');

        var end = new Date(date.getTime() + thirtyMinutes);//30 mins
        calendarElm.fullCalendar('renderEvent', {
          id: eventID,
          title: 'Selected',
          start: date,
          end: end,
					otherslot: true
        });
      },
			/**
       * * @TODO test!
       * @param date
       * @param calendarElm
       * @param calendarEvents
       * @returns {boolean}
       */
      NormaldayClickHandler: function (date, calendarElm) {
        var clickedTime = date.getTime(),
          thirtyMinutes = 1800000,
          ambassadorsIds = [];

        var eventCountElm = angular.element('#eventCount');
        // update the #eventCount input to ensure new events have unique IDs
        var eventID = eventCountElm.val();
        eventCountElm.attr('value', 1 + (eventID * 1));
        // if there are less than 6 events, add this new one with the following details
        var allevents = calendarElm.fullCalendar('clientEvents');

        if ($(allevents).length < 20) {
          var end = new Date(date.getTime() + thirtyMinutes);//30 mins
          calendarElm.fullCalendar('renderEvent', {
            id: eventID,
            title: 'Selected',
            start: date,
            end: end
          });
        } else {
          alert('Sorry, you cannot add more than 20 time blocks.');
        }
      },
			
			EventResizeHandler: function(event,dayDelta,minuteDelta,revertFunc){
        // break down the start time
        var startTimeString = (event.start);
        angular.element('#resizeStart').val(startTimeString);
        var splitStartDate = angular.element('#resizeStart').val().split(' ');
        var startTimeOnly = (splitStartDate[4].indexOf(':') != -1) ? Number(splitStartDate[4].replace(/:/g, '')) : Number(splitStartDate[3].replace(/:/g, '')); // IE's date format is different so time will actually be in array's index #3 instead of #4
        // break down the end time
        var endTimeString = (event.end);
        angular.element('#resizeStart').val(endTimeString);
        var splitEndDate = angular.element('#resizeStart').val().split(' ');
        var endTimeOnly = (splitEndDate[4].indexOf(':') != -1) ? Number(splitEndDate[4].replace(/:/g, '')) : Number(splitEndDate[3].replace(/:/g, '')); // IE's date format is different so time will actually be in array's index #3 instead of #4
        // get time difference and limit resizing to 2 hours
        var timeDiff = (endTimeOnly - startTimeOnly) / 10000;
        if ((timeDiff) > 12) {
          alert('Sorry, time blocks are limited to 12 hours.');
          revertFunc();
        }
        // limit the events from being extended past midnight
        var endTimePosition = endTimeOnly / 10000;
        if ((endTimePosition) > 0 && (endTimePosition) <= 6) {
          revertFunc();
        }
      },
      /**
       * * @TODO test!
       * @param event
       * @param dayDelta
       * @param minuteDelta
       * @param allDay
       * @param revertFunc
       * @param calendarEvents
       */
      eventDropHandler: function (event, dayDelta, minuteDelta, allDay, revertFunc, calendarEvents) {
        // break down the end time
        var endTimeString = (event.end);
        var dragAndDropEnd = angular.element('#dragAndDropEnd');

        dragAndDropEnd.val(endTimeString);
        var splitEndDate = dragAndDropEnd.val().split(' ');
        var endTimeOnly = splitEndDate[4].replace(/:/g, '');

        // limit the events from being dragged past midnight
        var endTimePosition = endTimeOnly / 10000;
        if ((endTimePosition) > 0 && (endTimePosition) <= 6) {
          revertFunc();
        }
        if (typeof service.validateTimeperiod == 'function' && !service.validateTimeperiod(event.start.getTime(), event.end.getTime(), calendarEvents)) {
          revertFunc();
        }
      },

      /**
       * * @TODO test!
       * @param start
       * @param end
       * @param allDay
       * @param ev
       * @param overlayflag
       * @param calendarElm
       */
      selectHandler: function (start, end, allDay, ev, overlayflag, calendarElm) {
        calendarElm.fullCalendar('unselect');
      },
      
      /**
       * * @TODO test!
       * @param event
       * @param calendarElm
       */
      eventMouseoverHandler: function (event, calendarElm) {
        $(this).dblclick(function () {
          calendarElm.fullCalendar('removeEvents', event.id);
        });
      },

      viewDisplay: function (calendarElm, calendarEvents, otherEvents) {
        $('div.fc-cell-overlay', calendarElm).remove();
        // add green background and title to available events
        angular.forEach(calendarEvents, function (event) {
					//alert(event.start+" *** "+event.end);
          service.highlightAvailableTimeslot(calendarElm, event);
        });
        $('div.fc-cell-overlay', calendarElm).tipTip();
				//var newOtherEvents = array();
				if(otherEvents){
					//service.getSeparatedEvents(otherEvents);
					angular.forEach(otherEvents, function (event){
						/*if(calendarEvents){
							for(i=0;i<calendarEvents.length; i++){
								mainevent = calendarEvents[i];
								var starttime = null;
								var endtime = null;
								
								if (event.start.getTime() >= mainevent.start.getTime() && event.start.getTime() < mainevent.end.getTime()) {
									if(event.end.getTime() >= event.end.getTime()){
										starttime = mainevent.end;
										endtime = event.end;
									}
								}else if(event.start.getTime() < mainevent.start.getTime() && event.end.getTime() >= mainevent.start.getTime()){
									starttime = event.start;
									endtime = mainevent.start;
									if( event.end.getTime() > mainevent.end.getTime() ){
										starttime = mainevent.end;
										endtime = event.end;
									}
								}else{
									
								}
							}
						}*/
	          service.highlightAvailableTimeslot(calendarElm, event, '#ff7700');
	        });
				}
      },
				
			getSeparatedEvents: function(events){
				/*var newslots = new Array();
				var startvalue = 0;
				var endvalue = 0;
				var thirtyMinutes = 1800000;
				var slotscount = 0;
				angular.forEach(events, function (event){
					startvalue = event.start.getTime();
					endvalue = event.end.getTime();
					slotscount = (endvalue - startvalue)/thirtyMinutes;
					for(i=0; i<slotscount; i++){
						newslots
					}
				});*/
			}

    };

    return service;
  });