'use strict';

/* Services */

onetoone.Services.factory('candidateFunctions', function ($http, config, utils) {

    /**
     * @param orgId
     */
    this.fetchCandidates = function (orgId) {
        if (!orgId) return;
        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.CANDIDATES.CANDIDATE_DATA,
            method: 'GET',
            params: {
                org_id : orgId,
            }
        }));
    };

	this.fetchCandidateCallHistory = function(candidateId) {
		if(!candidateId) return false;
		return $http(utils.httpParams({
			url: config.ROOT_URL + config.URLS.CANDIDATES.CALL_HISTORY_DATA,
			method: 'GET',
			params: {
				candidate_id: candidateId
			}
		}));
	};

    this.markProgramComplete = function (candidateId) {
        return $http(utils.httpParams({
             url: config.ROOT_URL + config.URLS.CANDIDATES.MARK_PROGRAM_COMPLETE,
             method: 'GET',
             params: {
                 candidate_id: candidateId,
             }
         }));
    };

    this.removeCandidateProgram = function(candidateId) {
    	return $http(utils.httpParams({
             url: config.ROOT_URL + config.URLS.CANDIDATES.REMOVE_CANDIDATE_PROGRAM,
             method: 'GET',
             params: {
                 candidate_id: candidateId,
             }
         }));
    };

    this.callDetails = function (queueId) {
        return $http(utils.httpParams({
             url: config.ROOT_URL + config.URLS.QUEUE.CALL_DETAILS,
             method: 'GET',
             params: {
                 queue_id: queueId,
                 admin_view: 1
             }
         }));
    };

    this.candidateCoreInfo = function(candidateId) {
        return $http(utils.httpParams({
             url: config.ROOT_URL + config.URLS.CANDIDATES.CANDIDATE_CORE_INFO,
             method: 'GET',
             params: {
                 candidate_id: candidateId,
             }
         })); 
    };

    this.getCallDetails = function(candidateId) {
        return $http(utils.httpParams({
             url: config.ROOT_URL + config.URLS.CANDIDATES.CANDIDATE_CALL_DETAILS,
             method: 'GET',
             params: {
                 candidate_id: candidateId,
             }
         })); 
    };

    this.getNotes = function(candidateId) {
        return $http(utils.httpParams({
             url: config.ROOT_URL + config.URLS.CANDIDATES.GET_NOTES,
             method: 'GET',
             params: {
                 candidate_id: candidateId,
             }
         })); 
    };

    this.saveNote = function(candidateId, notes) {
        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.CANDIDATES.SAVE_NOTE,
            method: 'POST',
            data: {
                candidate_id: candidateId,
                notes: notes
            }
        }));
    };

    this.getAvailability = function(candidateId) {
        return $http(utils.httpParams({
             url: config.ROOT_URL + config.URLS.CANDIDATES.CANDIDATE_AVAILABILITY,
             method: 'GET',
             params: {
                 candidate_id: candidateId,
             }
         })); 
    };

    this.saveCandidateInfo = function(candidateAttr) {
        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.CANDIDATES.SAVE_CANDIDATE_INFO,
            method: 'POST',
            data: {
                candidate_info: candidateAttr
            }
        }));
    };

    this.saveAvailability = function(candidateId, slots) {
        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.CANDIDATES.SAVE_CANDIDATE_AVAILABILITY,
            method: 'POST',
            data: {
                candidate_id: candidateId,
                slots: slots
            }
        }));
    }

    return this;

});