'use strict';

/* Services */

onetoone.Services.factory('accountjson', function($http, config, utils) {

    /**
     * calls accountjson/inactiveLogin action
     * @param userId
     */
    this.inactivateLogin = function (userId) {
        if (userId === undefined || !userId) return;

        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.ACCOUNTJSON.ROOT + config.URLS.ACCOUNTJSON.INACTIVATE_LOGIN,
            data: {
                user_id: userId
            }
        }));
    };

    return this;
});