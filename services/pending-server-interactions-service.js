/* Services */
onetoone.Services.factory('pendingServerInteractions',
function($rootScope, $timeout, config) {
    'use strict';

    var registry = {};

    var pendingServerInteractions = {
        /**
         * Adds an interaction that should be performed later to the registry under the
         * given key.
         *
         * If an interaction already existed under this key the old interaction will be
         * overwritten and not executed.
         *
         * @param {string} key The key under which to store the interaction
         * @param {function()} interaction The interaction function to be executed later
         */
        add: function(key, interaction) {
            registry[key] = interaction;
        },

        /**
         * @private
         *
         * Will be called by a watch on focussed state. Executes all pending interactions.
         */
        executeCallbacks: function() {
            _(registry).each(function(value, key, myMap) {
                if (value) {
                    value(key);
                }
            });
            registry = {};

            // setup next launch
            pendingServerInteractions.registerTimeout();
        },

        /**
         * @private
         *
         * executed with set up in config period
         */
        registerTimeout: function () {
            $timeout(function () {
                pendingServerInteractions.executeCallbacks();
            }, config.DELAYED_TASKS_EXECUTION_TIMEOUT);
        }
    };

    pendingServerInteractions.registerTimeout();

    return pendingServerInteractions;
});