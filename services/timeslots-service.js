onetoone.Services.factory('timeslots', function () {

    var service = {
        shift: function (timeslots, shift) {
            return _(angular.copy(timeslots)).map(function (timestamp) {
                return timestamp + shift;
            });
        }
    };

    return service;
});