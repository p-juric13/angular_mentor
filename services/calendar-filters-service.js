/**
 * Defines service to deal with reusable filters for calendar components
 */
onetoone.Services
.factory('calendarFilters', function (_, config) {
    'use strict';

    var service = {
        // returns data to init gender filter with
        getGenderFilter: function () {
            return {
                "id": "gender",
                "name": "Gender",
                "type": "single",
                "tooltip": null,
                "values": [
                    {"id": "M", "value": "M", "num_matches": 0, "green_label": 1, "yellow_label": 0, "red_label": 0},
                    {"id": "F", "value": "F", "num_matches": 0, "green_label": 1, "yellow_label": 0, "red_label": 0}
                ]
            };
        },

        // returns data to init age filter with
        getAgeFilter: function (minAge, maxAge) {
            return {
                "id": "age",
                "name": "Age",
                "type": "range",
                "tooltip": null,
                "values": [minAge, maxAge]
            };
        },

        // checks if age slider is set to default value
        ageRangeIsDefault: function (start, end) {
            return start == config.FILTERS.AGE.MIN && end == config.FILTERS.AGE.MAX;
        },

        //filter step by step by filtering out candidates by filter values arrays and group 'em by filters
        getSelectedCandidatesGroupedByFilters: function (filters, candidates, activeFilters) {
            var selectedCandidatesGroupedByFilters = {};
            angular.forEach(filters, function (filter) {
                selectedCandidatesGroupedByFilters[filter.id] = candidates;

                // goes through each filter
                angular.forEach(activeFilters, function (activeFilter, activeFilterId) {
                    if (filter.id == activeFilterId) return;
                    if (!activeFilter.length) return;

                    // @XXX what's the better way to guess range slider?
                    if (activeFilterId === 'age') {
                        selectedCandidatesGroupedByFilters[filter.id] =
                            _.filter(selectedCandidatesGroupedByFilters[filter.id], function (candidate) {
                                return candidate[activeFilterId] > activeFilter[0] && candidate[activeFilterId] < activeFilter[1];
                            });
                    } else {
                        selectedCandidatesGroupedByFilters[filter.id] =
                            _.filter(selectedCandidatesGroupedByFilters[filter.id], function (candidate) {
                                return !!_.intersection(candidate.values, activeFilter).length;
                            });
                    }
                });
            });
            return selectedCandidatesGroupedByFilters;
        },

        // update matchingCandidatesNumber object to show relevant candidates numbers for filters
        calculateMatches: function (filters, selectedCandidatesGroupedByFilters) {
            var matchingCandidatesNumber = {};
            angular.forEach(filters, function (filter) {
                angular.forEach(filter.values, function (value) {
                    if (filter.type === 'range') {
                        matchingCandidatesNumber[filter.id + '|' + 0] = _.filter(
                            selectedCandidatesGroupedByFilters[filter.id],
                            function (candidate) {
                                return candidate[filter.id] > filter.values[0] && candidate[filter.id] < filter.values[1];
                            }
                        ).length;
                    } else {
                        // here we have to calculate number of candidates that will be added to selection when we check this filter value
                        matchingCandidatesNumber[filter.id + '|' + value.id] = _.filter(
                            // if no filters were checked serve all candidates array
                            selectedCandidatesGroupedByFilters[filter.id],
                            function (candidate) {
                              if (_(candidate.filters_values[filter.id]).isArray()) {
                                return _(candidate.filters_values[filter.id]).contains(value.id);
                              } else {
                                return candidate.filters_values[filter.id] == value.id;
                              }
                            }
                        ).length;
                    }
                });
            });
            return matchingCandidatesNumber;
        },

        // filter candidates array to get only matching candidates
        getSelectedCandidates: function (activeFilters, candidates) {
            var selectedCandidates = candidates;

            angular.forEach(activeFilters, function (activeFilter, activeFilterId) {
                if (!activeFilter.length) return;

                // @XXX what's the better way to guess range slider?
                if (activeFilterId === 'age') {
                    selectedCandidates =
                        _.filter(selectedCandidates, function (candidate) {
                            return candidate[activeFilterId] >= activeFilter[0] && candidate[activeFilterId] <= activeFilter[1];
                        });
                } else {
                    selectedCandidates =
                        _.filter(selectedCandidates, function (candidate) {
                            return !!_.intersection(candidate.values, activeFilter).length;
                        });
                }
            });

            return selectedCandidates;
        },

        filtersAreEmpty: function (activeFilters) {
            var filtersEmpty = true;

            angular.forEach(activeFilters, function (activeFilter, activeFilterId) {
                if (!activeFilter.length || !filtersEmpty) return;

                // @XXX what's the better way to guess range slider?
                if (activeFilterId === 'age' && !service.ageRangeIsDefault(activeFilter[0], activeFilter[1])) {
                    filtersEmpty = false;
                }

                if (activeFilterId !== 'age') {
                  filtersEmpty = false;
                }
            });

            return filtersEmpty;
        },

        initScope: function (scope) {
            angular.extend(scope, {
                hasCalendar: false

                // containers for data to render template
                , filters: []
                , candidates: []

                // to use in html - stores changed checkboxes state
                , activeFiltersDirty: {}

                // contains only properties with true values from activeFiltersDirty object
                , activeFilters: {}
                , activeFiltersValues: []

                // get age settings
                , minAge: config.FILTERS.AGE.MIN
                , maxAge: config.FILTERS.AGE.MAX
            });
        },

        maxFilterValues: function (filters) {
          var maxValue = 0;
          _(filters).each(function (filter) {
            var length = filter.values.length;
            if (length > maxValue) maxValue = length;
          });
          return maxValue;
        }
    };

    return service;
});
