onetoone.Services.factory('queue', function ($http, config, utils) {
  'use strict';

    /**
     *
     */
    this.getData = function (orgId, ambassadorId) {
        if (!orgId) throw 'Expected orgId to be > 0, got :' + orgId;
        if (!ambassadorId) throw 'Expected ambassadorId to be > 0, got :' + ambassadorId;

        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.QUEUE.GET_DATA,
            method: 'GET',
            params: {
                org_id: orgId,
                ambassador_id: ambassadorId
            }
        }));
    };

    /**
     * @param queueId
     * @param page starts with 1
     * @return {Object}
     */
    this.callHistory = function (queueId, ambassadorId, page) {
        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.QUEUE.GET_HISTORY,
            method: 'GET',
            params: {
                per_page: config.NUMBER_OF_HISTORY_ITEMS_IN_CALL_POPUP,
                queue_id: queueId,
                page: page,
                ambassador_id: ambassadorId
            }
        }));
    };

     this.callTransactions = function (ambassadorId, candidateId) {
        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.QUEUE.GET_TRANSACTIONS,
            method: 'GET',
            params: {
                ambassador_id: ambassadorId,
                candidate_id: candidateId
            }
        }));
    };


    this.callInfo = function (queueId) {
         return $http(utils.httpParams({
             url: config.ROOT_URL + config.URLS.QUEUE.CALL_INFO,
             method: 'GET',
             params: {
                 queue_id: queueId
             }
         }));
    };

    this.callDetails = function (queueId) {
        return $http(utils.httpParams({
             url: config.ROOT_URL + config.URLS.QUEUE.CALL_DETAILS,
             method: 'GET',
             params: {
                 queue_id: queueId
             }
         }));
    };

    this.save = function (params) {
        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.QUEUE.SAVE,
            method: 'POST',
            data: params
        }));
    };

    this.saveRescheduledCall = function (params) {
      return $http(utils.httpParams({
        url: config.ROOT_URL + config.URLS.QUEUE.SAVE_RESCHEDULED_CALL,
        method: 'POST',
        data: params
      }));
    };

    this.scheduleNewCall = function (params) {
      return $http(utils.httpParams({
        url: config.ROOT_URL + config.URLS.QUEUE.SCHEDULE_NEW_CALL,
        method: 'POST',
        data: params
      }));
    };

    this.initiateCall = function (queueId) {
         return $http(utils.httpParams({
             url: config.ROOT_URL + config.URLS.QUEUE.INITIATE_CALL,
             method: 'GET',
             params: {
                queue_id: queueId
             }
         }));
    };

    this.markCallSuccess = function(queueId, selectedTopicNextCall) {
        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.QUEUE.MARK_CALL_COMPLETE,
            method: 'POST',
            data: {
                 queue_id: queueId,
                 selected_topic_for_next_call: selectedTopicNextCall
            }
        }));
    };

    this.updatePhone = function (phone, queueId) {
        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.QUEUE.ACTIONS.ROOT + config.URLS.QUEUE.ACTIONS.SAVE_CANDIDATE_PHONE,
            method: 'POST',
            data: {
                phone: phone,
                queue_id: queueId
            }
        }));
    };

    this.getProgramTopics = function (candidateProgramId, candidateProgramNodeId) {
        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.QUEUE.GET_PROGRAM_TOPICS,
            method: 'GET',
            params: {
                candidate_program_id: candidateProgramId,
                candidate_program_node_id: candidateProgramNodeId,
            }
        }));
    }

    this.getTalkingPointsQuestions = function (programNodeId, candidateProgramNodeId) {
        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.QUEUE.GET_TALKINGPOINTS_QUESTIONS,
            method: 'GET',
            params: {
                program_node_id: programNodeId,
                candidate_program_node_id: candidateProgramNodeId
            }
        }));
    }

    this.getMenteeAvailability = function(candidateId, queueId) {
        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.QUEUE.GET_MENTEE_AVAILABILITY,
            method: 'GET',
            params: {
                candidate_id: candidateId,
                queue_id: queueId
            }
        }));
    }

        /**
     * @param queueId
     */
    this.rescheduleCall = function (dateAndTime, queueId) {
        if (!queueId || !dateAndTime) return;

        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.QUEUE.ACTIONS.ROOT + config.URLS.QUEUE.ACTIONS.RESCHEDULE_CALL_BLOCK,
            method: 'POST',
            data: {
                queue_id: queueId,
                date_and_time: dateAndTime
            }
        }));
    };

    this.scheduleNextCallBlock = function (dateAndTime, candidateId, ambassadorId) {
      if (!dateAndTime) return;

      return $http(utils.httpParams({
        url: config.ROOT_URL + config.URLS.QUEUE.ACTIONS.ROOT + config.URLS.QUEUE.ACTIONS.SCHEDULE_NEXT_CALL_BLOCK,
        method: 'POST',
            data: {
                date_and_time: dateAndTime,
                candidate_id: candidateId,
                ambassador_id: ambassadorId
            }
      }));
    };

    this.checkCallLength = function(queueId) {
        if (!queueId) return;

        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.QUEUE.CHECK_CALL_LENGTH,
            method: 'GET',
            params: {
                queue_id: queueId,
            }
        }));
    }

    return this;
});