onetoone.Services.factory('guidance', function ($http, config, utils) {
  'use strict';

    /**
     *
     */
    /*this.getPacketData = function ( ambassadorId) {
        if (!ambassadorId) throw 'Expected ambassadorId to be > 0, got :' + ambassadorId;

        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.GUIDANCE.GET_PACKET_DATA,
            method: 'GET',
            params: {
                ambassador_id: ambassadorId
            }
        }));
    }; */

    this.getNavigation = function ( programHierarchyId) {
        if (!programHierarchyId) throw 'Expected programHierarchyId to be > 0, got :' + programHierarchyId;

        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.GUIDANCE.GET_NAVIGATION_DATA,
            method: 'GET',
            params: {
                program_hierarchy_id: programHierarchyId
            }
        }));
    };

    this.getSubcategoryData = function (ambassadorId, categoryId) {
        if (!categoryId) throw 'Expected categoryId to be > 0, got :' + categoryId;

        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.GUIDANCE.GET_SUBCATEGORY_DATA,
            method: 'GET',
            params: {
                ambassador_id: ambassadorId,
                category_id: categoryId
            }
        }));

    };

    this.getContentSummaryData = function (subcatId, ambassadorId) {
        if (!subcatId) throw 'Expected subcatId to be > 0, got :' + subcatId;

        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.GUIDANCE.GET_CONTENTSUMMARY_DATA,
            method: 'GET',
            params: {
                subcategory_id: subcatId,
                ambassador_id: ambassadorId
            }
        }));
    }

    this.getContentItemDetails = function(contentType, contentId) {
        if (!contentId || !contentType) throw 'Expected type & id';

        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.GUIDANCE.GET_CONTENTDETAIL_DATA,
            method: 'GET',
            params: {
                content_type: contentType,
                content_id: contentId
            }
        }));
    }

    this.updContentStatus = function(contentStatusId, contentStatus) {
        if (!contentStatusId || !contentStatus) throw 'Expected type & id to update';

        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.GUIDANCE.SET_CONTENTSTATUS,
            method: 'POST',
            data: {
                content_status_id: contentStatusId,
                content_status: contentStatus
            }
        }));
    }

    this.insContentStatus = function(ambassadorId, contentId, contentStatus) {
        if (!ambassadorId || !contentId || !contentStatus) throw 'Expected id & status';

        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.GUIDANCE.SET_CONTENTSTATUS,
            method: 'POST',
            data: {
                ambassador_id: ambassadorId,
                content_id: contentId,
                content_status: contentStatus
            }
        }));
    }

    this.getSlideData = function(moduleId) {
        if (!moduleId) throw 'Expected module id';

        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.GUIDANCE.GET_SLIDEMODULEDATA,
            method: 'GET',
            params: {
                module_id: moduleId,
            }
        }));
    }

    this.getSlideTemplates = function() {

        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.GUIDANCE.GET_SLIDETEMPLATEDATA,
            method: 'GET',
        }));
    }

    this.getFlipcardData = function(flipcardSetId) {

        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.GUIDANCE.GET_FLIPCARDDATA,
            method: 'GET',
            params: {
                flipcard_set_id: flipcardSetId
            }
        }));
    }

    this.getCandidateRelevantData = function(candidateProgramId, ambassadorId) {
        // if (!candidateProgramId) throw 'Expected candidate program id';

        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.GUIDANCE.GET_CANDIDATERELEVANT_DATA,
            method: 'GET',
            params: {
                candidate_program_id: candidateProgramId,
                ambassador_id: ambassadorId
            }
        }));
    };

    this.getRecommendedContent = function(ambassadorId) {
        if(!ambassadorId) throw 'Expected mentor ID';

        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.GUIDANCE.GET_RECOMMENDEDCONTENT,
            method: 'GET',
            params: {
                ambassador_id: ambassadorId
            }
        }));
    };

    this.getContentTypeSubcatId = function(contentId) {
        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.GUIDANCE.GET_CONTENTTYPESUBCATID,
            method: 'GET',
            params: {
                content_id: contentId
            }
        }));
    };

    return this;
});