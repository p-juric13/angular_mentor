'use strict';

/* Services */

onetoone.Services.factory('ambassadorFunctions', function ($http, config, utils) {

    /**
     * @param orgId
     */
    this.fetchAmbassadors = function (orgId) {
        if (!orgId) return;
        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.AMBASSADORS.AMBASSADORS_DATA,
            method: 'GET',
            params: {
                org_id : orgId,
            }
        }));
    };

    return this;

});