onetoone.Services.factory('scheduleAPI', function ($http, config, utils) {
		
    /**
     * calls schedule/typejson action
     * @param org_id
     */
	this.getOrgTypes=function(ord_uri){
		if(!ord_uri) throw 'Expected org_uri :' + org_id;
		return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.SCHEDULE.GET_TYPE_DATA+ord_uri,
            method: 'GET'
        }));
	}
    /**
     * calls getscheduledata/filteringdata.json action
     * @param org_id
     * @param candidate_id
     */
    this.getFiltersData = function (org_id, candidate_id, root_id, type_id, extended) {
        if (!org_id) throw 'Expected org_id to be > 0, got :' + org_id;
        if (!root_id) throw 'Expected root_id to be > 0, got :' + root_id;
        if (!type_id) throw 'Expected type_id to be > 0, got :' + type_id;
        if (!candidate_id) throw 'Expected candidate_id to be > 0, got :' + candidate_id;

        var params = {
            org_id: org_id,
            root_id: root_id,
            type_id: type_id,
            candidate_id: candidate_id
        };

        //if (extended === true) params.extended = 1;

        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.SCHEDULE.GET_FILTERS,
            method: 'GET',
            params: params
        }));
    };
		
		/**
     * calls getscheduledata/filteringdata.json action
     * @param org_id
     */
    this.getPreviewMatchFilters = function (org_id, root_id, type_id) {
        if (!org_id) throw 'Expected org_id to be > 0, got :' + org_id;
        if (!root_id) throw 'Expected root_id to be > 0, got :' + root_id;
        if (!type_id) throw 'Expected type_id to be > 0, got :' + type_id;

        var params = {
            org_id: org_id,
            root_id: root_id,
            type_id: type_id
        };

        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.SCHEDULE.GET_FILTERS,
            method: 'GET',
            params: params
        }));
    };
		
    /**
     * calls getscheduledata/schedulingcriteria.json
     * for filter values selected on previous step
     * @param num_filters
     * @param candidate_id
     * @return {*}
     */
    this.getSelectedFilters = function (num_filters, candidate_id) {
        if (!num_filters) num_filters = 0;//throw 'Expected num_filters to be > 0, got :' + num_filters;
        if (!candidate_id) throw 'Expected candidate_id to be > 0, got :' + candidate_id;

        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.SCHEDULE.GET_SELECTED_FILTERS,
            method: 'GET',
            params: {
                num_filters: num_filters,
                candidate_id: candidate_id
            }
        }));
    };

    /**
     * obtains timeslots_slice and ambassadors_per_slice data
     * @param candidate_id
     * @return {*}
     */
    this.getSchedulingData = function (candidate_id, type_id, org_id) {
        if (!candidate_id) throw 'Expected candidate_id to be > 0, got :' + candidate_id;
        if (!type_id) throw 'Expected type_id to be > 0, got :' + type_id;
        if (!org_id) throw 'Expected org_id to be > 0, got :' + org_id;

        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.SCHEDULE.GET_DATA,
            method: 'GET',
            params: {
                candidate_id: candidate_id,
                type_id: type_id,
                org_id: org_id
            }
        }));
    };

    return this;
});