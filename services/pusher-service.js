'use strict';


//<script src="https://d3dy5gmtp8yhk7.cloudfront.net/2.0/pusher.min.js" type="text/javascript"></script>

/**
 * This service can use only one Pusher channel.
 * Seems that's enought for now
 */
onetoone.Services
  .service('pusher', function (config) {

    /*
    // Enable pusher logging - @TODO don't include this in production
    Pusher.log = function (message) {
      if (window.console && window.console.log) window.console.log(message);
    };

    // Flash fallback logging - don't include this in production
    window.WEB_SOCKET_DEBUG = true;
    */

    var pusher = new Pusher(config.PUSHER.KEY);
    var channel = null;

    var service = {

      subscribe: function (channelId) {
        channel = pusher.subscribe(channelId);
      },

      bind: function (event, listener) {
        if (!channel) throw 'In order to bind(), you have to subscribe() first.';
        channel.bind(event, listener);
      }

    };

    return service;
  });