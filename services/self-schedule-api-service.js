onetoone.Services.factory('selfScheduleAPI', function ($http, config, utils) {

    /**
     * calls getscheduledata/filteringdata.json action
     * @param org_id
     * @param candidate_id
     */
    this.getFiltersData = function (org_id, root_id, amb_type_id) {
        if (!org_id) throw 'Expected org_id to be > 0, got :' + org_id;
        if (!root_id) throw 'Expected root_id to be > 0, got :' + root_id;
        if (!amb_type_id) throw 'Expected type_id to be > 0, got :' + type_id;

        var params = {
            org_id: org_id,
            root_id: root_id,
            amb_type_id: amb_type_id
        };

        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.SELF_SCHEDULE.GET_FILTERS,
            method: 'GET',
            params: params
        }));
    };

    /**
     * obtains timeslots_slice and ambassadors_per_slice data
     * @param candidate_id
     * @return {*}
     */
    this.getSchedulingData = function (org_id, root_id, type_id, candidate_id, offset) {
        if (!org_id) throw 'Expected org_id to be > 0, got :' + org_id;
        if (!root_id) throw 'Expected root_id to be > 0, got :' + root_id;
        if (!type_id) throw 'Expected type_id to be > 0, got :' + type_id;
        offset = offset || 1;

        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.SELF_SCHEDULE.GET_DATA,
            method: 'GET',
            params: {
                org_id: org_id,
                root_id: root_id,
                type_id: type_id,
                candidate_id: candidate_id,
                offset: offset
            }
        }));
    };

    return this;
});