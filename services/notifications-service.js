onetoone.Services
  .service('notifications', function ($rootScope) {
    if (!$rootScope.notifications) {
      $rootScope.notifications = [];
    }

    $rootScope.$on('$routeChangeStart', function () {
      $rootScope.notifications = [];
    });

    this.add = function (settings) {
      var index = $rootScope.notifications.length;

      var baseNotification = {
          index: index,
          severity: 'alert',
          showClose: settings.showClose !== false,
          isTemplateUrl: !!settings.templateUrl,
          display: true,
          forceOne: false
        };

      $rootScope.notifications[index] = angular.extend(baseNotification, settings);

      // hide other notifications if this forceOne property is true
      if ($rootScope.notifications[index].forceOne) {
        _($rootScope.notifications).each(function (notification) {
          if (notification.index == index) return;
          notification.display = false;
        });
      }

      return index;
    };

    this.hide = function (index) {
      if (typeof index == 'undefined') {
        return;
      }

      $rootScope.notifications[index].display = false;
    };

    this.reset = function () {
      $rootScope.notifications = [];
    };
  });

onetoone.Controllers
  .controller('notificationController', function ($scope, $rootScope, notifications) {
    $scope.getClass = function(notification) { 
      return {
        'alert-box': true, 
        'radius': true,
        'success': notification.severity === 'success', 
        'alert': notification.severity === 'error'
      };
    }; 
    $scope.notifications = $rootScope.notifications;
    $scope.hide = notifications.hide;
  });
