onetoone.Services.factory('outreach', function ($http, config, utils) {
  'use strict';

    /**
     *
     */
    this.getData = function (ambassadorId) {
        if (!ambassadorId) throw 'Expected ambassadorId to be > 0, got :' + ambassadorId;

        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.OUTREACH.GET_DATA,
            method: 'GET',
            params: {
                ambassador_id: ambassadorId
            }
        }));
    };

    this.callInfo = function(prospectiveMenteeId) {
        if (!prospectiveMenteeId) throw 'Expected prospectiveMenteeId to be > 0, got :' + prospectiveMenteeId;

        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.OUTREACH.GET_PROSPECTIVE_MENTEE_DATA,
            method: 'GET',
            params: {
                prospective_mentee_id: prospectiveMenteeId
            }
        }));

    }

    this.createInmemCall = function(listId) {

        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.OUTREACH.CREATE_INMEM_CALL,
            method: 'GET',
            params: {
                peer_coordinator_list_id: listId
            }
        }));
    }

    this.initiateCall = function(listId, phoneNumInd) {
        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.OUTREACH.INITIATE_CALL,
            method: 'GET',
            params: {
                peer_coordinator_list_id: listId,
                phone_num_ind: phoneNumInd
            }
        }));
    }

    this.incrementMissedCallBlock = function(prospectiveMenteeId, phoneNumInd, listId) {
        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.OUTREACH.INCREMENT_MISSED_ATTEMPT,
            method: 'GET',
            params: {
                prospective_mentee_id: prospectiveMenteeId,
                phone_num_ind: phoneNumInd,
                peer_coordinator_list_id: listId
            }
        }));
    }

    this.fetchNotes = function(listId) {
        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.OUTREACH.FETCH_NOTES,
            method: 'GET',
            params: {
                peer_coordinator_list_id: listId,
            }
        }));
    }

    this.saveNotes = function(listId, notesArray) {
        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.OUTREACH.SAVE_NOTES,
            method: 'POST',
            data: {
                peer_coordinator_list_id: listId,
                notes: notesArray,
            }
        }));
    }

    this.deleteNote = function(noteId) {
        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.OUTREACH.DELETE_NOTE,
            method: 'POST',
            data: {
                note_id: noteId
            }
        }));
    }

    this.getOutreachTx = function(listId, numAttempts) {
        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.OUTREACH.GET_OUTREACH_TX,
            method: 'GET',
            params: {
                peer_coordinator_list_id: listId,
                num_attempts: numAttempts
            }
        }));
    }

    this.updateOutreachTxStatus = function(listId, statusInd) {
        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.OUTREACH.UPDATE_OUTREACH_TX_STATUS,
            method: 'POST',
            data: {
                peer_coordinator_list_id: listId,
                status: statusInd
            }
        }));

    }

    this.markComplete = function(listId) {
        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.OUTREACH.MARK_OUTREACH_COMPLETE,
            method: 'POST',
            data: {
                peer_coordinator_list_id: listId
            }
        }));

    }

    
    
    return this;
});