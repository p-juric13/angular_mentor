'use strict';

/* Services */

onetoone.Services.factory('queueFunctions', function ($http, config, utils) {

    /**
     * @param queueId
     */
    this.nextBestTime = function (queueId) {
        if (!queueId) return;

        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.QUEUE.ACTIONS.ROOT + config.URLS.QUEUE.ACTIONS.NEXT_BEST_TIME,
            urlParams: {
                queue_id: queueId
            },
            data: {
                id: queueId
            }
        }));
    };

    /**
     * @param queueId
     */
    this.connectedCall = function (queueId) {
        if (!queueId) return;

        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.QUEUE.ACTIONS.ROOT + config.URLS.QUEUE.ACTIONS.CONNECTED_CALL,
            urlParams: {
                queue_id: queueId
            },
            data: {
                id: queueId
            }
        }));
    };

    /**
     * @param queueId
     */
    // this.successfulCall = function (queueId) {
    //     if (!queueId) return;

    //     return $http(utils.httpParams({
    //         url: config.ROOT_URL + config.URLS.QUEUE.ACTIONS.ROOT + config.URLS.QUEUE.ACTIONS.SUCCESSFUL_CALL,
    //         urlParams: {
    //             queue_id: queueId
    //         },
    //         data: {
    //             id: queueId
    //         }
    //     }));
    // };
    
    /**
     * @param queueId
     */
    this.rescheduleCall = function (timeslotId, queueId) {
        if (!queueId) return;

        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.QUEUE.ACTIONS.ROOT + config.URLS.QUEUE.ACTIONS.RESCHEDULE_CALL,
            urlParams: {
                queue_id: queueId
            },
            data: {
                queue_id: queueId,
                timeslot_id: timeslotId
            }
        }));
    };

    /**
     * @param queueId
     */
    this.requeueCall = function (queueId) {
        if (!queueId) return;

        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.QUEUE.ACTIONS.ROOT + config.URLS.QUEUE.ACTIONS.REQUEUE_CALL,
            urlParams: {
                queue_id: queueId
            },
            data: {
                id: queueId
            }
        }));
    };

    /**
     * @param queueId
     */
    this.logIncompleteCallTransaction = function (queueId) {
        if (!queueId) return;

        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.QUEUE.ACTIONS.ROOT + config.URLS.QUEUE.ACTIONS.LOG_INCOMPLETE_CALL_TRANSACTION,
            urlParams: {
                queue_id: queueId
            },
            data: {
                id: queueId
            }
        }));
    };

    /**
     * @param id
     */
    this.createInmemCall = function (id) {
        if (!id) return;

        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.QUEUE.ACTIONS.ROOT + config.URLS.QUEUE.ACTIONS.CREATE_INMEM_CALL,
            urlParams: {
                id: id
            },
            data: {
                id: id
            }
        }));
    };

    return this;
});