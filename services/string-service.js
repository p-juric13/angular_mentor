onetoone.Services.factory('string', function (config) {

    /**
     * splits string by delimiter set in config
     */
    this.split = function (string) {
        return string.split(config.STRING_DELIMITER);
    };
    this.splitextra = function (string) {
        var splitterlist = [config.STRING_DELIMITER, config.STRING_BLD_HD_DELIMITER, config.STRING_SUB_HD_DELIMITER, config.STRING_BULLET_DELIMITER, config.STRING_HINT_DELIMITER];
        var result = [];

        function sortNumber (a, b) {
            return a - b;
        }

        function getresult (str, splitterlist) {
            if (str != "") {
                var indexarray = [];
                var org_indexarray = [];
                var minvindex = 0;
                var minsplitter = "";
                for (var i = 0; i < splitterlist.length; i++) {
                    indexarray[i] = str.indexOf(splitterlist[i]);
                    org_indexarray[i] = str.indexOf(splitterlist[i]);
                }

                indexarray.sort(sortNumber);
                for (var i = 0; i < indexarray.length; i++) {
                    if (indexarray[i] != -1) {
                        minindex = indexarray[i];
                        minsplitter = splitterlist[org_indexarray.indexOf(indexarray[i])];
                        firstpart = str.substr(0, minindex);
                        if (result[result.length - 1] == undefined) {
                            if (firstpart != "")
                                result.push(firstpart);
                            else
                                result.push(minsplitter);
                        } else {
                            result[result.length - 1] += firstpart;
                        }
                        secondpart = str.substr(minindex + minsplitter.length, str.length - minindex);
                        if (firstpart != "")
                            result.push(minsplitter);
                        getresult(secondpart, splitterlist);
                        break;
                    } else if (i == indexarray.length - 1) {
                        if (result[result.length - 1] == undefined)
                            result.push(str);
                        else
                            result[result.length - 1] += str;
                    }
                }
            }
        }

        function get_object (str, splitterlist) {
            for (var i = 0; i < splitterlist.length; i++) {
                if (_.isFunction(str.substr) && str.substr(0, splitterlist[i].length) == splitterlist[i]) {
                    var obj = {};
                    obj.type = splitterlist[i];
                    obj.text = str.substr(splitterlist[i].length);
                    return obj;
                }
            }
            obj = {};
            obj.type = config.STRING_DELIMITER;
            obj.text = str;
            return obj;
        }

        function get_objectarray (strarray, splitterlist) {
            var resultarray = [];
            for (var i = 0; i < strarray.length; i++) {
                resultarray.push(get_object(strarray[i], splitterlist));
            }
            return resultarray;
        }

        getresult(string, splitterlist);
        object_result = get_objectarray(result, splitterlist);
        return object_result;
    };
    return this;
});