/**
 * Defines service to deal with reusable calendars components
 */
onetoone.Services.factory('calendar', function () {
    'use strict';

    var service = {
        // define if all of timeslots included are available
        validateTimeperiod: function (startTime, endTime, events) {
            var thirtyMinutes = 1800000,
                currentTime = startTime,
                validSelection = true,
                validTime;

            while (currentTime < endTime) {
                validTime = false;

                // check if current timeslot belongs to any event
                angular.forEach(events, function (event) {
                    if (validTime) return;

                    var sliceStart = currentTime + 1,
                        sliceEnd = currentTime + thirtyMinutes - 1;
                    if (sliceStart > event.start.getTime() && sliceEnd < event.end.getTime()) {
                        validTime = true;
                    }
                });

                validSelection = validTime;
                if (!validSelection) break;
                currentTime += thirtyMinutes;
            }

            return validSelection;
        },

        /**
         * returns array of calendar events
         * @param matchingCandidates
         * @param ambassadorAvailability
         * @param timeslotsSlice
         * @return {Array}
         */
        generateEvents: function (matchingCandidates, ambassadorAvailability, timeslotsSlice) {
            var calendarEvents = [],
                timeslotsMatches = {};

            // TO DO: Receive each candidate and plop in the age
            angular.forEach(matchingCandidates, function (candidate) {
                angular.forEach(ambassadorAvailability[candidate.id], function (timeslotId) {
                    if (timeslotsMatches.hasOwnProperty(timeslotId)) {
                        // create new object
                        timeslotsMatches[timeslotId].ambassadors += 1;
                        timeslotsMatches[timeslotId].ambassadorsIds.push(candidate.id);
                    } else {
                        // extend existing object
                        timeslotsMatches[timeslotId] = {
                            ambassadors: 1,
                            ambassadorsIds: [candidate.id],
                            start: new Date(timeslotsSlice[timeslotId]),
                            end: new Date(timeslotsSlice[timeslotId] + 1800000)
                        };
                    }
                });
            });

            // TO DO: Use title for tooltip text
            // rawEvent.ambassadors = number of ambassadors that matches this timeslice
            angular.forEach(timeslotsMatches, function (rawEvent) {
                calendarEvents.push({
                        title: rawEvent.ambassadors + ' ambassador(s)' + (rawEvent.ambassadors == 1 ? '' : 's') + ' available<br />',
                        start: rawEvent.start,
                        end: rawEvent.end,
                        ambassadorsIds: rawEvent.ambassadorsIds
                    });
            });

            return calendarEvents;
        },

        /**
         * @TODO test!
         * looks for overlapping slots
         * @return {Boolean}
         */
        overlappingSlotsExist: function () {
            // Checking for overlapping slots
            var overlappingSlots = false;
            $('input[name="slots[]"]').each(function (i, i_element) { // Outer loops through slots
                var i_slot_data = $(i_element).attr("value").split(',');
                
                $('input[name="slots[]"]').each(function (j, j_element) { // Inner loops through slots
                    var j_slot_data = $(j_element).attr("value").split(',');
                    if (i_slot_data[0] == j_slot_data[0] && i != j) {
                        // Builds Date objects for start/end times of current inner and outer slots
                        var i_slot_start = new Date("09/18/1986 " + i_slot_data[1]);
                        var i_slot_end = new Date("09/18/1986 " + i_slot_data[2]);
                        var j_slot_start = new Date("09/18/1986 " + j_slot_data[1]);
                        var j_slot_end = new Date("09/18/1986 " + j_slot_data[2]);
                        // If start time of inner loop's slot is between the start & end time of the outer loop's slot, they overlap
                        if (j_slot_start >= i_slot_start && j_slot_start < i_slot_end)
                            overlappingSlots = true;
                        // If end time of inner loop's slot is between the start & end time of the outer loop's slot, they also overlap
                        else if (j_slot_end > i_slot_start && j_slot_end <= i_slot_end)
                            overlappingSlots = true;
                    }
                });
            });
            return overlappingSlots;
        },

      /**
       * @TODO test!
       * @param slotObjects
       * @param activeFilters
       * @param includeDate
       * @returns {{dates: Array, slots: Array, ambassadors: Array, filters: Array}}
       */
        populateSlotsAndAmbassadors: function (slotObjects, activeFilters, includeDate) {
            var result = {
                dates: [],
                slots: [],
                ambassadors: [],
                filters: [],
				otherslots: [],
				otherdates: []
            };

            _(slotObjects).each(function (item) {
				  var startDateMoment = moment(item.start);
                  var endDateMoment = moment(item.end);
                  // Retrieving slot's weekday and start time
                  var selWeekday = item.start.getDay();
                  var start = startDateMoment.format('HH:mm');
                  // If slot does not have an end time, make up one (half an hour after start time) and skip to next iteration
                  var end =
                      (item.end ? endDateMoment : moment(startDateMoment).add('minutes', 30))
                          .format('HH:mm');
					if(item.otherslot) {
                        // format dates to display
                        result.otherdates.push(startDateMoment.format('dddd, ' + (includeDate ? 'MMMM D, ' : '') + 'h:mm A') + " - " + endDateMoment.format('h:mm A'));

                        // Creating input field
                        result.otherslots.push(selWeekday + ',' + startDateMoment.format('MM-DD-YYYY')
                        + ',' + start + ',' + end + ',' + endDateMoment.format('A'));
                    } else {
						// format dates to display
                        result.dates.push(startDateMoment.format('dddd, ' + (includeDate ? 'MMMM D, ' : '') + 'h:mm A') + " - " + endDateMoment.format('h:mm A'));

                        // Creating input field
                        result.slots.push(selWeekday + ',' + startDateMoment.format('MM-DD-YYYY')
                            + ',' + start + ',' + end + ',' + endDateMoment.format('A'));

                        result.ambassadors.push(item.candidateIds.join(','));
				    }
            });

            result.filters = _(activeFilters).map(function (value, key) {
                return key + ':' + value.join(',');
            });

            return result;
        },
				
		populateSlots: function (slotObjects, activeFilters, includeDate) {
            var result = {
                dates: [],
                slots: [],
                ambassadors: [],
                filters: []
            };

            _(slotObjects).each(function (item) {
                var startDateMoment = moment(item.start);
                var endDateMoment = moment(item.end);
                // Retrieving slot's weekday and start time
                var selWeekday = item.start.getDay();
                var start = startDateMoment.format('HH:mm');

                // If slot does not have an end time, make up one (half an hour after start time) and skip to next iteration
                var end =
                    (item.end ? endDateMoment : moment(startDateMoment).add('minutes', 30))
                        .format('HH:mm');

                // format dates to display
                result.dates.push(startDateMoment.format('dddd, ' + (includeDate ? 'MMMM D, ' : '') + 'h:mm A') + " - " + endDateMoment.format('h:mm A'));

                // Creating input field
                result.slots.push(selWeekday + ',' + startDateMoment.format('MM-DD-YYYY')
                    + ',' + start + ',' + end + ',' + endDateMoment.format('A'));

                //result.ambassadors.push(item.candidateIds.join(','));
            });

            result.filters = _(activeFilters).map(function (value, key) {
                return key + ':' + value.join(',');
            });

            return result;
        }
    };

    return service;
});
