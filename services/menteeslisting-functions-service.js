'use strict';

/* Services */

onetoone.Services.factory('menteeFunctions', function ($http, config, utils) {

    this.getMenteeNames = function (ambassadorId) {
        if (!ambassadorId) throw 'Expected ambassadorId to be > 0, got :' + ambassadorId;

        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.MENTEESLIST.GET_MENTEE_NAMES,
            method: 'GET',
            params: {
                ambassador_id: ambassadorId
            }
        }));
    };

    this.getCoreMenteeInfo = function(candidateId) {
        if (!candidateId) throw 'Expected candidateId to be > 0, got :' + candidateId;

        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.MENTEESLIST.GET_MENTEE_CORE_INFO,
            method: 'GET',
            params: {
                candidate_id: candidateId
            }
        }));
    };

    this.getMenteeActionPlanInfo = function(candidateId) {
        if (!candidateId) throw 'Expected candidateId to be > 0, got :' + candidateId;

        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.MENTEESLIST.GET_MENTEE_ACTION_PLAN,
            method: 'GET',
            params: {
                candidate_id: candidateId
            }
        }));
    };

    this.getSummaryOfCalls = function(candidateId, ambassadorId) {
        if (!candidateId) throw 'Expected candidateId to be > 0, got :' + candidateId;

        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.MENTEESLIST.GET_MENTEE_SUMMARY_CALLS,
            method: 'GET',
            params: {
                candidate_id: candidateId,
                ambassador_id: ambassadorId
            }
        }));
    }

    this.getSummaryOfTx = function(candidateId, ambassadorId) {
        if (!candidateId) throw 'Expected candidateId to be > 0, got :' + candidateId;

        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.MENTEESLIST.GET_MENTEE_SUMMARY_TX,
            method: 'GET',
            params: {
                candidate_id: candidateId,
                ambassador_id: ambassadorId
            }
        }));
    }

    this.getAtAGlance = function(candidateId) {
        if (!candidateId) throw 'Expected candidateId to be > 0, got :' + candidateId;

        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.MENTEESLIST.GET_AT_A_GLANCE,
            method: 'GET',
            params: {
                candidate_id: candidateId
            }
        })); 
    }

    this.actionPlanDetails = function(actionPlanStepId) {
        if (!actionPlanStepId) throw 'Expected actionPlanStepId to be > 0, got :' + actionPlanStepId;

        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.MENTEESLIST.GET_ACTION_PLAN_INFO,
            method: 'GET',
            params: {
                step_id: actionPlanStepId
            }
        })); 
    }

    this.buildTaskList = function(ambassadorId) {
        if (!ambassadorId) throw 'Expected ambassadorId to be > 0, got :' + ambassadorId;

        return $http(utils.httpParams({
            url: config.ROOT_URL + config.URLS.MENTEESLIST.GET_MENTEE_TASK_LIST,
            method: 'GET',
            params: {
                ambassador_id: ambassadorId
            }
        })); 
        
    }


    return this;
});