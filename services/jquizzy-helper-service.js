'use strict';

onetoone.Services
  .service('jquizzyHelper', function (_) {

    var service = {
      init: function (elm, options) {
        //call jQuizzy from an empty html tag, to keep angular happy.
        elm.html('').jquizzy(options);
      }
    };

    return service;
  });