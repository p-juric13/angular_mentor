'use strict';

onetoone.Services
  .service('fancybox', function () {

    var defaults = {
        padding: 0,
        margin: 20,
        autoScale: false,
        autoDimensions: false,
        fitToView: false,
        scrolling: 'no',
        height: 'auto',
        autoCenter: false
    };

    return {

      // triggers fancybox popup
      open: function (href, options) {

        $('<a href="' + href + '"></a>')
          .fancybox(_(_(defaults).clone()).extend(options))
          .trigger('click');
      },

      close: $.fancybox.close

    };

  });