onetoone.Services.factory('timezones', function (config, $rootScope, $http, $q) {

  var timezonesList;

    var service = {
        list: function () {
          if (timezonesList) {
            return timezonesList;
          } else {

            var result = $q.defer();
            $http.get(config.ROOT_URL + config.URLS.TIMEZONES).success(function (response) {
              timezonesList = response;
              result.resolve(response);
            });
            $rootScope.$broadcast('timezones.Ready');
            return result.promise;
          }
        },

        getDefault: function () {
          return _(timezonesList).find(function (item) {
            return item.is_default == 1;
          });
        },

        offset: function () {
            return -(new Date().getTimezoneOffset() / 60);
        },

        findByOffset: function (offset) {
          return _(config.TIMEZONES).find(function (item) {
            return item.offset == offset;
          })
        }
    };

    service.list();

    return service;
});