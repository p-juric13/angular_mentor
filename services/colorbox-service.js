'use strict';

onetoone.Services
  .service('colorbox', function () {

    var defaults = { inline: true };

    return {

      // triggers colorbox popup
      open: function (options) {
        $.colorbox(_(_(defaults).clone()).extend(options));
      },

      /**
       * This method initiates the close sequence,
       * which does not immediately complete.
       * The lightbox will be completely closed
       * only when the cbox_closed event / onClosed callback is fired.
       */
      close: $.colorbox.close,

      /**
       * This allows Colorbox to be resized
       * based on it's own auto-calculations,
       * or to a specific size.
       * This must be called manually after Colorbox's content has loaded.
       * The optional parameters object can accept width or innerWidth
       * and height or innerHeight.
       * Without specifying a width or height,
       * Colorbox will attempt to recalculate the height of it's current content.
       */
      resize: $.colorbox.resize

    };
  });