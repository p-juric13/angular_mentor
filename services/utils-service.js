'use strict';

/* Services */

onetoone.Services.factory('utils', function() {

    /**
     * parses parameters to string
     * parameters need to be used in string like ':parameter'
     * @example: utils.parseString('http://site/:username/', {username: 'demo'}) => 'http://site/demo'
     *
     * @param string
     * @param parameters
     * @return {*}
     */
    this.parseString = function (string, parameters) {
        for (var index in parameters) {
            if (!parameters.hasOwnProperty(index)) continue;

            string = string.replace(':' + index, parameters[index])
        }

        return string;
    };

    /**
     * normalizes data to be passed as config to $http service
     *
     * @param params {object}
     * @return {object} $http config
     */
    this.httpParams = function (params) {
        var utils = this;

        if (!params.hasOwnProperty('url') || !params.url.length) throw 'Expected to receive url in params object. Instead got null.';

        // populare url with urlParams object
        if (params.hasOwnProperty('urlParams')) {
            params.url = utils.parseString(params.url, params.urlParams);

            params.urlParams = null;
            delete params.urlParams;
        }

        // transform data to be posted like form object to backend (otherwise JSON is posted)
        if (params.hasOwnProperty('data')) {
            params.data = $.param(params.data);
        }

        // provide default properties array
        var defaults = {
            method: 'POST',
            dataType: 'json',
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        };

        // return defaults extended with passed params object
        return angular.extend(defaults, params);
    };

    return this;
});