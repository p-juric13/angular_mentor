onetoone.Controllers.controller('candidateAvailability',
function ($scope, $filter, $http, config, utils, candidateFunctions, fullcalendarHelper, calendar){
	
    $scope.candidateAvailability = [];

    // array of all timestamps
    $scope.timeslots_slice = [];

    // calendar events object
    $scope.calendarEvents = [];

    // placeholder for result object to store all the data needed to be sent
    $scope.result = {
        dates: [],
        slots: [],
        filters: []
    };

    $scope.confirmed = false;

    // calendar element
    var calendarElm = angular.element('#calendar-angular');

    $scope.$watch("infoMode", function(newValue, oldValue) {
       	if(newValue == oldValue) return false;
    	if(!newValue) return false;

    	if(newValue == 'calendar') { 
            $scope.resetCalendar();
            $scope.initCal();

    	} else return false;
    });

    $scope.$watch("selectedCandidate.candidateId", function(newValue, oldValue) {
        if(newValue == oldValue) return false;
        if(!newValue) return false;

        $scope.initCal();
    });

    /**
     * initiates fullcalendar plugin
     */
    $scope.initCalendar = function () {
        //Default View Options
        var options = {
            events: $scope.candidateAvailability,
            defaultView: 'agendaWeek',
            defaultEventMinutes: 30,
            minTime: 6,
            maxTime: 23,
            allDaySlot: false,
            allDayDefault: false,
            selectable: true,
            selectHelper: true,
            editable: true,
            eventColor: '#0e88af',
            disableResizing: true,
            availInfo: [],
            header: {
              left: '',
              center: '',
              right: ''
            },
            eventSources: [
              {
                events: [],
                backgroundColor: 'green',
                borderColor: 'green',
                textColor: 'yellow'
              }
            ],
            height: 810,
            axisFormat: 'h(:mm)TT',
            columnFormat: {
                week: 'dddd'
            },
            disableResizing: false,
            editable: true,
            select: function (start, end, allDay, ev, overlayflag) {
              fullcalendarHelper.selectHandler.call(this, start, end, allDay, ev, overlayflag, calendarElm);
            },

            dayClick: function (date) {
              fullcalendarHelper.NormaldayClickHandler.call(this, date, calendarElm);
            },

            eventDrop: function (event, dayDelta, minuteDelta, allDay, revertFunc) {
              fullcalendarHelper.eventDropHandler.call(this, event, dayDelta, minuteDelta, allDay, revertFunc, $scope.calendarEvents);
            },

            eventMouseover: function (event) {
              fullcalendarHelper.eventMouseoverHandler.call(this, event, calendarElm);
            },
                    
            eventResize: function(event,dayDelta,minuteDelta,revertFunc) {
                fullcalendarHelper.EventResizeHandler.call(this, event,dayDelta,minuteDelta,revertFunc);
            },
                    
            viewDisplay: function (view) {
              fullcalendarHelper.viewDisplay.call(this, calendarElm, $scope.calendarEvents);
            }
        };

        fullcalendarHelper.init(calendarElm, options);
    };

    // form submit handler
    $scope.submitCalendar = function (event) {
        if (event) event.preventDefault();

        // Retrieving calendar slots
        var slotObjects = calendarElm.fullCalendar('clientEvents');

        if (slotObjects.length < 1) {

            alert("Please give at least one available timeslot.");
            return false;
        }

        $scope.result = calendar.populateSlots(slotObjects);

        candidateFunctions.saveAvailability($scope.selectedCandidate.candidateId, $scope.result.slots)
            .success(function (response) {
                // $scope.results = [];
                alert("Mentee's schedule has been updated.");
            });
    };

    $scope.noMatchingTimes = function (event) {
        if (event) event.preventDefault();

        fancybox.open('#noMatchingTimes', {});
    }

    $scope.processRequest = function (event) {
        if (event) event.preventDefault();
        // $scope.confirmed = true;
        $scope.submitCalendar();
    };

    $scope.resetCalendar = function (event) {
        if (event) event.preventDefault();
        fullcalendarHelper.reset(calendarElm);
    };

    var default_timeslots = [];

    /**
     * handles data initialisation
     */
    $scope.initCal = function () {

        var successCallback = function (response) {
        	$scope.candidateAvailability = response;
            $scope.initCalendar();
        };

        candidateFunctions.getAvailability($scope.selectedCandidate.candidateId).success(successCallback);
    };

    // $scope.updateTimeslots = function () {
    //     $scope.timeslots_slice = timeslots.shift(default_timeslots, ($scope.timezone.offset - $scope.userTzOffset) * 3600000);
    // };

});