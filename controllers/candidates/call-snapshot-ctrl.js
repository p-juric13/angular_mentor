onetoone.Controllers.controller('callSnapshotCtrl',
function ($scope, $filter, $http, fancybox, config, utils, candidateFunctions) {

	$scope.info = null;
    $scope.history = null;

    var defaultCallParams = {
        queueId: null,
        notes: '',
        answers: {}
    };

    $scope.$watch('activeQueueId', function(newValue, oldValue) { 
        if(newValue != oldValue) $scope.init();
    }, true);

	$scope.init = function () {
        var queueId = $scope.activeQueueId;

        $scope.callParams = angular.copy(defaultCallParams);
        $scope.callParams.queueId = queueId;

        candidateFunctions.callDetails(queueId)
            .success(function (response) {
                $scope.info = response;

                // init properties to store questions values
                _(response.questions).each(function (question) {
                    $scope.callParams.answers[question.question_id] = null;
                });

                fancybox.open("#call_detail");
            });
    };

    $scope.init();

});