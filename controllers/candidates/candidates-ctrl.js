onetoone.Controllers.controller('candidates',
function ($scope, $filter, $http, fancybox, config, utils, candidateFunctions){
	$scope.title = "Mentees";
	$scope.org_id = null;
	$scope.candidates = [];

	$scope.mode = 'candidate-main'; // candidate-main, mentee-information
	$scope.infoMode = 'information'; //information, calendar

	$scope.modalMode = 'table'; // table, snapshot, noteDetails

	$scope.activeQueueId = null;
	
	var msg_modal_options={
		padding:15,
		minHeight : 20,
		closeBtn:false,
		helpers:{
			overlay:{
				closeClick:false
			}
		}
	};

	$scope.selectedCandidate = {
		candidateId: null,
		candidateInfo: {},
		candidateDetails: {},
		callDetails: [],
		phoneEdit: {
			primary: {
				disabled: null,
				verified: null,
				determineEditIcon: function() {
					if(this.disabled) return 'pencil';
					else return 'check';
				},
				determineVerifyIcon: function() {
					if(this.verified) return 'down';
					else return 'up';
				}
			},
			secondary: {
				exists: null,
				visible: null,
				disabled: null,
				verified: null,
				determineEditIcon: function() {
					if(this.disabled) return 'pencil';
					else return 'check';
				},
				determineVerifyIcon: function() {
					if(this.verified) return 'down';
					else return 'up';
				}
			}
		}
	};

	$scope.message={
		subject: "",
		msg: "",
		to_id: 0,
		to_type: 1, //candidates group
		from_id: 0,
		from_type: 50 //admin group
	}
	
	$scope.state = {
		searchTerm: null,
		pageSizesAvailable: [10, 15, 25, 50],
		filter: {
		    type: 'all',//all, active, match_pending, enrolled, cancelled, completed
		    query: '',
		},
		pageSize: 10,
		totalAmbassadors: 0,
		currentPage: 0,
		numberOfPages: function(candidates) {
			return Math.ceil(candidates / $scope.state.pageSize) || 1;
		}
	};

	$scope.notes = {
		mode: 'list', // list, add
		notes: [],
		activeNote: {},
		newNote: {
			title: null,
			body: null,
			visible_all_ind: 0
		},
		error: null,
		message: null
	}

	function rtrim(str, chars) {
        chars = chars || "\s";
        return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
    }

	// Underscore JS workaround for getting the index of array of objects
    // save a reference to the core implementation
    var indexOfValue = _.indexOf;

    // using .mixin allows both wrapped and unwrapped calls:
    // _(array).indexOf(...) and _.indexOf(array, ...)
    _.mixin({

        // return the index of the first array element passing a test
        indexOf: function(array, test) {
            // delegate to standard indexOf if the test isn't a function
            if (!_.isFunction(test)) return indexOfValue(array, test);
            // otherwise, look for the index
            for (var x = 0; x < array.length; x++) {
                if (test(array[x])) return x;
            }
            // not found, return fail value
            return -1;
        }

    });

	$scope.editPhone = function(phoneType) {
		if(!$scope.selectedCandidate.phoneEdit[phoneType].disabled) $scope.saveUpdatedPhone(phoneType);
		else $scope.selectedCandidate.phoneEdit[phoneType].disabled = !$scope.selectedCandidate.phoneEdit[phoneType].disabled;
	};

	$scope.saveUpdatedPhone = function(phoneType) {
		var phoneno = /^\d{10}$/;  

		if(phoneType == 'primary') {
			if(!$scope.selectedCandidate.candidateDetails.phone_num) {
				alert("Please enter a valid phone number.");
				return false;
			}
			$scope.selectedCandidate.candidateDetails.phone_num = $scope.selectedCandidate.candidateDetails.phone_num.replace(/[^0-9.]/g, "");
			if($scope.selectedCandidate.candidateDetails.phone_num.match(phoneno)) {
				$scope.selectedCandidate.candidateDetails.phone_num = $scope.selectedCandidate.candidateDetails.phone_num.slice(0,3)+"-"+$scope.selectedCandidate.candidateDetails.phone_num.slice(3,6)+"-"+$scope.selectedCandidate.candidateDetails.phone_num.slice(6);
				var candidateAttr = { id: $scope.selectedCandidate.candidateId, phone_num : $scope.selectedCandidate.candidateDetails.phone_num };
			} else {
				alert("Please enter a valid phone number.");
				return false;
			}
		} else {
			if(!$scope.selectedCandidate.candidateDetails.secondary_phone_num) {
				alert("Please enter a valid phone number.");
				return false;
			}
			$scope.selectedCandidate.candidateDetails.secondary_phone_num = $scope.selectedCandidate.candidateDetails.secondary_phone_num.replace(/[^0-9.]/g, "");
			if($scope.selectedCandidate.candidateDetails.secondary_phone_num.match(phoneno)) {
				$scope.selectedCandidate.candidateDetails.secondary_phone_num = $scope.selectedCandidate.candidateDetails.secondary_phone_num.slice(0,3)+"-"+$scope.selectedCandidate.candidateDetails.secondary_phone_num.slice(3,6)+"-"+$scope.selectedCandidate.candidateDetails.secondary_phone_num.slice(6);
				var candidateAttr = { id: $scope.selectedCandidate.candidateId, secondary_phone_num : $scope.selectedCandidate.candidateDetails.secondary_phone_num };
			} else {
				alert("Please enter a valid phone number.");
				return false;
			}
			
		}

		candidateFunctions.saveCandidateInfo(candidateAttr)
			.success(function (response) { 
				if(phoneType == 'secondary') $scope.selectedCandidate.phoneEdit.secondary.exists = true;
				$scope.selectedCandidate.phoneEdit[phoneType].disabled = !$scope.selectedCandidate.phoneEdit[phoneType].disabled;
				alert("Phone number updated.");	
			});	
	};

	$scope.verifyPhone = function(phoneType) {
		if(phoneType == 'primary') {
			var verificationStatus = !$scope.selectedCandidate.phoneEdit.primary.verified ? 1 : 0;
			var candidateAttr = { id: $scope.selectedCandidate.candidateId, phone_num_val : verificationStatus };
		} else {
			var verificationStatus = !$scope.selectedCandidate.phoneEdit.secondary.verified ? 1 : 0;
			var candidateAttr = { id: $scope.selectedCandidate.candidateId, secondary_phone_num_val : verificationStatus };
		}

		candidateFunctions.saveCandidateInfo(candidateAttr)
			.success(function (response) { 
				if(phoneType == 'primary') $scope.selectedCandidate.phoneEdit.primary.verified = !$scope.selectedCandidate.phoneEdit.primary.verified;
				else $scope.selectedCandidate.phoneEdit.secondary.verified = !$scope.selectedCandidate.phoneEdit.secondary.verified;
				alert("Phone verification status updated.");
			});	
	};

	$scope.removePhone = function(phoneType) {
		if(phoneType == 'primary') {
			alert("Cannot remove primary phone.")
			return false;
		}

		if(window.confirm("Are you sure you want to remove this phone number?")) {
			var candidateAttr = { id: $scope.selectedCandidate.candidateId, secondary_phone_num : null, secondary_phone_num_val : 0 };
			candidateFunctions.saveCandidateInfo(candidateAttr)
			.success(function (response) { 
				$scope.selectedCandidate.candidateDetails.secondary_phone_num = null;
				$scope.selectedCandidate.phoneEdit.secondary.exists = false;
				$scope.selectedCandidate.phoneEdit.secondary.visible = false;
				alert("Phone verification status updated.");
			});	
		}
	}

	$scope.activateSecondarySlot = function() {
		$scope.selectedCandidate.phoneEdit.secondary.visible = true;
		$scope.selectedCandidate.phoneEdit.secondary.disabled = false;
	};

	$scope.setViewMode = function(event, updatedMode) {
    	if(event) event.preventDefault();
    	$scope.mode = updatedMode;
    };

    $scope.setInfoMode = function(event, updatedMode) {
    	if(event) event.preventDefault();
    	$scope.infoMode = updatedMode;
    };

	$scope.init = function (orgId, org_model, admin_id, selected_candidate_id) {
		$scope.mode = 'candidate-main';
        $scope.org_id = orgId;
		$scope.message.from_id = admin_id;
		$scope.org_model=org_model;
		
		// $scope.selectedCandidate.phoneEdit.primary.disabled = true; 
		// $scope.selectedCandidate.phoneEdit.primary.verified = false; 
		// $scope.selectedCandidate.phoneEdit.secondary.exists = false; 
		// $scope.selectedCandidate.phoneEdit.secondary.visible = false; 
		// $scope.selectedCandidate.phoneEdit.secondary.disabled = true; 
		// $scope.selectedCandidate.phoneEdit.primary.verified = false; 

     	candidateFunctions.fetchCandidates(orgId)
			.success(function(result){
				if(result && result.candidates.length > 0){
					$scope.candidates = result.candidates;
					if(selected_candidate_id) {
						$scope.menteeInformationMode(null, selected_candidate_id);
					}
				}
			});
    };

	$scope.availability={};
	$scope.activeCalls=[];

	$scope.fetchCallHistory = function(candidateId) {
		candidateFunctions.fetchCandidateCallHistory(candidateId)
			.success(function (result) {
				$scope.activeCalls = result;
			});
	};
	
	$scope.opendropdown = function (mentor_id){
		var list=angular.element("#dropdownlist_"+mentor_id);
		angular.element(".js-datalist").not(list).addClass('hidden');
    	list.toggleClass('hidden');
		return false;
  };
	
	$scope.setTypeFilter = function (event, value) {
	    if (event) event.preventDefault();
	    $scope.state.filter.type = value;
	    $scope.state.currentPage = 0;
		$scope.state.filter.query = "";
	};
	
	$scope.setFilterQuery = function (event){
		if (event) event.preventDefault();
		
		if($scope.state.searchTerm !== null && $scope.state.searchTerm.title) {
			$scope.state.filter.query = $scope.state.searchTerm.title;
			$scope.state.currentPage = 0;
		}
	}

	$scope.isTypeFilterActive = function (value) {
	    return $scope.state.filter.type === value;
	};

	$scope.filterCandidate = function (candidate){
	    var valid = true,
	    type = $scope.state.filter.type;
	    query = $scope.state.filter.query;

	    // check mentor type
	    if (type !== 'all') {
	      if (type === 'active') {
					var pattern = /active/ig;
					/*valid = candidate.candidate_update_status == "Active";*/
					valid = pattern.test(candidate.candidate_update_status);
	      } else if (type === 'enrolled') {
					var pattern = /enrolled/ig;
	        //valid = candidate.candidate_update_status == "Enrolled; First Call Pending" || candidate.candidate_update_status == "Enrolled; Awaiting Mentor Acceptance";
					valid = pattern.test(candidate.candidate_update_status);
	      } else if (type === 'match_pending') {
	        valid = candidate.candidate_update_status == "Match Pending";
	      } else if (type === 'cancelled') {
	        valid = candidate.candidate_update_status == "Cancelled; Mentee Cancelled" || candidate.candidate_update_status == "Sidelined; Mentor Cancelled";
	      } else if (type === 'completed') {
	        valid = candidate.candidate_update_status == "Completed Program!";
	      }
	    }
		
		if (valid && query && query.length >= 1) {
        	var fullName = candidate.first_name + ' ' + candidate.last_name;
            if(fullName != query) valid = false;
	    }

    return valid;
  };
	
	$scope.showMessagePopup = function (id) {
		$scope.message.to_id=id;
		$scope.message.subject="";
		$scope.message.msg="";
		angular.element('#message_subject').removeClass("error");
		angular.element('#message_msg').removeClass("error");
		fancybox.open("#message_popup");
	};

	$scope.send_message=function(){
		//angular.element('#availability').scope()
		var error=false;
		if($scope.message.subject==""){
			angular.element('#message_subject').addClass("error");
			error=true;
		}
		if($scope.message.msg==""){
			angular.element('#message_msg').addClass("error");
			error=true;
		}
		if(!error){
			$http(utils.httpParams({
	      url: config.ROOT_URL + config.URLS.MESSAGE.SEND_MESSAGE,
	      method: 'POST',
	      data: $scope.message
	    })).success(function (response) {
				if(response>0){
					fancybox.close();
				}else{
					alert('server error');
				}
			});
		}
	}

	// $scope.showCallsTable = function (id,indexnumber,callCount) {
	// 	if (!id) return;
	// 	if(callCount < 1) return;
 //    // cache dom query after the first call
 //    //availPopupScope = availPopupScope || angular.element('#availability').scope();
		
 //    $http(utils.httpParams({
 //      url: config.ROOT_URL + config.URLS.AMBASSADORS.GET_ACTIVE_MENTEES_DATA,
 //      method: 'GET',
 //      params: {
 //        id:id 
 //      }
 //    })).success(function (response) { 
	// 		var tempVar = _.where($scope.mentors, {id: id});
	// 		angular.extend($scope.selectedMentor, tempVar[0]); 
	// 		$scope.activeMentees = response;
	// 		fancybox.open("#current_mentored");
	// 	});
	// 	var tempVar = _.where($scope.candidates, {id: id});
	// 	angular.extend($scope.selectedCandidate, tempVar[0]);
	// 	$scope.fetchCallHistory(id);

	// 	fancybox.open("#active_calls");
 //  };
	
	$scope.viewCallDetail = function(queueId){
		$scope.modalMode = 'snapshot';
		if($scope.activeQueueId != queueId) $scope.activeQueueId = queueId;
		else fancybox.open("#call_detail");
	};
	
	function closedropdown(){
		angular.element(".js-datalist").addClass('hidden');
		return false;
  	};
	
	function changemsgstatus(msg,status,modal_options){
		angular.element("#ajaxstatusmodal").html(msg);
		if(status=="success"){
			angular.element("#ajaxstatusmodal").css("color","green");
		}else if(status=="error"){
			angular.element("#ajaxstatusmodal").css("color","red");
		}else{
			angular.element("#ajaxstatusmodal").css("color","#404040");
		}
		fancybox.open("#ajaxstatusmodal",modal_options);
	}

	//Resend Invitation
	$scope.resendInvitation=function(ambassador_id){
		if (!ambassador_id) return;
    // cache dom query after the first call
    //availPopupScope = availPopupScope || angular.element('#availability').scope();
		closedropdown();
		changemsgstatus("Sending ...","normal",msg_modal_options);
	    $http(utils.httpParams({
	      url: config.ROOT_URL + config.URLS.ADMINISTRATION.SEND_INVITE_REMINDER,
	      method: 'GET',
	      params: {
	        ambassador_id:ambassador_id 
	      }
	    })).success(function (response) {
				if(response.result==1){
					changemsgstatus(response.msg,"success",msg_modal_options);
				}else{
					changemsgstatus(response.msg,"error",msg_modal_options);
				}
				setTimeout(fancybox.close,1000);
			}).error(function(data,status,headers,config){
				changemsgstatus(" Error! ","error",msg_modal_options);
				setTimeout(fancybox.close,1000);
			});
		}

  $scope.markProgramComplete = function(candidateId) {
  	candidateFunctions.markProgramComplete(candidateId)
		.success(function (result) {
			window.location = '/getcandidatedata/mark_program_complete/1';
		});
  }	

  $scope.removeCandidateProgram = function(candidateId) {
  	candidateFunctions.removeCandidateProgram(candidateId)
		.success(function (result) {
			window.location = '/getcandidatedata/remove_candidate_program/1';
		});
  }
	
  $scope.nextPage = function() {
  	$scope.state.currentPage = $scope.state.currentPage + 1;
  };

  $scope.previousPage = function() {
  	$scope.state.currentPage = $scope.state.currentPage - 1;
  };

  $scope.isOnFirstPage = function() {
  	return $scope.state.currentPage === 0;
  }

  $scope.isOnLastPage = function () {
    return $scope.state.currentPage >= $scope.candidates.length / $scope.state.pageSize - 1;
  };

  $scope.menteeInformationMode = function(event, selectedCandidateId) {
  	if(event) event.preventDefault();
  	$scope.title = "Mentee Information";
  	$scope.mode = 'mentee-information';

  	$scope.selectedCandidate.candidateId = selectedCandidateId;
  	$scope.selectedCandidate.candidateInfo = _.find($scope.candidates, function(aCandidate) { return aCandidate.id == selectedCandidateId });
  	candidateFunctions.candidateCoreInfo(selectedCandidateId)
  		.success(function (response) {
	  		$scope.selectedCandidate.candidateDetails = response;
	  		// Phone number adjustments
	  		if($scope.selectedCandidate.candidateDetails.secondary_phone_num) $scope.selectedCandidate.phoneEdit.secondary.exists = true;
	  		if($scope.selectedCandidate.candidateDetails.phone_num_val == 1) $scope.selectedCandidate.phoneEdit.primary.verified = true;
	  		else $scope.selectedCandidate.phoneEdit.primary.verified = false;
	  		if($scope.selectedCandidate.candidateDetails.secondary_phone_num_val == 1) $scope.selectedCandidate.phoneEdit.secondary.verified = true;
	  		else $scope.selectedCandidate.phoneEdit.secondary.verified = false;
	  		candidateFunctions.getCallDetails(selectedCandidateId)
	  			.success(function (response2) {
	  				$scope.selectedCandidate.callDetails = response2.call_details;
	  				$scope.selectedCandidate.candidateDetails.aggregate = response2.aggregate_info;
	  			});

	  		// Get the notes relevant to this mentee
	  		candidateFunctions.getNotes(selectedCandidateId)
	  			.success(function (response) {
	  				$scope.notes.notes = response;
	  			});
  		});
  };

  $scope.calculateAge = function(yob) {
  	var currentYear = new Date().getFullYear();
  	return currentYear - yob;
  };

  $scope.determineCallWidth = function(numCalls) {
  	var width = (numCalls/12)*100;
  	return width;
  };

  $scope.saveNote = function(event) {
  	if(event) event.preventDefault();

  	if(!$scope.notes.newNote.title || $scope.notes.newNote.title.length < 3) { 
  		$scope.notes.error = "Please enter a title.";
  		return;
  	} else if(!$scope.notes.newNote.body || $scope.notes.newNote.body.length < 10) { 
  		$scope.notes.error = "Please enter content in the body of the note.";
  		return;
  	} else $scope.notes.error = null;

  	$scope.notes.newNote.admin_id_added_by = $scope.message.from_id;

	  	candidateFunctions.saveNote($scope.selectedCandidate.candidateId, $scope.notes.newNote)
	  		.success(function (response) {
	  			if(response.type == 'update') {
		  			candidateFunctions.getNotes($scope.selectedCandidate.candidateId)
			  			.success(function (response) {
			  				$scope.notes.notes = response;
			  				$scope.notes.mode = 'list';
				  			$scope.notes.message = "Note updated.";
		  			});
	  			} else {
		  			$scope.notes.newNote.title = null;
		  			$scope.notes.newNote.body = null;
		  			if($scope.notes.notes.length == 0) { 
		  				$scope.notes.notes = [];
		  				$scope.notes.notes.push(response.response);
		  			} else $scope.notes.notes.unshift(response.response);
		  			$scope.notes.mode = 'list';
		  			$scope.notes.message = "Note added.";
	  			}
	  			
	  		});
  };

  $scope.openNote = function(noteId) {
  	$scope.notes.activeNote = _.find($scope.notes.notes, function(aNote) { return aNote.id ==  noteId });
  	$scope.modalMode = 'noteDetails';
  	fancybox.open("#note_details");
  };

  $scope.verifyAuthor = function(adminId) {
  	if($scope.message.from_id == adminId) return true;
  	return false;
  };

  $scope.editNote = function(event, noteId) {
  	if(event) event.preventDefault();
  	$scope.notes.mode = 'add';
  	var noteToEdit = _.find($scope.notes.notes, function(aNote) { return aNote.id == noteId });
  	$scope.notes.newNote.id = noteToEdit.id;
  	$scope.notes.newNote.title = noteToEdit.title;
  	$scope.notes.newNote.body = noteToEdit.body;
  	$scope.notes.newNote.visible_all_ind = noteToEdit.visible_all_ind;
  }

});