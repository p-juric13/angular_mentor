onetoone.Controllers.controller('CandidateInformation', function ($scope, config, accountjson) {
    $scope.state = {
        resetPasswordForm: false
    };

    // form values
    $scope.first_name='';
    $scope.last_name='';
    $scope.email='';
    $scope.gender='';
    $scope.phone_num='';
    $scope.zipcode='';
    $scope.sms_alerts='';
    $scope.user_id=0;
    $scope.org_uri='';
    $scope.vol_language_list=[];

    $scope.resetPassword = function (event) {
        if (event) event.preventDefault();

        $scope.state.resetPasswordForm = true;

        accountjson.inactivateLogin($scope.user_id)
            .success(function () {})
            .error(function () {});
    };
});