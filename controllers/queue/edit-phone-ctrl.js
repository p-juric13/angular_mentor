onetoone.Controllers.controller('editPhoneCtrl',
function ($scope, config, queue, colorbox, notifications) {
  'use strict';

    // obtain phone from call-ctrl by inheritance
    $scope.$watch("info.candidate_profile.phone", function (newValue) {
        $scope.phone = newValue;
    });

    $scope.updatePhone = function (event, phone) {
        event.preventDefault();
        queue.updatePhone(phone, $scope.callParams.queueId).success(function () {
            colorbox.close();
            notifications.add(
                angular.extend(
                  angular.copy(config.EVENT[13]),
                  { queue_id: $scope.callParams.queueId }
                )
              );
        });
    };
});