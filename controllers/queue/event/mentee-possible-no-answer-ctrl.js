'use strict';

onetoone.Controllers
    .controller('MenteePossibleNoAnswerCtrl', function ($scope, queueFunctions, config, notifications) {
        $scope.status = 'form';
        // $scope.next_time = null;

        $scope.callNotSuccessful = function () {
            queueFunctions.logIncompleteCallTransaction($scope.notification.queue_id)
                .success(function () {
                    $scope.status = 'failure';
                });
        };

        $scope.callConnected = function () {
            $scope.queue.menteeAnsweredCall = 1;
            queueFunctions.connectedCall($scope.notification.queue_id)
                .success(function (result) {
                    $scope.call.options.scheduling_next = 1;
                    $scope.status = 'success';
                });
        }

        $scope.openCallPopup = function () {
            angular.element('#da-content-area').scope().openCall($scope.notification.queue_id);
        };

        $scope.callPopupScope = function () {
            return angular.element('#call_popup').scope();
        };

        // $scope.rescheduleCall = function () {
        //     queueFunctions.rescheduleCall($scope.notification.queue_id)
        //         .success(function () {
        //             $scope.openCallPopup();
        //             $scope.callPopupScope().openRescheduleCall();
        //             $scope.hide($scope.notification.index);
        //         });
        // };

        // $scope.requeueCall = function () {
        //     queueFunctions.requeueCall($scope.notification.queue_id)
        //         .success(function () {
        //             $scope.hide($scope.notification.index);
        //             alert("The call will be requeued shortly.");
        //         });
        // };

        // $scope.callEndedSuccessfully = function () {
        //     queueFunctions.successfulCall($scope.notification.queue_id)
        //         .success(function () {
        //             $scope.status = 'success';
        //         });
        // };

        $scope.tryAgain = function () {
            $scope.hide($scope.notification.index);
            $scope.queue.mode = 'list';
            alert("We have left the call workbook open. You can leave this window open or if you come back, you will see that you can 'Continue' the workbook to get back to this page.");
        };

        $scope.rescheduleReminder = function () {
            notifications.add(
              angular.extend(
                angular.copy(config.EVENT[21]),
                {queue_id: $scope.queue.activeId }
              )
            );
            alert('Be sure to go to "Call Snapshot" section of the workbook and reschedule the call for a later date and time.');
        };

    });
