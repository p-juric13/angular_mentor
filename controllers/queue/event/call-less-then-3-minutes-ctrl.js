'use strict';

onetoone.Controllers
  .controller('CallLessThen3MinutesCtrl', function ($scope, config, notifications) {
    $scope.status = 'form';

    $scope.openCallPopup = function () {
      angular.element('#da-content-area').scope().openCall($scope.notification.queue_id);
    };

    $scope.callNotSuccessful = function () {
      $scope.hide($scope.notification.index);
      $scope.openCallPopup();
      notifications.add({
        message: config.STRING.CALL_LESS_THEN_3_MINS_NOT_SUCCESSFUL
      });
    };
  });
