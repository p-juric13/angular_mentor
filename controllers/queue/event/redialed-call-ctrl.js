'use strict';

onetoone.Controllers
  .controller('RedialedCallCtrl', function ($scope, config, notifications) {
    $scope.status = 'form';

    $scope.openCallPopup = function () {
      angular.element('#da-content-area').scope().openCall($scope.notification.queue_id);
      $scope.hide($scope.notification.index);
    };

    $scope.doNotContinue = function () {
      notifications.add({message: config.STRING.FILL_IN_CALL_SNAPSHOT});
      $scope.hide($scope.notification.index);
    };
  });
