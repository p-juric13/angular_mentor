onetoone.Controllers.controller('callGoalsCtrl',
function ($scope, candidateGoals, fancybox) {
    'use strict';
    

    $scope.state = {
        orderBy: "priority",
        filterByStatus: "all"
    };

    $scope.goals = [];

    $scope.totalActiveGoals = 0;

    candidateGoals.getOverview($scope.candidateProgramId)
        .success(function (response) {
            $scope.goals = response;

            _.each($scope.goals, function(goal) {
                _.each(goal.selected_steps, function(selected_goal) {
                    if(selected_goal.status == 'active') $scope.totalActiveGoals = $scope.totalActiveGoals + 1;
                });
            });
        });

    // watch selected_steps array and update datepickerOptions for each selected step
    $scope.$watch("goals", function (goals) {
        if (! goals.length) {
            return false;
        }

        _(goals).map(function (goal) {
            _(goal.selected_steps).map(function (step) {
                if (! step.datePickerOptions) {
                    step.datePickerOptions = $scope.getDatePickerOptions(step);
                }
            });
        });
    }, true);

    $scope.preventDropdownClosing = function () {
        $("body").delegate(".dropdown-menu input[data-ui-date]", "click", function (event) {
            event.stopPropagation();
        });
    };

    /**
     * Utility function to define step status in UI
     *
     * @param {string} status
     *
     * @returns {boolean}
     */
    $scope.isStepActive = function (status) {
        return status === 'active';
    };

    /**
     * Utility function to define step status in UI
     *
     * @param {string} status
     *
     * @returns {boolean}
     */
    $scope.isStepInactive = function (status) {
        return status === 'inactive';
    };

    /**
     * Utility function to define step status in UI
     *
     * @param {string} status
     *
     * @returns {boolean}
     */
    $scope.isStepCompleted = function (status) {
        return status === 'completed';
    };

    /**
     * Filters goals to show only ones that match filtering rules
     *
     * @param {array} goal
     */
    $scope.filterGoals = function (goal) {
        if ($scope.state.filterByStatus === "all" || $scope.state.filterByStatus === goal.status) {
            return true;
        }

        return false;
    };

    /**
     * Handler for click on step in addStep dropdown
     *
     * @param {event} event
     * @param {object} step
     * @param {object} goal
     */
    $scope.updateStep = function (event, step, goal) {
        if (event) {
            event.stopPropagation();
        }

        // debugger;
        var selectedSteps = goal.selected_steps;

        // checks state of steps.selected and adds/removes goal to/from selected_goals
        if (!! parseInt(step.selected)) {
            var newStep = $scope.fillInNewStep(_(step).clone(), selectedSteps.length + 1);

            candidateGoals.addStep($scope.candidateProgramId, step.id)
                .success(function (stepResponse) {
                    selectedSteps.push(stepResponse || newStep);
                }).error(function () {
                    console.log("addStep failed");
                });
        } else {
            var currentStep = $scope.findStepById(selectedSteps, step.id);
            var currentStepIndex = _(selectedSteps).indexOf(currentStep);
            candidateGoals.removeStep($scope.candidateProgramId, step.id)
                .success(function () {
                    selectedSteps.splice(currentStepIndex, 1);
                }).error(function () {
                    console.log("removeStep failed");
                });
        }

        return false;
    };

    /**
     * Receives step object and extends it with fields required for display
     *
     * @param {object} step
     * @param {int} index
     * @returns {object}
     */
    $scope.fillInNewStep = function (step, index) {
        return _(step).extend({
            priority: index,
            target_completion_date: null,
            status: 'active'
        });
    };

    /**
     * Finds step by its id in steps array
     *
     * @param {Array} steps
     * @param {int} stepId
     */
    $scope.findStepById = function (steps, stepId) {
        return _(steps).find(function (step) {
            return step.id === stepId;
        });
    };

    /**
     * Handler for click stops event propagation
     * @param event
     */
    $scope.stopPropagation = function (event) {
        if (event) {
            event.stopPropagation();
        }
    };

    /**
     * Toggle step complete status
     *
     * @param {event} event
     * @param {object} step
     */
    $scope.toggleStepCompleted = function (event, step) {
        if (event) {
            event.stopPropagation();
            event.preventDefault();
        }

        if (step.status === 'completed') {
            step.status = 'active';
        } else {
            step.status = 'completed';
        }

        candidateGoals.saveStep($scope.candidateProgramId, step.id, { status: step.status });
    };

    $scope.getDatePickerOptions = function (step) {
        return {
            minDate: 0,
            onSelect: function () {
                candidateGoals.saveStep($scope.candidateProgramId, step.id, { target_completion_date: step.target_completion_date });
            }
        };
    };

    $scope.getSelectedStepsSortableOptions = function (goal) {
        return {
            disabled: false,
            update: function ( event, ui ) {
                var idsInOrder = $('#' + goal.path.id + 'sortable').sortable("toArray"),
                    goalsPriorities = [];

                for(var i=0; i < idsInOrder.length; i++) {
                    goalsPriorities.push({
                        id: idsInOrder[i],
                        priority: i+1
                    });
                }
               
                candidateGoals.setGoalsPriorities($scope.candidateProgramId, goalsPriorities);
            }
        };
    };

    /**
     * Opens fancybox popup with action list settings
     *
     * @param {object} step
     */
    $scope.openActionListDetails = function (step) {
        angular.element('#call_actions_list_popup').scope().init(step.id);
				fancybox.open("#call_actions_list_popup");
    };

});
