onetoone.Controllers.controller('callDetailsCtrl',
function ($scope, config, queue) {
    'use strict';

    $scope.call = {
        state: {
            mode: "snapshot" // snapshot | talking_points | goals | notes
        }
    };

    $scope.info = null;
    $scope.history = null;

    var defaultCallParams = {
        queueId: null,
        notes: '',
        answers: {}
    };

    /**
     * Switches tab inside call info based on its name
     *
     * @param {string} name
     */
    $scope.switchTab = function (name) {
        $scope.call.state.mode = name;
    };

    /**
     * Used to determine if tab is active
     *
     * @param name
     * @returns {boolean}
     */
    $scope.isTabActive = function (name) {
        return $scope.call.state.mode === name;
    };

    $scope.init = function () {
        var queueId = $scope.queue.activeId;

        $scope.callParams = angular.copy(defaultCallParams);
        $scope.callParams.queueId = queueId;

        queue.callDetails(queueId)
            .success(function (response) {
                $scope.info = response;

                // init properties to store questions values
                _(response.questions).each(function (question) {
                    $scope.callParams.answers[question.question_id] = null;
                });
            });

        $scope.setBreadcrumbs([{
            title: "Call Workbook",
            action: function () {
                $scope.openCallDetails($scope.callParams.queueId);
            }}]);
    };

    $scope.init();

});
