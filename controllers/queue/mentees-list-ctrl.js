onetoone.Controllers.controller('MenteesList', 
function ($scope, $filter, $compile, queue, $http, fancybox, config, menteeFunctions) {

    $scope.menteesNames = [];
    $scope.presentedNames = [];

    $scope.activeMentee = {
        'candidate_id': null,
        'primary_info': {},
        'demographic_data': [],
        'clinical_data': [],
        'action_plan_data': [],
        'summary_of_calls': [],
        'summary_of_tx': [],
        'at_a_glance': {},
        'summary_of_calls_prop': {
            currentPage: 0,
            pageSize: 5
        },
        'summary_of_tx_prop': {
            currentPage: 0,
            pageSize: 5
        }
    };

    $scope.activeStyle = {background: '#708090', color: '#FFFFFF'};

    $scope.callDetailsObj = {};
    $scope.actionPlansObj = {};

    $scope.schedulingWidget = {
        'mode': null, // '1 => reschedule', 0 => schedule
        'pullInSchedulingWidget': 0,
        'scheduled_ind': 0
    };

    $scope.menteesavails = {
      activeindex : 0,
      currentPage : 0,
      pageSize : 9,
      data: []
    };

    $scope.taskList = {
        mode: 'all', // all | missedCalls | openWorkbooks | forgettenMentees
        activeHeader: 'All Open Items', //All Open Items | Missed Calls | Forgotten Mentees | Open Workbooks
        missedCalls: [],
        openWorkbooks: [],
        forgottenMentees: []
    };

    $scope.activeQueueId = null;

    $scope.activeLetterIndex = null;

    $scope.$watch("queue.filter.searchTerm.title", function(newValue, oldValue) {
        if(newValue && newValue != oldValue) {
            var matchedCandidateId = null;
            _.each($scope.queue.filter.searchArea, function(a_name) { 
                var menteeName = a_name.first_name + ' ' + a_name.last_name;
                if(menteeName == $scope.queue.filter.searchTerm.title) { 
                    matchedCandidateId = a_name.candidate_id;
                }
            });
            if(matchedCandidateId) $scope.fetchMenteeInfo(matchedCandidateId);
        }
    });

    $scope.init = function() {
        $scope.queue.mode = 'mentee-list';

        menteeFunctions.buildTaskList($scope.ambassadorId)
            .success(function (response) {
                $scope.taskList.missedCalls = response.missed_call_details;
                $scope.taskList.forgottenMentees = response.forgotten_mentee_details;
                $scope.taskList.openWorkbooks = response.open_workbooks;
            });

        menteeFunctions.getMenteeNames($scope.ambassadorId)
            .success(function (response) {
                $scope.queue.filter.searchArea = response.searchable_list;
                for (var key in response.response) {

                    var tempObj = {
                        'firstInitial': null,
                        names: null
                    };
                    tempObj.firstInitial = key;
                    tempObj.names = response.response[key];
                    $scope.menteesNames.push(tempObj);
                }

                if($scope.activateMenteeView.candidateId) { 
                    var invalidRelationship = _.every(response.response, function(aLetter) { 
                            var checkRelationship = _.find(aLetter, function(aCandidate) { return aCandidate.candidate_id == $scope.activateMenteeView.candidateId}); 
                            if(checkRelationship) return false;
                            else return true;
                        }
                    );

                    if(!invalidRelationship) $scope.fetchMenteeInfo($scope.activateMenteeView.candidateId);
                    else { 
                        $scope.notification.displayInd = 1;
                        $scope.notification.message = 'Sorry, this is not one of your mentees. If you believe this is an error, please message us at yourfriends@1to1mentor.org';
                    }
                }

                // For init, push the whole list over to the presentedNames list;
                $scope.presentedNames = $scope.menteesNames;
            }); 
    };

    $scope.filterList = function(firstNameInitial) {
        $scope.presentedNames = [];
        $scope.presentedNames.push(_.find($scope.menteesNames, function(aMentee) {return aMentee.firstInitial == firstNameInitial}));
    };

    $scope.filterListReset = function() {
        $scope.presentedNames = $scope.menteesNames;
    }

    $scope.determineAlphaClass = function(index) {
        if(index == 0) return 'active';
    };

    $scope.highlightActive = function(menteeId) {
        if(menteeId == $scope.activeMentee.candidate_id) return $scope.activeStyle;
    };

    $scope.fetchMenteeInfo = function(candidateId, event) {
        if(event) event.preventDefault();

        window.scrollTo(0, 0);

        $scope.schedulingWidget.pullInSchedulingWidget = 0;

        menteeFunctions.getCoreMenteeInfo(candidateId)
            .success(function (response) {
                $scope.activeMentee.candidate_id = response.candidate_id;
                $scope.activeMentee.primary_info.first_name = response.first_name;
                $scope.activeMentee.primary_info.last_name = response.last_name;
                $scope.activeMentee.primary_info.yob = response.yob;
                $scope.activeMentee.primary_info.gender = response.gender;
                $scope.activeMentee.demographic_data = [];
                $scope.activeMentee.clinical_data = [];

                for(x=1; x <= 10; x++) {
                    if(response['attr_'+x+'_section'] == 'Demographic') {
                        if(!response['attr_'+x+'_val']) response['attr_'+x+'_val'] = response['attr_'+x+'_val_text'];
                        $scope.activeMentee.demographic_data.push({'attr_name': response['attr_'+x], 'attr_val': response['attr_'+x+'_val']});
                    }
                    if(response['attr_'+x+'_section'] == 'Clinical') {
                        if(!response['attr_'+x+'_val']) response['attr_'+x+'_val'] = response['attr_'+x+'_val_text'];
                        $scope.activeMentee.clinical_data.push({'attr_name': response['attr_'+x], 'attr_val': response['attr_'+x+'_val']});
                    }
                }
            });
        menteeFunctions.getMenteeActionPlanInfo(candidateId) 
            .success(function (response) {
                $scope.activeMentee.action_plan_data = response;
            });

        menteeFunctions.getSummaryOfCalls(candidateId, $scope.ambassadorId)
            .success(function (response) {
                $scope.activeMentee.summary_of_calls = response;
            });

        menteeFunctions.getSummaryOfTx(candidateId, $scope.ambassadorId)
            .success(function (response) {
                $scope.activeMentee.summary_of_tx = response;
            });

        menteeFunctions.getAtAGlance(candidateId) 
            .success(function (response) {
                $scope.activeMentee.at_a_glance = response;
                $scope.schedulingWidget.scheduled_ind = 0;
                if(!$scope.activeMentee.at_a_glance.next_call_queue_id) $scope.schedulingWidget.mode = 0; else $scope.schedulingWidget.mode = 1;
            });
    };

    $scope.rescheduleCall = function(event, queueId) {
        if(event) event.preventDefault();
        $scope.schedulingWidget.name = 'Rescheduling';
        $scope.schedulingWidget.pullInSchedulingWidget = 1;

        $scope.activeQueueId = queueId;
        
        queue.getMenteeAvailability($scope.activeMentee.candidate_id, queueId)
            .success(function (response) { 
              $scope.menteesavails.data = response;
              $scope.menteesavails.pages = Math.ceil($scope.menteesavails.data.length / $scope.menteesavails.recordnum)
            });
    }

    $scope.scheduleCall = function(event) {
        if(event) event.preventDefault();
        $scope.schedulingWidget.name = 'Scheduling';
        $scope.schedulingWidget.pullInSchedulingWidget = 1;

        queue.getMenteeAvailability($scope.activeMentee.candidate_id)
            .success(function (response) { 
              $scope.menteesavails.data = response;
              $scope.menteesavails.pages = Math.ceil($scope.menteesavails.data.length / $scope.menteesavails.recordnum)
            });
    }

    $scope.viewCallDetails = function(event, queueId) {
        if(event) event.preventDefault();

        queue.callDetails(queueId)
            .success(function (response) {
                $scope.callDetailsObj = response;
                fancybox.open("#completedcalls") 
            });

    };
	
	$scope.openActionPlanModal = function(event, actionPlanStepId) {
        if(event) event.preventDefault();

        menteeFunctions.actionPlanDetails(actionPlanStepId)
            .success(function (response) {
                $scope.actionPlansObj = response;
                fancybox.open("#actionpopupmodal");
            });
	}

    /**
     * Goes through scheduling widget items 
     */
    $scope.scheduleNextPage = function () {
        $scope.menteesavails.currentPage = $scope.menteesavails.currentPage + 1;
    };

    /**
     * Goes through scheduling widget items 
     */
    $scope.schedulePreviousPage = function () {
        $scope.menteesavails.currentPage = $scope.menteesavails.currentPage - 1;
    };

    /**
     * Flag function to check pagination state
     */
    $scope.scheduleIsOnTheFirstPage = function () {
        return $scope.menteesavails.currentPage == 0;
    };

    /**
     * Flag function to check pagination state
     */
    $scope.scheduleIsOnTheLastPage = function () {
        return $scope.menteesavails.currentPage >= $scope.menteesavails.data.length / $scope.menteesavails.pageSize - 1
    };

     /**
     * Goes through summary of calls items 
     */
    $scope.callsSummaryNextPage = function () {
        $scope.activeMentee.summary_of_calls_prop.currentPage = $scope.activeMentee.summary_of_calls_prop.currentPage + 1;
    };

    /**
     * Goes through summary of calls items 
     */
    $scope.callsSummaryPreviousPage = function () {
        $scope.activeMentee.summary_of_calls_prop.currentPage = $scope.activeMentee.summary_of_calls_prop.currentPage - 1;
    };

    /**
     * Flag function to check pagination state
     */
    $scope.callsSummaryIsOnTheFirstPage = function () {
        return $scope.activeMentee.summary_of_calls_prop.currentPage == 0;
    };

    /**
     * Flag function to check pagination state
     */
    $scope.callsSummaryIsOnTheLastPage = function () {
        return $scope.activeMentee.summary_of_calls_prop.currentPage >= $scope.activeMentee.summary_of_calls.length / $scope.activeMentee.summary_of_calls_prop.pageSize - 1;
    };

     /**
     * Goes through summary of tx items 
     */
    $scope.txSummaryNextPage = function () {
        $scope.activeMentee.summary_of_tx_prop.currentPage = $scope.activeMentee.summary_of_tx_prop.currentPage + 1;
    };

    /**
     * Goes through summary of tx items 
     */
    $scope.txSummaryPreviousPage = function () {
        $scope.activeMentee.summary_of_tx_prop.currentPage = $scope.activeMentee.summary_of_tx_prop.currentPage - 1;
    };

    /**
     * Flag function to check pagination state
     */
    $scope.txSummaryIsOnTheFirstPage = function () {
        return $scope.activeMentee.summary_of_tx_prop.currentPage == 0;
    };

    /**
     * Flag function to check pagination state
     */
    $scope.txSummaryIsOnTheLastPage = function () {
        return $scope.activeMentee.summary_of_tx_prop.currentPage >= $scope.activeMentee.summary_of_tx.length / $scope.activeMentee.summary_of_tx_prop.pageSize - 1
    };

    $scope.schedulingAction = function(dateReceived, timeReceived) {
        var dateAndTime = dateReceived + ' ' + timeReceived;
        if($scope.schedulingWidget.mode == 1) $scope.rescheduleCallBlock(dateAndTime);
        else $scope.scheduleCallBlock(dateAndTime);
    }

    $scope.rescheduleCallBlock = function(dateAndTime) {
      var confirmBox = confirm("Are you sure you want to reschedule for " + dateAndTime + "?")
      if(confirmBox == true) {
        queue.rescheduleCall(dateAndTime, $scope.activeQueueId)
          .success(function (response) {
            if(response.response == 'succeeded') { 
                $scope.schedulingWidget.pullInSchedulingWidget = 0;
                $scope.activeMentee.at_a_glance.next_call_datetime = response.queue_array.datetime_formatted;
                $scope.activeMentee.at_a_glance.next_call_queue_id = response.queue_array.new_queue_id;
                $scope.schedulingWidget.scheduled_ind = 1;
                alert("The call has been rescheduled. Please be sure to login at the rescheduled time.");
            }
          });
      }
    };

    $scope.scheduleCallBlock = function(dateAndTime) {
      var confirmBox = confirm("Are you sure you want to schedule for " + dateAndTime + "?")
      if(confirmBox == true) {
        queue.scheduleNextCallBlock(dateAndTime, $scope.activeMentee.candidate_id, $scope.ambassadorId)
          .success(function (response) {
            if(response.response == 'succeeded') { 
                $scope.schedulingWidget.pullInSchedulingWidget = 0;
                $scope.schedulingWidget.mode = 1;
                $scope.activeMentee.at_a_glance.next_call_datetime = response.queue_array.datetime_formatted;
                $scope.activeMentee.at_a_glance.next_call_queue_id = response.queue_array.new_queue_id;
                $scope.refreshCandidateData();
                $scope.schedulingWidget.scheduled_ind = 1;
                alert("The call has been scheduled. Please be sure to login at the rescheduled time.");
            }
          });
      }
    };

    $scope.openRescheduleCall = function () {
      var confirmBox = confirm("You should only pick other times if you have spoken with the mentee and he/she has agreed to speak at other times. Otherwise, pick a time from the reschedule widget. Has the mentee agreed to the time you are selecting?");
      if(confirmBox) fancybox.open('#reschedule_call_popup');
      else alert("Please contact the administrator or pick a time from the schedule widget above.");
    };

    $scope.determineWidgetName = function() {
        if($scope.schedulingWidget.mode == 1) return 'Rescheduling'; else return 'Scheduling';
    };

    $scope.showDetails = function(e){
        title_element = e.target;
        content_element = angular.element(title_element).parents(".grid-title").next();
        content_element.toggleClass("hidden");
    }

    $scope.setTaskListMode = function(event, mode) {
        if(event) event.preventDefault();

        switch(mode) {
            case 'all':
                $scope.taskList.activeHeader = 'All Open Items';
                break;
            case 'missedCalls':
                $scope.taskList.activeHeader = 'Missed Calls';
                break;
            case 'openWorkbooks':
                $scope.taskList.activeHeader = 'Open Workbooks';
                break;
            case 'forgottenMentees':
                $scope.taskList.activeHeader = 'Forgotten Mentees';
                break;
            default:
                $scope.taskList.activeHeader = 'All Open Items';
        }

        $scope.taskList.mode = mode;
    }

    $scope.popTaskItem = function(event, listSet, candidateId, queueId) {
        if(event) event.preventDefault();

        switch(listSet) {
            case 'missedCalls':
                $scope.taskList.missedCalls = _.reject($scope.taskList.missedCalls, function(listItem) { return listItem.candidate_id = candidateId });
                break;
            case 'openWorkbooks':
                $scope.taskList.openWorkbooks = _.reject($scope.taskList.openWorkbooks, function(listItem) { return listItem.queue_id == queueId });
                break;
            case 'forgottenMentees':
                $scope.taskList.forgottenMentees = _.reject($scope.taskList.forgottenMentees, function(listItem) { return listItem.candidate_id == candidateId });
                break;
        }
    }

    $scope.init();
});
