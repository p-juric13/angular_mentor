onetoone.Controllers.controller('callCtrl',
function ($scope, $timeout, config, string, pendingServerInteractions, queue, fancybox, notifications) {
    'use strict';
		
    $scope.call = {
        state: {
            mode: "snapshot" // snapshot | talking_points | goals | notes
        },
        options: {
            mark_complete: 1
        }
    };
		
		$scope.talkingpoint = {
      show_topics_ind: null,
      relevant_topics: [],
      relevant_topics_for_next: [],
      selected_topic: null,
      selected_topic_index: null,
      selected_topic_for_next: null,
    };
		
		$scope.menteesavails = {
      activeindex : 0,
      currentPage : 0,
			pageSize : 10,
			// page: 1,
			data: []
		}
		
    $scope.info = {};
    $scope.history = null;
		$scope.activities = [];
		
    var defaultCallParams = {
        queueId: null,
        notes: '',
        answers: {}
    };

    $scope.$watch('talkingpoint.selected_topic', function (newValue, oldValue) {
          // there is a value to save
        if (!newValue || !newValue.id) return false;

        // this is not an init change
        if((!_(oldValue).isObject() && _(newValue).isObject() && !($scope.info.hasOwnProperty('script') && $scope.info.script !== null)) || (_(oldValue).isObject() && _(newValue).isObject() && ($scope.info.hasOwnProperty('script') && $scope.info.script !== null))) null;
        else return false;

        // change actually happened
        if (_(newValue).isEqual(oldValue)) return false;

        $scope.talkingpoint.show_topics_ind = 0;

        // Update the relevant topics for the next call by eliminating the currently selected one.
        // $scope.talkingpoint.relevant_topics_for_next = $scope.talkingpoint.relevant_topics;
        $scope.talkingpoint.relevant_topics_for_next = _.filter($scope.talkingpoint.relevant_topics, function(topic) { return topic.id != newValue.id });
        $scope.callParams.answers = [];

        // Get the talking points and questions related to that node from the 
        queue.getTalkingPointsQuestions(newValue.id, $scope.info.candidate_profile.candidate_program_node_id)
          .success( function (response) { 
            $scope.info.script = response.script;
            $scope.info.script = $scope.transformScriptIntoCheckboxes($scope.info.script);
            $scope.info.questions = response.questions;
            _(response.questions).each( function (element, index, list) {
                  $scope.callParams.questions[index] = element.question_id;
            });
          });
    }, true); 

    // Underscore JS workaround for getting the index of array of objects
    // save a reference to the core implementation
    var indexOfValue = _.indexOf;

    // using .mixin allows both wrapped and unwrapped calls:
    // _(array).indexOf(...) and _.indexOf(array, ...)
    _.mixin({

        // return the index of the first array element passing a test
        indexOf: function(array, test) {
            // delegate to standard indexOf if the test isn't a function
            if (!_.isFunction(test)) return indexOfValue(array, test);
            // otherwise, look for the index
            for (var x = 0; x < array.length; x++) {
                if (test(array[x])) return x;
            }
            // not found, return fail value
            return -1;
        }

    });


    /**
     * Goes through queue items table
     */
    $scope.nextPage = function () {
        $scope.menteesavails.currentPage = $scope.menteesavails.currentPage + 1;
    };

    /**
     * Goes through queue items table
     */
    $scope.previousPage = function () {
        $scope.menteesavails.currentPage = $scope.menteesavails.currentPage - 1;
    };

    /**
     * Flag function to check pagination state
     */
    $scope.isOnTheFirstPage = function () {
        return $scope.menteesavails.currentPage == 0;
    };

    /**
     * Flag function to check pagination state
     */
    $scope.isOnTheLastPage = function () {
        return $scope.menteesavails.currentPage >= $scope.menteesavails.data.length / $scope.menteesavails.pageSize - 1
    };

    // $scope.updateSelectedTopic = function(selectedTopic) {
    //   // $scope.talkingpoint.selected_topic = selectedTopic;
    //   alert(selectedTopic);
    // }

    /**
     * Switches tab inside call info based on its name
     *
     * @param {string} name
     */
    $scope.switchTab = function (name) {
        $scope.call.state.mode = name;
    };

    /**
     * Used to determine if tab is active
     *
     * @param name
     * @returns {boolean}
     */
    $scope.isTabActive = function (name) {
        return $scope.call.state.mode === name;
    };

    /**
     * transformScriptIntoCheckboxes
     *
     * Transforms script string to array of options
     *
     * @param script
     * @returns {*}
     */
    $scope.transformScriptIntoCheckboxes = function (script) {
        return _(string.splitextra(script)).map(function (row) {
            var namearray = [];
            namearray[config.STRING_DELIMITER] = 's_checkbox';
            namearray[config.STRING_BLD_HD_DELIMITER] = 's_bold_header';
            namearray[config.STRING_SUB_HD_DELIMITER] = 's_sub_header';
            namearray[config.STRING_BULLET_DELIMITER] = 's_bullet';
						namearray[config.STRING_HINT_DELIMITER] = 's_hint';
            return {
                value: row.text,
                checked: false,
                type: namearray[row.type]
            }
        });
    };

    /**
     * handleSavedData
     *
     * Populates saved data from backend into scope
     *
     * @param data
     */
    $scope.handleSavedData = function (data) {
      if (!data) return;

      // populate notes
      if (data.notes) $scope.callParams.notes = data.notes;
      if (data.scribble) $scope.callParams.scribble = data.scribble;

      if($scope.info.questions.length > 0) {
      // populate questions
        if (data.q1_response && $scope.info.questions.length > 0) $scope.callParams.answers[0] = data.q1_response;
        if (data.q2_response && $scope.info.questions.length > 1) $scope.callParams.answers[1] = data.q2_response;
        if (data.q3_response && $scope.info.questions.length > 2) $scope.callParams.answers[2] = data.q3_response;
        if (data.q4_response && $scope.info.questions.length > 3) $scope.callParams.answers[3] = data.q4_response;
        if (data.q5_response && $scope.info.questions.length > 4) $scope.callParams.answers[4] = data.q5_response;
        // $scope.info.questions[4].question_id
      }
    };

    $scope.getHistoryPage = function (event, page) {
        if (event) event.preventDefault();

        queue.callHistory($scope.callParams.queueId, $scope.ambassadorId, page)
            .success(function (response) {
                $scope.history = response;
                $scope.history.page = page;
                $scope.history.pages = Math.ceil(response.total / 3);
            });

        queue.callTransactions($scope.ambassadorId, $scope.info.candidate_id)
             .success(function (response) {
                 $scope.activities = response;
             });

    };

		$scope.showNotesModal = function(item){
			if(item.note=="No notes"){
				return false;
			}else{
				angular.element("#notecontent").html(item.note);
				fancybox.open("#notesModal");
			}
		}
		
    /**
     * triggers call info save action
     */
    $scope.callSave = (function () {
        // public
        var self = {
            progress: false,
            message: '',
            status: null,

            start: function () {
                // reformat questions into object with id & value properties
                // $scope.callParams.questions_formatted = [];
                // _.each($scope.callParams.answers, function(value, key, list) {
                //   $scope.callParams.questions_formatted.push({'id': key, 'answer': value});
                // });

                self.progress = true;
                self.message = '';
                self.status = null;
            },
            success: function () {
                self.progress = false;
                self.message = 'Saved';
                self.status = true;
                self.clean();
            },
            error: function () {
                self.progress = false;
                self.message = 'Save failed';
                self.status = false;
                self.clean();
            },
            clean: function (timeout) {
                if (timeout === undefined) timeout = 1000;

                var clean = function () {
                    self.progress = false;
                    self.message = '';
                    self.status = null;
                };

                if (timeout > 0) {
                    $timeout(clean, timeout);
                } else {
                    clean();
                }
            }
        };

        return self;
    }());

    var saveCall = function () {
        $scope.callSave.start();

        queue.save($scope.callParams)
            .success(function () {
                $scope.callSave.success();
            })
            .error(function () {
                $scope.callSave.error();
            });
    };

    $scope.saveCallParams = saveCall;

    // auto saving
    $scope.$watch('callParams', function (newValue, oldValue) {
        // there is a value to save
        if (!newValue || !newValue.queueId) return false;

        // this is not an init change
        if (!_(oldValue).isObject()) return false;
        if (_(oldValue.answers).isEmpty()) return false;

        // change actually happened
        if (_(newValue).isEqual(oldValue)) return false;

        pendingServerInteractions.add('queue.save_' + $scope.callParams.queueId, saveCall);
    }, true);

    $scope.initiateCall = function () {
        queue.initiateCall($scope.callParams.queueId);
    };

    $scope.openRescheduleCall = function () {
      alert("You should only pick other times if you have spoken with the mentee and he/she has agreed to speak at other times. Otherwise, pick a time from the reschedule widget.");
      fancybox.open('#reschedule_call_popup');
   //    if($scope.call.options.scheduling_next == 0) {
   //      notifications.add(
   //        angular.extend(
   //          angular.copy(config.EVENT[18]),
   //          {queue_id: $scope.queue.activeId }
   //        )
   //        );
   //    } 
   //    else{
			// 		fancybox.open('#reschedule_call_popup');
			// } 
    };

    $scope.openScheduleNextCall = function () {

    queue.checkCallLength($scope.queue.activeId)
        .success(function (response) {
          if(response.call_lengths > 0) {
            if($scope.info.candidate_profile.last_call_ind === 1) {
              notifications.add(
                angular.extend(
                  angular.copy(config.EVENT[14]),
                  {queue_id: $scope.queue.activeId }
                )
                );
            } 
            else{
      				fancybox.open('#schedule_next_call_new_popup');
      			}
          } else {
            alert("You have not yet had a call that is greater than five minutes in length. You may be looking to reschedule the call rather than schedule the next call. Please use the reschedule widget on the 'Snapshot' page to reschedule your call.");
            notifications.add({
              message  : "You have not yet had a call that is greater than five minutes in length. You may be looking to reschedule the call rather than schedule the next call. Please use the reschedule widget on the 'Snapshot' page to reschedule your call.",
              forceOne : true,
              queue_id : $scope.queue.activeId
            });
          }
        });
    };

    $scope.editPhoneNumber = function () {
			fancybox.open('#edit_phone_number_popup');
    };

    function refreshTopicsForNext() {
      if($scope.talkingpoint.selected_topic && $scope.talkingpoint.selected_topic.hasOwnProperty('id') && $scope.talkingpoint.selected_topic.id != null) $scope.talkingpoint.relevant_topics_for_next = _.filter($scope.talkingpoint.relevant_topics_for_next, function(topic) { return topic.id != $scope.talkingpoint.selected_topic.id });
    }

    $scope.markCallComplete = function (event) {
        if(event) event.preventDefault();

        // at least 3 questions should be filled to mark call complete
        var numberOfAnswers = _($scope.callParams.answers).reduce(function (memo, answer) {
            return memo + (answer ? 1 : 0);
        }, 0);

        if (numberOfAnswers < 3 && $scope.info.candidate_profile.last_call_ind != 1) {
            notifications.add({
                message  : 'In order to mark call complete, please answer at least 3 questions.',
                forceOne : true,
                queue_id : $scope.queue.activeId
            });
            return false;
        }

        // Check if a call has been rescheduled; if so, mark_complete would be disabled.
        if($scope.call.options.mark_complete == 0) {
          notifications.add({
                message  : 'You just rescheduled this call and so cannot mark it complete at this time. Contact yourfriends@1to1mentor.org for assistance.',
                forceOne : true,
                queue_id : $scope.queue.activeId
            });
            return false;
        }

        var pushNextTopic = $scope.pickNextTopic();
        if(!pushNextTopic) {
          $scope.callComplete();
        }
    };

    $scope.pickNextTopic = function() {
       if($scope.talkingpoint.selected_topic_for_next === null) {
          refreshTopicsForNext();
          fancybox.open('#pick_next_topic');
          return true;
       } else return false;
    };

    $scope.callComplete = function(event) {
        if(event) event.preventDefault();
        
      // Check if there is a call at least 5 minutes in length recorded on the backend before allowing the user to mark the call complete
        queue.checkCallLength($scope.queue.activeId)
          .success(function (response) {
            if(response.call_lengths > 0) {
              queue.markCallSuccess($scope.callParams.queueId, $scope.talkingpoint.selected_topic_for_next.id)
              .success(function (response) {
                  $.fancybox.close();
                  if(response.response == 'no_transactions') alert("You don't have any successful attempts. We cannot mark a call complete until you have spoken to the mentee.");
                  else if(response.response == 'tutorial_completed') document.location.href = '/queuefunctions/redirect_queue_tutorial';
                  else if(response.response == 'call_rescheduled') {
                    notifications.add(
                      angular.extend(
                        angular.copy(config.EVENT[19]),
                        {queue_id: $scope.queue.activeId }
                      )
                    );
                    alert("You have rescheduled this call and cannot mark it complete.");
                  }
                  else { 
                    $scope.notification.displayInd = 1;
                    $scope.notification.message = "We've marked the call complete! If you've scheduled the next call, then you'll see the entry below (click on 'All'). Otherwise, we have queued up the next call based on both of your availability. You may reschedule that call to pick a better date/time.";
                    $scope.gotoQueueView(1);
                    // alert("The call has been rescheduled. Please be sure to login at the rescheduled time.");
                  }
              });
            } else {
              $.fancybox.close();
              alert("You have not yet had a call that is greater than five minutes in length; please be sure you have properly attempted and completed a call before marking it complete.");
              notifications.add({
                message  : 'You have not yet had a call that is greater than five minutes in length; please be sure you have properly attempted and completed a call before marking it complete.',
                forceOne : true,
                queue_id : $scope.queue.activeId
              });
              return false;
            }
          });
    }

    // define and fire init
    $scope.init = function () {
        var queueId = $scope.queue.activeId;

        $scope.notification.displayInd = 0;

        $scope.callParams = angular.copy(defaultCallParams);
        $scope.callParams.queueId = queueId;

        queue.callInfo(queueId)
            .success(function (response){
                $scope.info = response;

                $scope.programNode = response.candidate_profile.candidate_program_node_id;

                $scope.candidateProgramId = response.candidate_profile.candidate_program_id;

                queue.getProgramTopics($scope.info.candidate_profile.candidate_program_id, $scope.info.candidate_profile.candidate_program_node_id)
                    .success(function (response) {

                      if($scope.info.candidate_profile.program_node_id !== null) { 
                        $scope.info.script = $scope.transformScriptIntoCheckboxes($scope.info.script);
                        // angular.extend($scope.talkingpoint.relevant_topics_for_next, response);
                        angular.extend($scope.talkingpoint.relevant_topics, response);
                        $scope.talkingpoint.selected_topic_index = _.indexOf($scope.talkingpoint.relevant_topics, function(topic) { return topic.id == $scope.info.candidate_profile.program_node_id; });
                        if(!$scope.talkingpoint.selected_topic_index) $scope.talkingpoint.selected_topic_index = 0;
                        //  { 
                        //   id: $scope.info.candidate_profile.program_node_id,
                        //   title: $scope.info.candidate_profile.program_node_title
                        // };
                        $scope.talkingpoint.show_topics_ind = 0;
                        angular.extend($scope.talkingpoint.relevant_topics_for_next, response);
                        // $scope.talkingpoint.relevant_topics_for_next = response;
                      } else {
                        $scope.talkingpoint.show_topics_ind = 1;
                        // $scope.talkingpoint.relevant_topics = response;
                        // angular.extend($scope.talkingpoint.relevant_topics_for_next, response);
                        // $scope.talkingpoint.relevant_topics_for_next = response;
                      }
                      $scope.talkingpoint.relevant_topics = response;
                    });

                $scope.talkingpoint.selected_topic_for_next = response.logistical_factors.next_topic;

                queue.getMenteeAvailability($scope.info.candidate_id, $scope.queue.activeId)
                    .success(function (response) { 
                      $scope.menteesavails.data = response;
                      $scope.menteesavails.pages = Math.ceil($scope.menteesavails.data.length / $scope.menteesavails.recordnum)
                    });

                $scope.callParams.questions = {};

                // init properties to store questions values
                _(response.questions).each( function (element, index, list) {
                      $scope.callParams.answers[index] = null;
                      $scope.callParams.questions[index] = element.question_id;
                });
                if(response.saved_content) $scope.handleSavedData(response.saved_content);
                $scope.getHistoryPage(false, 1);
            });

        $scope.setBreadcrumbs([{
            title: "Call Workbook",
            action: function () {
                $scope.openCall($scope.callParams.queueId);
            }}]);
    };

		
		$scope.showAddModal = false;
		$scope.showAddTalkingPointTip = function(){
			$scope.showAddModal = !$scope.showAddModal;
			/*angular.element(".addTalkingPointModal").addClass("showtooltip");
			angular.element("#showAddTalkingbtn").text("Cancel");*/
		}
		
    $scope.rescheduleCallBlock = function(dateReceived, timeReceived) {
      var dateAndTime = dateReceived + ' ' + timeReceived;
      var confirmBox = confirm("Are you sure you want to reschedule for " + dateAndTime + "?")
      if(confirmBox == true) {
        queue.rescheduleCall(dateAndTime, $scope.queue.activeId)
          .success(function (response) {
            if(response.response == 'succeeded') { 
              var tempCandArr = _.findWhere($scope.queue.candidates, {'id': $scope.queue.activeId});
              tempCandArr.contactHistory. push(response.queue_array.contactHistory[0]);
              response.queue_array.contactHistory = tempCandArr.contactHistory;
              angular.extend(_($scope.queue.candidates).findWhere({'id': $scope.queue.activeId}), response.queue_array);
              $scope.queue.mode = 'list';
              alert("The call has been rescheduled. Please be sure to login at the rescheduled time.");
            }
          });
      }
    };
		
    $scope.updateEndTime = function(itemIndex) {
       var startTimeIndex = $scope.timeItems[itemIndex].startTime.id;
      $scope.timeItems[itemIndex].endTime = $scope.hours[startTimeIndex + 1];
    };

		$scope.opendropdown = function (indexnumber){
			var list=angular.element("#dropdownlist_"+indexnumber);
			angular.element(".dropdownlist").not(list).addClass('hidden');
	    list.toggleClass('hidden');
			return false;
	  };
    // fire init call when controller loaded
    $scope.init();
});
