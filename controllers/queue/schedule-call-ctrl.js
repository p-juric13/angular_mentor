onetoone.Controllers.controller('scheduleCallCtrl',
function ($scope, config, queue, $location, timezones, notifications) {
  'use strict';

  // views management
  $scope.state = $scope.state || {};

  $scope.state.view = 'form'; // form, confirmation

  var now = new Date();

  $scope.hours = config.SCHEDULE_HOURS;

  $scope.timezones = config.TIMEZONES;
  $scope.timezone = timezones.findByOffset($scope.timezoneOffset);

  // $scope.$watch('otherTime1', function (newValue) {
  //       console.log("newValue");
  //      if (!newValue) return;
  //  });


  // config item to display time picking interface
  var defaultTimeItem = function () {
    var itemIndex = $scope.timeItems.length;
    var hoursLength = $scope.hours.length;

    return angular.copy({
      index: itemIndex,
      date: null,
      minTime: null,
      startTime: $scope.hours[0],
      endTime: null,

      datepickerConfig: {
        minDate: now,
        dateFormat: 'mm/dd/yy',
        onSelect: function (datetext, obj) {
          $scope.timeItems[itemIndex].minTime =
            $scope.getMinTime(datetext, moment(), itemIndex);
        }
      },

      startTimeFilter: function (item) {
        return ($scope.timeItems[itemIndex].minTime
          ? item.id > $scope.timeItems[itemIndex].minTime.id : true)
          && item.id < hoursLength;
      },

      endTimeFilter: function (item) {
        return (item.id > $scope.timeItems[itemIndex].startTime.id)
          && item.id > 1
          && item.id <= ($scope.timeItems[itemIndex].startTime.id + config.NUMBER_OF_SCHEDULE_TIMESLOTS_TO_PICK_AT_ONCE);
      },

      isCompleted: function () {
        var currentItem = $scope.timeItems[itemIndex];
        return !!currentItem.date
          && !!currentItem.startTime
          && !!currentItem.endTime;
      }
    });
  };

  /**
   * Datepicker callback. Updates timepickers associated to this date
   * @param datetext - 'yyyy-mm-dd'
   * @param obj
   * @param itemIndex - index of this date in timeItems array
   */
  $scope.getMinTime = function (datetext, nowMoment, itemIndex) {
    var formattedMoment = moment(nowMoment).minutes(Math.ceil(nowMoment.minutes() / 30) * 30);
    if (nowMoment.format('YYYY-MM-DD') == datetext) { // guess if this is today
      var found = _($scope.hours).find(function (item) { return item.label == formattedMoment.format('hh:mma'); });
      return found ? found : $scope.hours[$scope.hours.length - 1];
    } else {
      return $scope.timeItems[itemIndex].minTime = null;
    }
  };

  $scope.verifyPopulated = function() {
    var verified = false;
    for(var x = 0; x <= 3; x++) {
      if($scope.timeItems[x]) {
        if ($scope.timeItems[x].date || $scope.timeItems[x].startTime || $scope.timeItems[x].endTime) verified = true; 
      } 
    }

    return verified;
  }

  // switches view to confirmation screen
  $scope.continueToConfirmation = function () {
    // Verify that at least one date, startTime, and endTime is fully populated
    if(!$scope.checkForCompletedItems()) {
      alert("You must select at least one date and time.");
      return false;
    }

    if ($scope.newCall == 1) {
      $scope.state.view = 'pick_topic';
    } else {
      $scope.state.view = 'confirmation';
    }

  };

  // switches view to pick topic screen
  $scope.continueToPickTopic = function () {
    if ($scope.checkForCompletedItems()) {
      $scope.state.view = 'confirmation';
    }
  };

  $scope.skipTopic = function () {
      $scope.state.view = 'confirmation';
  };

  // switches view back to form screen
  $scope.returnToForm= function () {
    $scope.state.view = 'form';
  };

  $scope.returnToTopic = function() {
    $scope.state.view = 'pick_topic';
  }

  $scope.submitData = function () {
    $.fancybox.close()
    if($scope.queue.mode == 'mentee-list') {
      var result = {
        queue_id: $scope.activeQueueId,
        candidate_id: $scope.activeMentee.candidate_id,
        ambassador_id: $scope.ambassadorId,
        times: []
      };
    } else {
      var result = {
        queue_id: $scope.queue.activeId,
        date_ind: 1, // for now it's all dates
        // program_node: $scope.programNode,
        candidate_id: $scope.info.candidate_id,
        // ambassador_id: $scope.ambassadorId,
        times: []
      };
    }

    _($scope.timeItems).each(function (item) {
      if (!item.isCompleted()) return;
      result.times.push({
        date_day: moment(item.date).format('YYYY-MM-DD'),
        time_start: item.startTime.label,
        time_end: item.endTime.label
      });
    });

    // handle different calls for scheduling new call and updating existing one
    if ($scope.newCall) {// new call

      if($scope.talkingpoint.selected_topic_for_next && $scope.talkingpoint.selected_topic_for_next.hasOwnProperty('id')) result.next_topic = $scope.talkingpoint.selected_topic_for_next.id;

      queue.scheduleNewCall(result)
        .success(function (response) {
            $scope.closeScheduleNewCall();
            if($scope.queue.mode == 'mentee-list') {
                alert("The call has been scheduled.");
            } else {
              if(response.response == 'completed') { 
                notifications.add(
                angular.extend(
                  angular.copy(config.EVENT[14]),
                  {queue_id: $scope.queue.activeId }
                )
                );
              } else if(response.response == 'one_call_left') {
                notifications.add(
                angular.extend(
                  angular.copy(config.EVENT[16]),
                  {queue_id: $scope.queue.activeId }
                )
                );
              } else {
                notifications.add(
                angular.extend(
                  angular.copy(config.EVENT[11]),
                  {queue_id: $scope.queue.activeId }
                )
                );
              }
            }
        });

    } else {// reschedule this call
      queue.saveRescheduledCall(result)
        .success(function (response) {
          if(response.response == 'succeeded') {
            // $scope.callParams.queueId = response.new_queue_id;
            // $scope.queue.activeId = response.new_queue_id;

            // $scope.call.options.mark_complete = 0;

            $scope.closeRescheduleCall();
            if($scope.queue.mode == 'mentee-list') {
              $scope.schedulingWidget.pullInSchedulingWidget = 0;
                $scope.activeMentee.at_a_glance.next_call_datetime = response.queue_array.datetime_formatted;
                $scope.activeMentee.at_a_glance.next_call_queue_id = response.queue_array.id;
                $scope.activeMentee.at_a_glance.queue_date_passed = 0;
                alert("The call has been rescheduled. Please be sure to login at the rescheduled time.");
            } else {
              var tempCandArr = _.findWhere($scope.queue.candidates, {'id': $scope.queue.activeId});
              tempCandArr.contactHistory. push(response.queue_array.contactHistory[0]);
              response.queue_array.contactHistory = tempCandArr.contactHistory;
              angular.extend(_($scope.queue.candidates).findWhere({'id': $scope.queue.activeId}), response.queue_array);
              $scope.queue.mode = 'list';
              alert("The call has been rescheduled. Please be sure to login at the rescheduled time.");
            }
            // notifications.add(
            // angular.extend(
            //   angular.copy(config.EVENT[12]),
            //   {queue_id: $scope.queue.activeId }
            // )
            // );
          } else {
            if($scope.queue.mode == 'mentee-list') {
              alert("We apologize- there was an issue in re-scheduling. Please call or e-mail support. Thanks.");
            } else {
              $scope.closeRescheduleCall();
              notifications.add(
              angular.extend(
                angular.copy(config.EVENT[20]),
                {queue_id: $scope.queue.activeId }
              )
              );
            }
          }
        });
      }
    };

  // returns true if there is at least one completed item
  $scope.checkForCompletedItems = function () {
    var completedExists = false;
    _($scope.timeItems).each(function (item) {
      if (completedExists) return;
      completedExists = item.isCompleted();
    });
    return completedExists;
  };

  $scope.updateEndTime = function(itemIndex) {
     var startTimeIndex = $scope.timeItems[itemIndex].startTime.id;
    $scope.timeItems[itemIndex].endTime = $scope.hours[startTimeIndex + 1];
  };

  $scope.closeRescheduleCall = function () {
    $('#reschedule_call_popup').colorbox.close();
  };

  $scope.closeScheduleNewCall = function () {
    $('#schedule_next_call_popup').colorbox.close();
  };

  // fire initial logic
  $scope.timeItems = [];
  $scope.timeItems.push(defaultTimeItem());
  $scope.timeItems.push(defaultTimeItem());
  $scope.timeItems.push(defaultTimeItem());
});