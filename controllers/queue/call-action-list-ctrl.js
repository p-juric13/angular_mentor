onetoone.Controllers.controller('callActionListCtrl',
function ($scope, candidateGoals, $compile, colorbox) {
    'use strict';

    $scope.unsavedActionList = false;
    $scope.hideStepFormButton = false;

    $scope.errors = {
      'confidence': false,
      'name': false,
      'difficult_to_do': false
    };

    $scope.errorExists = false;

    /**
     * Sets step active
     *
     * @param {int} index
     */
    $scope.setActiveStep = function (index) {
        // console.log($scope.unsavedActionList);
        if($scope.step && $scope.step.hasOwnProperty('id') && $scope.unsavedActionList == false) {
          $scope.saveStepEdits();
          if($scope.errorExists === true) {
            alert("Please complete necessary details before switching action plans. See errors in red.")
            return;
          }
        }

        var step = $scope.data.action_plan[index];

        // angular.extend($scope.step, step);
        $scope.step = step;

        $scope.step.datePickerOptions = $scope.getDatePickerOptions($scope.step);

        if (! step && ! step.id) {
            $scope.displayForm = false;
        }

        $scope.step.index_val = index;

        colorbox.resize();
    };

    $scope.setActiveStepInactive = function() {
      // $scope.saveStepEdits();
      $scope.step = null;
    }

    $scope.isUnsavedActionList = function () {
      var unsavedList = _.where($scope.data.action_plan,  { id: 0 });

      if(unsavedList.length > 0) { 
        $scope.hideStepFormButton = true;
      }
    };

    /**
     * Overwrites copy of current step with initial data from action_list array
     */
    // $scope.cancelStepEdits = function (index, outsideInd, event) {
    //     if (event) {
    //         event.stopPropagation();
    //         event.preventDefault();
    //     }

    //     // angular.extend($scope.step, _($scope.data.action_plan).findWhere({ id: $scope.step.id }));
    //     // if(stepId) { 
    //     //   var removeStepId = stepId;
    //     //   alert(removeStepId);
    //     // }
    //     // else var removeStepId = $scope.step.id;
    //     var r = window.confirm("Are you sure you want to cancel this item. Hitting 'Yes' will delete it.");
    //     if(r) {
    //       // Delete the action list item from the database
    //       // var tempIndex = _($scope.data.action_plan).findWhere({ id: removeStepId });
    //       $scope.removeActionPlan(index);

    //       // $scope.data.action_plan.splice(tempIndex, 1); 
    //       if(outsideInd != 1 && tempindex > 0) angular.extend($scope.step, $scope.data.action_plan[tempIndex - 1]);
    //       $scope.hideStepFormButton = false;
    //     } else {
    //       return;
    //     }
    // };

    $scope.cancelStepEdits = function (event) {
        if (event) {
            event.stopPropagation();
            event.preventDefault();
        }

        if(!$scope.step) {
          alert("There's nothing to remove. You haven't named the action list yet. Click either 'Cancel' or 'Add' on the menu to the left.");
          return;
        }

        var r = window.confirm("Are you sure you want to cancel this item. Hitting 'Yes' will delete it.");
        if(r) {
          $scope.removeActionPlan($scope.step.index_val);

          if($scope.step.index_val > 0) { 
            // angular.extend($scope.step, $scope.data.action_plan[$scope.step.index_val - 1]);
            $scope.step = $scope.data.action_plan[$scope.step.index_val - 1];
          }

          $scope.hideStepFormButton = false;

        } else {
          return;
        }
    };

    /**
     * Overwrites copy of current step with initial data from action_list array
     */
    $scope.saveStepEdits = function (saveButtonClicked) {
        if(!$scope.step) {
          alert("Please add a name for the action list first by clicking the 'Add' button on the left side.");
          return;
        }
       
        $scope.actionListFormValidation();

        if (!$scope.errorExists) {
          // alert("error doesnt exist");
          angular.extend(_($scope.data.action_plan).findWhere({ id: $scope.step.id }), $scope.step);

          var stepToSave = candidateGoals.cleanStepPresentationData($scope.step);
          var stepId = $scope.step.id;

          candidateGoals.saveActionPlan($scope.candidateProgramId, $scope.data.step.id, stepToSave)
              .success(function (createdActionPlan) {

                  angular.extend(_($scope.data.action_plan).findWhere({ id: stepId }), createdActionPlan);
                  // if($scope.step.id != 0) 
                  
                  if($scope.step.id != 0) angular.extend($scope.step, _($scope.data.action_plan).findWhere({ id: $scope.step.id }));
                  else { 
                    $scope.step.id = createdActionPlan.id;
                  }
                  $scope.unsavedActionList = false;
                  $scope.hideStepFormButton = false;

                  // Increment the steps array step.action_plan_count parameter by 1
                  if(createdActionPlan.new_ind == 1) { 
                    _.each($scope.goals, function(obj){ 
                      _.each(obj.selected_steps, function(inside_obj) {
                        if(inside_obj.id == $scope.data.step.id) inside_obj.action_plan_count = parseFloat(inside_obj.action_plan_count) + 1;
                        return; 
                      })
                    });
                  }

                  if(saveButtonClicked) alert("Action list saved.")
              });
        }
    };

    /**
     * Verifies that necessary form fields are populated
     */
    $scope.actionListFormValidation = function() {
       // @TODO consider using built-in angular validation
        // Validate fields are populated. Specifically, step.difficult_to_do, step.confidence, step.name
        $scope.errors.action_plan_description =         !! ($scope.step.action_plan_description == ''       || $scope.step.action_plan_description === null);
        $scope.errors.difficult_to_do =         !! ($scope.step.difficult_to_do == ''       || $scope.step.difficult_to_do === null);
        $scope.errors.confidence =              !! ($scope.step.confidence == ''            || $scope.step.confidence === null);
        $scope.errors.name =                    !! ($scope.step.name == ''                  || $scope.step.name === null);

        $scope.errorExists = !! ($scope.errors.action_plan_description === true || $scope.errors.difficult_to_do === true || $scope.errors.confidence === true || $scope.errors.name === true);
    }

    /**
     * Creates new active step with entered title
     */
    $scope.addActiveStep = function (title, event) {
      if(event) event.preventDefault();

      if (! title.length) return false;

      $scope.showAddStepForm = false;
      $scope.newStepTitle = '';
      $scope.displayForm = true;
      $scope.unsavedActionList = true;

      // $scope.isUnsavedActionList();

      $scope.data.action_plan.push({
        id: 0,
        name: title,
        action_plan_description: "",
        priority: $scope.data.action_plan.length,
        difficult_to_do: "",
        other_comments: "",
        confidence: 0,
        target_completion_date: null,
        status: 'active'
      });


      $scope.setActiveStep($scope.data.action_plan.length - 1);

      $scope.isUnsavedActionList();
    };

    $scope.points = [
      { r: 10, x: 15, y: 35, interval: null },
      { r: 10, x: 65, y: 35, interval: null },
      { r: 10, x: 115, y: 35, interval: null },
      { r: 10, x: 165, y: 35, interval: null },
      { r: 10, x: 215, y: 35, interval: null },
      { r: 10, x: 265, y: 35, interval: null },
      { r: 10, x: 315, y: 35, interval: null },
      { r: 10, x: 365, y: 35, interval: null },
      { r: 10, x: 415, y: 35, interval: null },
      { r: 10, x: 465, y: 35, interval: null }
    ];

    $scope.lines = [
      { x1: 15, x2: 65,   y: 35 },
      { x1: 65, x2: 115,  y: 35 },
      { x1: 115, x2: 165, y: 35 },
      { x1: 165, x2: 215, y: 35 },
      { x1: 215, x2: 265, y: 35 },
      { x1: 265, x2: 315, y: 35 },
      { x1: 315, x2: 365, y: 35 },
      { x1: 365, x2: 415, y: 35 },
      { x1: 415, x2: 465, y: 35 }
    ];

    /**
     * Handles click on confidence value
     * @param value
     */
    $scope.setConfidence = function (value) {
      $scope.step.confidence = value + 1;
    };

    /**
     * Removes step by its index
     */
    $scope.removeActionPlan = function (index) {
      candidateGoals.removeActionPlan($scope.candidateProgramId, $scope.data.action_plan[index].id)
        .success(function () {
          $scope.data.action_plan.splice(index, 1);

          // Decrement the steps array step.action_plan_count parameter by 1
          _.each($scope.goals, function(obj){ 
            _.each(obj.selected_steps, function(inside_obj) {
              if(inside_obj.id == $scope.data.step.id) inside_obj.action_plan_count = parseFloat(inside_obj.action_plan_count) - 1;
              return; 
            })
          });
      });
    };

    /**
     * Loads action step data
     */
    $scope.init = function (stepId) {
        candidateGoals.fetchActionPlan($scope.candidateProgramId, stepId)
          .success(function (response) {
            // active step data container
            $scope.step = {};
            $scope.displayForm = true;

            $scope.data = response;

            // new step title
            $scope.newStepTitle = '';

            if ($scope.data.action_plan.length) {
              $scope.setActiveStep(0);
            } else {
              $scope.showAddStepForm = true;
            }
        });

    };

    /**
     * Toggle action plan complete status
     *
     * @param {event} event
     * @param {object} step
     */
    $scope.toggleActionCompleted = function (event) {
        if (event) {
            event.stopPropagation();
            event.preventDefault();
        }

        if($scope.step.id == 0) {
          alert("You cannot mark a new action list as complete. Please fill out the details of the action plan first.");
          return;
        }
        // // Verify a form is completed before trying to mark an action item complete.
        // $scope.actionListFormValidation();

        // // If there's an error, display the error.
        // if($scope.errorExists) {

        // }

        if ($scope.step.status === 'completed') {
            $scope.step.status = 'active';
        } else {
            $scope.step.status = 'completed';
        }

        $scope.updateStatus($scope.step.status);

        // candidateGoals.saveStep($scope.candidateProgramId, step.id, { status: step.status });
    };

    $scope.updateStatus = function (status) {
      var tempStep = {
        'id': $scope.step.id,
        'status': status
      };
      candidateGoals.saveActionPlan($scope.candidateProgramId, $scope.data.step.id, tempStep)
        .success(function (createdActionPlan) {
            angular.extend(_($scope.data.action_plan).findWhere({ id: $scope.step.id }), createdActionPlan);
            alert("Action plan status updated!")
        });
    };

    $scope.cancelNewStep = function(event) {
      if (event) {
          event.stopPropagation();
          event.preventDefault();
      }

      $scope.showAddStepForm = false;
      $scope.hideStepFormButton = false;
    };

    /**
     * Used to initialize tooltip plugin with ui-jq directive
     *
     * @param candidate
     * @returns {{tooltipClass: string, position: {my: string, at: string}, content: *}}
     */
    $scope.getActionPlanTooltipOptions = function (type) {
        if(!$scope.data) return;
        var tooltipOption = type + "_tooltip";
        return {
            tooltipClass: "tooltip-call-history",
            position: { my: "right+50 top+15", at: "right bottom" },
            // content: ($compile('<div><ul><li><div class="header">testing</div><div class="content">testing2</div></li></ul>')($scope)).html()
            content: ($compile('<div><ul><li>'+ $scope.data.tooltips[tooltipOption] + '</li></ul></div>')($scope)).html()
        };
    };



});
