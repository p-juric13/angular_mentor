onetoone.Controllers.controller('queue',
function ($scope, $filter, $compile, queue, $http, pusher, fancybox, notifications, config, queueFunctions) {

    var oneDay = 60 * 60 * 24 * 1000,
        oneWeek = oneDay * 7;

    var currentTime = new Date();
    var currentHour = currentTime.getHours();

    var startDay = new Date(0, 0, 0, 0, 0, 0);
    var endDay = new Date(0, 0, 0, currentHour, 0, 0);

    var dayStartDiff = startDay.getTime() - endDay.getTime();

    // var currentDayEndTimestamp = moment().utc().endOf('day').valueOf();
    var currentDayEndTimestamp = moment().endOf('day').valueOf();
    
    $scope.filterOptions = {
        date: [
                    {value:'today', label:'Today'},
                    {value:'week', label:'This Week'},
                    {value:'all', label:'All'},
                ],
        type:[
                    {value:'all', label:'All Mentees'},
                    {value:'new', label:'New Mentees'},
                    {value:'existing', label:'Existing Mentees'}
                ],
        selectedDate: "today",
        selectedType: "all"
    };

    $scope.filtersdropdown = {
        containerCssClass : 'queuedropdown',
    };
    
    $scope.guidancepage = config.ROOT_URL + config.URLS.GUIDANCE.FROMQUEUE;

    /**
     * Queue state object
     */
    $scope.queue = {
        activeId: 0,
        filter: {
            date: 'today',//today, week, all
            type: 'all',//all, new, existing
            searchTerm: {},
            querySubmitted: false,
            searchArea: [],
            query: ''
        },
        candidates: [],
        // currentCount: 0,
        // queue pagination settings
        currentPage: 0,
        pageSize: 10,
        pageSizesAvailable: [10, 15, 25, 50],
        numberOfPages: function (candidates) {
            return Math.ceil(candidates / $scope.queue.pageSize) || 1;
        },
        mode: 'list', // list | call | call-details | mentee-list
        menteeAnsweredCall: 0,
        breadcrumbs: []
    };

    $scope.activateMenteeView = {
        candidateId: null
    }

    $scope.notification = {
        displayInd: 0,
        message: null,
    }

    $scope.checkListMode = function() {
        if($scope.queue.mode != 'mentee-list') return true;
        else return false;
    }
        
    $scope.gotoQueueView = function(refresh){
        $scope.setBreadcrumbs();
        $scope.queue.mode = 'list';
        if(refresh == 1)$scope.init($scope.orgId, $scope.ambassadorId, $scope.timezoneOffset);
    };

    /*
        Go to the list view
        parameters:
            loadMentee- if 1, then activate the specified candidateId in the list view
    */
    $scope.gotoListView = function(loadMentee, candidateId) {
        $scope.queue.mode = 'mentee-list';
        if(loadMentee == 1) {
            $scope.activateMenteeView.active = 1;
            $scope.activateMenteeView.candidateId = candidateId;
        }
    }
        
    /**
     * Sets breadcrumbs
     */
    $scope.setBreadcrumbs = function (breadcrumbs) {
        $scope.queue.breadcrumbs = [{
            title: "Call Queue",
            action: $scope.gotoQueueView
        }];

        if (breadcrumbs) {
            $scope.queue.breadcrumbs = $scope.queue.breadcrumbs.concat(breadcrumbs)
        }
    };

    /**
     * Takes care about filtering queue items to make filters work
     *
     * @param item
     * @returns {boolean}
     */

    function rtrim(str, chars) {
        chars = chars || "\s";
        return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
    }

    $scope.filterQueue = function (item) {
        var valid = true,
            type = $scope.queue.filter.type,
            date = $scope.queue.filter.date,
            query = $scope.queue.filter.query;

        // check item type
        if (type !== 'all') {
            if (type === 'new') {
                valid = item.existing == 0;
            }
            if (type === 'existing') {
                valid = item.existing != 0;
            }
        }

        // check item date
        if (valid && date !== 'all') {
            var timeDiff = (item.timestamp - Date.now());
            if(timeDiff < dayStartDiff && item.status_ind != 200) {
                // do not display older items
                valid = false;
            }
            else if (date === 'today') {
                valid = ((item.timestamp + item.timezone_offset) < (currentDayEndTimestamp)) || item.status_ind == 200;
            }
            else if (date === 'week') {
                valid = (timeDiff < oneWeek) || item.status_ind == 200;
            }
        }

        if (valid && query) {
            var fullName = rtrim(item.first_name + ' ' + item.last_name);
            if(fullName != query) valid = false;
            // var jsonString = $filter('json')(_(item).values()).toLowerCase();
            // valid = jsonString ? jsonString.search(query.toLowerCase()) > -1 : false;
        }

        return valid;
    };

    $scope.queueOrdering = true;

    /*$scope.setDateFilter = function (event, value) {
        if (event) event.preventDefault();
        $scope.queue.filter.date = value;
        $scope.queueOrdering = (value != 'week');
    };*/

    $scope.isDateFilterActive = function (value) {
        return $scope.queue.filter.date === value;
    };

    /*$scope.setTypeFilter = function (event, value) {
        if (event) event.preventDefault();
        $scope.queue.filter.type = value;
    };*/

    $scope.isTypeFilterActive = function (value) {
        return $scope.queue.filter.type === value;
    };

    /**
     * Used to initialize tooltip plugin with ui-jq directive
     *
     * @param candidate
     * @returns {{tooltipClass: string, position: {my: string, at: string}, content: *}}
     */
    $scope.getCallHistoryTooltipOptions = function (candidate) {
        return {
            tooltipClass: "tooltip-call-history",
            position: { my: "right+50 top+15", at: "right bottom" },
            content: ($compile($filter('callHistoryTooltipHtml')(candidate.contactHistory))($scope)).html()
        };
    };

    /**
     * Used to initialize tooltip plugin with ui-jq directive
     *
     * @returns {{tooltipClass: string, position: {my: string, at: string}, content: *}}
     */
    $scope.getActionPlanTooltipDetails = function () {
        return {
            tooltipClass: "tooltip-call-history",
            position: { my: "right+50 top+15", at: "right bottom" },
            content: ($compile($filter('goalActionPlanTooltipHtml')(candidate.contactHistory))($scope)).html()
        };
    };

    /**
     * Goes to call screen
     *
     * @param {int} id
     */
    $scope.openCall = function (id, event) {
        if(event) event.preventDefault();

        // cache dom query after the first call
        queueFunctions.createInmemCall(id)
            .success(function () {
                $scope.queue.activeId = id;
                $scope.queue.mode = 'call';
            });
    };

    /**
     * Goes to call details screen
     * @param id
     */
    $scope.openCallDetails = function (id) {
        queueFunctions.createInmemCall(id)
            .success(function () {
                $scope.queue.activeId = id;
                $scope.queue.mode = 'call-details';
            });
    };

    /**
     * reschedule popup
     * Logic sequence:
        1. Offer dates & times based on when candidate is available.
     */
     $scope.rescheduleCall = function (id) {
       fancybox.open('/queue/other_candidate_times/' + id + '/1/3/1', {type: "ajax", autoDimensions: false, width: 600, height: 200});
     };

    /**
     * opens cancel popup
     * Logic sequence:
        1. Check for when user wants to reschedule call, one of the options of which is to end the relationship altogether.
        2. Reschedule accordingly.
     */
     $scope.cancelCall = function (id) {
       fancybox.open('/queue/cancel_call/' + id, {type: "ajax", autoDimensions: false, width: 500, height: 175});
     };

     /**
     * opens accept popup
     * Logic sequence:
        1. Accept confirmation box (same as default for when user logs in)
     */
     $scope.acceptCall = function (id) {
        alert("accepting");
     };

     /**
     * opens decline popup
     * Logic sequence:
        1. Decline confirmation box (same as default for when user logs in)
     */
     $scope.declineCall = function (id) {
        alert("declining");
     };

     $scope.reopenBox = function () {
        fancybox.open("#call_popup", {padding: 0});
        // $.colorbox({inline: true, href: angular.element('#reschedule_call_popup')});
     };

     $scope.rescheduleCallBox = function () {
        fancybox.open("#call_popup", {padding: 0});
        $.colorbox({inline: true, href: angular.element('#reschedule_call_popup')});
     };

     $scope.initiateCall = function () {
        queue.initiateCall($scope.callParams.queueId);
     };

    /**
     * Initializes queue environment
     *
     * @param {int} orgId
     * @param {int} candidateId
     * @param {int} timezoneOffset
     * @param {int} programNode
     */
    $scope.init = function (orgId, ambassadorId, timezoneOffset, candidateId, queueId) {
        $scope.ambassadorId = ambassadorId;
        $scope.orgId = orgId;

        // save user timezone offset to state
        $scope.timezoneOffset = timezoneOffset;

        if(candidateId) {
            $scope.gotoListView(1, candidateId);
            return;            
        } else if(queueId) {
            $scope.openCall(queueId);
        }

        queue.getData(orgId, ambassadorId)
            .success(function (response) {
                if (_(response).isArray()) {
                    $scope.queue.candidates = _(response).map(function (candidate) {
                        candidate.demographicSummary = $scope.transformSummaryToArray(candidate.demographicSummary);
                        candidate.clinicalSummary = $scope.transformSummaryToArray(candidate.clinicalSummary);
                        return candidate;
                    });

                    $scope.queue.filter.searchArea = $scope.queue.candidates;
                }
            });

        $scope.setBreadcrumbs();
                
        var w=angular.element(window);
        angular.element('.queue-main').css("min-height",w.height()-372);
        // $('#dateOption').select2('val', 'today');

    };

    $scope.refreshCandidateData = function() {
         queue.getData($scope.orgId, $scope.ambassadorId )
            .success(function (response) {
                if (_(response).isArray()) {
                    $scope.queue.candidates = _(response).map(function (candidate) {
                        candidate.demographicSummary = $scope.transformSummaryToArray(candidate.demographicSummary);
                        candidate.clinicalSummary = $scope.transformSummaryToArray(candidate.clinicalSummary);
                        return candidate;
                    });
                }
            });
    }

    /**
     * Transforms array of objects { name:*, value:* }
     * to { headings: [*], values: [*] } arrays
     */
    $scope.transformSummaryToArray = function (summary) {
        var result = {
            headings: [],
            values  : []
        };

        _(summary).each(function (item) {
            result.headings.push(item.name);
            result.values.push(item.value);
        });

        return result;
    };

    /**
     * Pusher events
     */
    $scope.initPusher = function (channelId) {
      pusher.subscribe(channelId);

      // listen to phones functions
      pusher.bind('queue/telephony', function (response) {

        if (config.EVENT.hasOwnProperty(response.msg_ind)) {
          if(!(response.msg_ind == 9 && $scope.queue.menteeAnsweredCall == 1)) {
              notifications.add(
                angular.extend(
                  angular.copy(config.EVENT[response.msg_ind]),
                  response
                )
              );
          }
        } else {
          throw 'Options for event with id ' + response.msg_ind + ' not found';
        }

      });
    };

    /**
     * Goes through queue items table
     */
    $scope.nextPage = function () {
        $scope.queue.currentPage = $scope.queue.currentPage + 1;
    };

    /**
     * Goes through queue items table
     */
    $scope.previousPage = function () {
        $scope.queue.currentPage = $scope.queue.currentPage - 1;
    };

    /**
     * Flag function to check pagination state
     */
    $scope.isOnTheFirstPage = function () {
        return $scope.queue.currentPage == 0;
    };

    /**
     * Flag function to check pagination state
     */
    $scope.isOnTheLastPage = function () {
        return $scope.queue.currentPage >= $scope.queue.candidates.length / $scope.queue.pageSize - 1
    };

    $scope.setTypeFilterNew = function () {
        $scope.queue.filter.query = null;
        $scope.queue.filter.type = $scope.filterOptions.selectedType;
    };
    
    $scope.setDateFilterNew = function () {
        $scope.queue.filter.query = null;
        $scope.queue.filter.date = $scope.filterOptions.selectedDate;
        $scope.queueOrdering = ($scope.filterOptions.selectedDate != 'week');
    };
	
	/**
	* Dropdown functions
	*/
	
	// function closeDropdown(){
	// 	angular.element(".js-datalist").addClass('hidden');
	// 	return false;
    // };
	
	// $scope.openSearchDropdown = function(){
	// 	var list=angular.element("#searchdropdown");
    //  list.toggleClass('hidden');
	// }
	
	// $scope.setSearchFilterType=function(searchArea){
    //  $scope.queue.filter.area = searchArea;
	// 	closeDropdown();
	// 	return false;
	// }

    $scope.searchCurrentArea = function(event) {
        if(event) event.preventDefault();

        if($scope.queue.filter.searchTerm !== null && $scope.queue.filter.searchTerm.title) {
            if($scope.queue.mode == 'list') {
                $scope.queue.filter.date = 'all';
                $scope.queue.filter.type = 'all';
                $scope.queue.filter.query = $scope.queue.filter.searchTerm.title;
            } else if($scope.queue.mode == 'mentee-list') {
                $scope.queue.filter.querySubmitted = true;
            }
        } 
        
    }
});
