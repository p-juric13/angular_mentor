onetoone.Controllers.controller('selfScheduleFilters', function ($scope, $rootScope, config, selfScheduleAPI, calendarFilters) {

    calendarFilters.initScope($scope);

    /**
     * yob slider config
     * @type {Object}
     */
    $scope.ageSlider = {
        filterIndex: 0,
        min: $scope.minAge,
        max: $scope.maxAge,
        config: {
            range: true,
            min: $scope.minAge,
            max: $scope.maxAge,
            values: [ 18, 100 ],
            slide: function (event, ui) {
              $scope.safeApply(function () {
                $scope.ageSlider.min = ui.values[0];
                $scope.ageSlider.max = ui.values[1];
                $scope.filters[$scope.ageSlider.filterIndex].values[0] = ui.values[0];
                $scope.filters[$scope.ageSlider.filterIndex].values[1] = ui.values[1];
              });
            },
            change: function (event, ui) {
              $scope.safeApply(function () {
                $scope.ageSlider.min = ui.values[0];
                $scope.ageSlider.max = ui.values[1];
                $scope.filters[$scope.ageSlider.filterIndex].values[0] = ui.values[0];
                $scope.filters[$scope.ageSlider.filterIndex].values[1] = ui.values[1];
                $scope.activeFilters.age = [ui.values[0], ui.values[1]];
                $scope.defineIfFiltersAreEmpty();
                $scope.calculateMatches();
              });
            }
        }
    };

    /**
     * handles activeFiltersDirty update and populates changes to activeFilters
     * @param valueId
     */
    $scope.filterChanged = function (filterId, valueId) {
        if (valueId == 0) {
            if (filterId == 'age') {
                angular.element('#' + filterId + '-slider').slider("values", [$scope.minAge, $scope.maxAge]);
            } else {
                $scope.activeFilters[filterId] = [];
            }
        } else {
            pushValueToFilters(filterId, valueId);
            $scope.activeFiltersValues.push(valueId);
        }

        $scope.defineIfFiltersAreEmpty();
        $scope.calculateMatches();
    };

    var pushValueToFilters = function (filterId, valueId) {
        $scope.activeFilters[filterId] = [];
        $scope.activeFilters[filterId].push(valueId);
    };

    /**
     * contains properties like filterId|valueId
     * that are equal to number of matching candidates for this pair
     * @type {Object}
     */
    $scope.matchingCandidatesNumber = {};

    /**
     * updates $scope.matchingCandidatesNumber on filters changed
     * @param event
     * @return {Boolean}
     */
    $scope.calculateMatches = function () {
        //filter step by step by filtering out candidates by filter values arrays and group 'em by filters
        angular.extend(
            $scope.matchingCandidatesNumber,
            calendarFilters.calculateMatches(
                $scope.filters,
                calendarFilters.getSelectedCandidatesGroupedByFilters($scope.filters, $scope.candidates, $scope.activeFilters)
            )
        );

        // filter candidates array to get only matching candidates
        $rootScope.matchingCandidates = calendarFilters.getSelectedCandidates($scope.activeFilters, $scope.candidates);

        return false;
    };

    /**
     * maps corresponding filterId, valueId to property in $scope.matchingCandidatesNumber
     * @param filterId
     * @param valueId
     * @return {*}
     */
    $scope.showMatchingCandidates = function (filterId, valueId) {
        return $scope.matchingCandidatesNumber[filterId + '|' + valueId] || 0;
    };

    /**
     * listeners to define if filters should be hidden from page
     */
    $scope.filtersEmpty = true;

    $scope.defineIfFiltersAreEmpty = function () {
        $scope.filtersEmpty = calendarFilters.filtersAreEmpty($scope.activeFilters);
    };

    $scope.defineIfAgeRangeIsDefault = calendarFilters.ageRangeIsDefault;

    /**
     * call backend for JSON data on controller load
     * @param org_id
     * @param candidate_id
     */
    $scope.initData = function (org_id, root_id, type_id, num_filters) {
        selfScheduleAPI.getFiltersData(org_id, root_id, type_id)
            .success(function (response) {
                if (response.filter_data) {
                    $scope.filters = response.filter_data;
                }
                if (response.matching_candidates) {
                    $scope.candidates = response.matching_candidates;
                    //prepare data to be used with gender
                    angular.forEach($scope.candidates, function (candidate, id) {
                        $scope.candidates[id].filters_values.gender = candidate.gender;
                        $scope.candidates[id].values.push(candidate.gender);
                    });
                }

                // push fake yob filter
                $scope.filters.unshift(calendarFilters.getAgeFilter($scope.ageSlider.min, $scope.ageSlider.max));

                $scope.activeFilters.age = [ $scope.ageSlider.min, $scope.ageSlider.max ];

                 //push fake gender filter
                $scope.filters.unshift(calendarFilters.getGenderFilter());

                // filters array was unshifted twice
                $scope.ageSlider.filterIndex = 1;

                $scope.hasCalendar = true;
                $scope.calculateMatches();
            })
            .error(function (response) {
                //console.log(response);
            });
    };

    $scope.showSelectedValue = function (filter) {
      var selectedFiterValues = $scope.activeFilters[filter.id];
      if (selectedFiterValues) {
        var activeValues = _(filter.values).filter(function (value) {
          return _(selectedFiterValues).contains(value.id);
        });
        return _(activeValues).reduce(function (memo, value) {
          return memo + value.value;
        }, '');
      }
    };
});