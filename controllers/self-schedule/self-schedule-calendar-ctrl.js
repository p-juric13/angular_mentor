onetoone.Controllers.controller('selfScheduleCalendar', function ($scope, $rootScope, config, selfScheduleAPI, timezones, calendar, timeslots, fancybox, fullcalendarHelper) {
    'use strict';

    // object keyed by candidate_id containing array of timeslots_ids
    $scope.ambassador_availability = [];

    // array of all timestamps
    $scope.timeslots_slice = [];

    // calendar events object
    $scope.calendarEvents = [];

    // property to track week number
    $scope.calendarWeek = 1;

    // values to store current and prev fullcalendar views start
    $scope.previousViewStart = null;
    $scope.currentViewStart = null;

    // if calendar is confirmed and ready to submit
    $scope.confirmed = false;

    // calendar element
    var calendarElm = angular.element('#calendar-angular');

    // init dates
    var today = new Date();
    var calYear = today.getFullYear();
    var calMonth = today.getMonth();
    var calDate = today.getDate() + 1;

    // calendar config object
    $scope.calendarConfig = {
        axisFormat: "'<span>'h(:mm)'<i>'TT'</i></span>'",
        year: calYear,
        month: calMonth,
        date: calDate,
        firstDay: today.getDay() + 1,
        columnFormat: {
            week: "dddd '<span>'MMMM d'</span>'"
        },

        select: function (start, end, allDay, ev, overlayflag) {
          fullcalendarHelper.selectHandler.call(this, start, end, allDay, ev, overlayflag, calendarElm);
        },

        dayClick: function (date) {
					if($scope.othertime){
						fullcalendarHelper.otherAvailableTimeClickHandler.call(this, date, calendarElm, $scope.calendarEvents);
					}else{
						fullcalendarHelper.dayClickHandler.call(this, date, calendarElm, $scope.calendarEvents);
					}
        },

        eventDrop: function (event, dayDelta, minuteDelta, allDay, revertFunc) {
          fullcalendarHelper.eventDropHandler.call(this, event, dayDelta, minuteDelta, allDay, revertFunc, $scope.calendarEvents);
        },

        eventMouseover: function (event) {
          fullcalendarHelper.eventMouseoverHandler.call(this, event, calendarElm);
        },

        viewDisplay: function (view) {
            $rootScope.safeApply(function () {
              // update calendar events dates when week changed
              $scope.previousViewStart = (!$scope.previousViewStart) ? view.start : $scope.currentViewStart;
              $scope.currentViewStart = view.start;
            });

            fullcalendarHelper.viewDisplay.call(this, calendarElm, $scope.calendarEvents);
        }
    };

  /**
   * helper method to obtain reference for a fiters controller scope
   * @returns {*}
   */
    $scope.filtersCtrl = function () {
        var filtersCtrl = angular.element('#selfScheduleFiltersCtrl');
        if (!filtersCtrl.length) {
            filtersCtrl = angular.element('#scheduleFiltersCtrl');
        }
        return filtersCtrl.scope();
    };

    // update timeslots
    $scope.$watch('currentViewStart', function (newValue) {
        if (!newValue) return;
        //$scope.viewStartWatcher();
    });

    $scope.viewStartWatcher = function () {
      var viewsTimediff = $scope.currentViewStart.getTime() - $scope.previousViewStart.getTime();

      if (viewsTimediff) {
          $scope.timeslots_slice = timeslots.shift($scope.timeslots_slice, viewsTimediff);
          $scope.renderCalendarEvents();
      }

      $scope.updateCalendar();
    };

    /**
     * initiates fullcalendar plugin
     */
    $scope.initCalendar = function () {
        //Default View Options
        var options = { events: $scope.calendarEvents };

        $scope.renderCalendarEvents();
        //extend the options to suite the custom directive.
        angular.extend(options, fullcalendarHelper.defaultOptions, $scope.calendarConfig);

				if($scope.othertime){
					options.disableResizing = false;
					options.eventColor='#FF7700';
				}else{
					options.disableResizing = true;
					options.eventColor='#0E88AF';
				}
        fullcalendarHelper.init(calendarElm, options);
    };


    /**
     * update method that is called on load and whenever the events array is changed.
     */
    $scope.updateCalendar = function update () {
        var options = { events: $scope.calendarEvents };
        //extend the options to suite the custom directive.
        angular.extend(options, fullcalendarHelper.defaultOptions, $scope.calendarConfig);
				
				//fullcalendarHelper.init(calendarElm, options);
        fullcalendarHelper.render(calendarElm);
    };

    /**
     * candidates matched by currently selected filters
     */
    $scope.$watch('matchingCandidates', function (newValue) {
        if (!newValue) return;

        $scope.renderCalendarEvents();
    }, true);

    $scope.renderCalendarEvents = function () {
      $scope.calendarEvents = calendar.generateEvents($scope.matchingCandidates, $scope.ambassador_availability, $scope.timeslots_slice);
      fullcalendarHelper.removeEvents(calendarElm);
      $scope.updateCalendar();
    };

    $scope.submitCalendar = function (event) {
        if ($scope.confirmed === true) {
            // let the form be submitted
            if (!$scope.emailProvided) {
              fancybox.open('#getEmail', {});
            } else {
                angular.element('#set_candidate_schedule').trigger('submit');
            }
            return true;
        }

        // Retrieving calendar slots
        var slotObjects = fullcalendarHelper.clientEvents(calendarElm);

        if (slotObjects.length < 1) {
            if (event) event.preventDefault();
            alert("Please give at least one available timeslot.\n"
                + "You're not committed to it - you can always make yourself 'unavailable' through 'My Hub'.\n"
                + "Plus we'll confirm all call requests with you.");
            return false;
        }

        $scope.result = calendar.populateSlotsAndAmbassadors(slotObjects, $scope.filtersCtrl().activeFilters, true);

        // Checking for overlapping slots
        // If any overlapping events were found, cancel form submission and display alert
        if (calendar.overlappingSlotsExist()) {
            if (event) event.preventDefault();
            alert("You cannot have any overlapping time slots");
            return false;
        }

        if (event) event.preventDefault();
        fancybox.open('#confirmBox', {});
    };

    $scope.confirmEmail = function (event) {
        if (event) event.preventDefault();
        $scope.emailProvided = true;
        $("#calFieldset").append('<input type="hidden" name="email" value="' + $('#email').val() + '">');
        $scope.processRequest();
    };

    $scope.processRequest = function (event) {
        if (event) {
            event.preventDefault();
        }

        $scope.submitCalendar();
    };

    $scope.resetCalendar = function (event) {
        if (event) event.preventDefault();
        fullcalendarHelper.reset(calendarElm);
    };


    // private to keep clean timeslots copy
    var default_timeslots = [];

    /**
     * handles data initialisation
     */
    $scope.load = function (orgId, rootId, typeId, emailProvided, candidateId) {
        $scope.dateModeCalendarData = $scope.dateModeCalendarData || {};
        $scope.emailProvided = !!emailProvided;

        $scope.schedulingParams = {
          orgId: orgId,
          rootId: rootId,
          typeId: typeId,
          candidateId: candidateId
        };

        $scope.getWeekEvents(1, true);
				$scope.candidatetimes = null;
				$scope.otheravailabletimes = null;
				//$scope.othertime = false;
				$scope.othertimeshowflag = true;
    };

    $scope.processResponse = function (response, init) {
      $scope.ambassador_availability = response.ambassador_availability;

      $scope.timeslots_slice = response.timeslots_slice;
      default_timeslots = angular.copy(response.timeslots_slice);

      if ($scope.timezone) {
        $scope.updateTimeslots();
      }
      if (init) {
        $scope.initCalendar();
      } else {
        $scope.updateCalendar();
      }
      $scope.renderCalendarEvents();
    };

    $scope.getWeekEvents = function (week, init) {
      if ($scope.dateModeCalendarData[week]) {
        $scope.processResponse($scope.dateModeCalendarData[week]);
      } else {
        selfScheduleAPI.getSchedulingData(
            $scope.schedulingParams.orgId,
            $scope.schedulingParams.rootId,
            $scope.schedulingParams.typeId,
            $scope.schedulingParams.candidateId,
            week)
          .success(function (response) {
            $scope.dateModeCalendarData[week] = response;
            $scope.processResponse(response, init);
          });
      }
    };

    var triggerWeekUpdate = function () {
      $('#weekNumber').html($scope.calendarWeek);
      $scope.getWeekEvents($scope.calendarWeek);
    };

    $scope.toNextWeek = function () {
        if ($scope.calendarWeek > config.SCHEDULE.ALLOWED_WEEKS_AHEAD) return false;
        // update incremental week number
        $scope.calendarWeek += 1;
        fullcalendarHelper.triggerNext(calendarElm);
        triggerWeekUpdate();
    };

    $scope.toPrevWeek = function () {
        if ($scope.calendarWeek < config.SCHEDULE.ALLOWED_WEEKS_AHEAD) return false;
        // update incremental week number
        $scope.calendarWeek -= 1;
        fullcalendarHelper.triggerPrev(calendarElm);
        triggerWeekUpdate();
    };

  /* --- timezones logic --- */
  // all the available timezones
  $scope.timezones = timezones.list();

  // get user utc offset
  $scope.userTzOffset = timezones.offset();

  $scope.$on('timezones.Ready', function () {
    // default timezone is -5
    $scope.timezone = timezones.getDefault();

    $scope.updateTimeslots();
    $scope.updateCalendar();
    $scope.renderCalendarEvents();
  });

  $scope.updateTimeslots = function () {
      $scope.timeslots_slice = timeslots.shift(default_timeslots, ($scope.timezone.offset - $scope.userTzOffset) * 3600000);
  };

  $scope.$watch('timezone', function (newValue, oldValue) {
      if (!newValue) return;
      $scope.updateTimeslots();
      $scope.renderCalendarEvents();
  });
	
	$scope.$on('changeothertime', function(e) {
		if($scope.othertime){
			var slotObjects = calendarElm.fullCalendar('clientEvents');
			for(var i=0; i<slotObjects.length; i++){
				if(slotObjects[i].editable === false){
					slotObjects.splice(i, 1);
					i = i-1;
				}
			}
			$scope.candidatetimes = slotObjects;
			$scope.initCalendar();
      $scope.renderCalendarEvents();
			
			if($scope.otheravailabletimes){
				fullcalendarHelper.renderEvents(calendarElm, $scope.otheravailabletimes, true);
			}
			fullcalendarHelper.renderEvents(calendarElm, $scope.candidatetimes, false,  false ,'#0E88AF');
		}else{
			var slotObjects = calendarElm.fullCalendar('clientEvents');
			for(var i=0; i<slotObjects.length; i++){
				if(slotObjects[i].editable === false){
					slotObjects.splice(i, 1);
					i = i-1;
				}
			}
			$scope.otheravailabletimes = slotObjects;
			$scope.initCalendar();
      $scope.renderCalendarEvents();
			if($scope.candidatetimes){
				fullcalendarHelper.renderEvents(calendarElm, $scope.candidatetimes);
			}
			if($scope.otheravailabletimes){
				fullcalendarHelper.renderEvents(calendarElm, $scope.otheravailabletimes, true, false, '#FF7700');
			}
		}
  });
});
