onetoone.Controllers.controller('PreviewFilters',
function ($scope, $rootScope, _, config, scheduleAPI, calendarFilters,fancybox) {
    //
    calendarFilters.initScope($scope);
		$scope.expand=false;
		$scope.org_uri=null;
		$scope.org_id=null;
		$scope.org_types=[];
		$scope.org_roots=[];
		$scope.type_id=null;
		$scope.root_id=null;
		$scope.loaded=false;

		$scope.initTypeData=function(org_id,org_uri){
			$scope.org_id=org_id;
			$scope.org_uri=org_uri;
			scheduleAPI.getOrgTypes($scope.org_uri)
				.success(function(response){
					if(response.data.org_types){
						$scope.org_types=response.data.org_types;
						if($scope.org_types.length==1){
							$scope.type_id=$scope.org_types[0].id;
						}
					}
					if(response.data.org_roots){
						$scope.org_roots=response.data.org_roots;
						if($scope.org_roots.length==1){
							$scope.root_id=$scope.org_roots[0].id;	
						}
					}
				});
		}
		$scope.expandfilters=function (){
			if($scope.expand){
				$scope.expand=false;
			}else{
				if($scope.type_id==null||$scope.root_id==null){
					fancybox.open("#inline-modal-example", {padding: 15});
				}else{
					if($scope.loaded){
						$scope.expand=true;
					}else{
						$scope.showfilters();
					}
				}
			}
		}
		
		$scope.selecttype=function(){
			if($scope.type_id==null||$scope.root_id==null){
				//alert('Please select');
			}else{
				$scope.showfilters();
				fancybox.close();
			}
		}
		
		$scope.showfilters=function(){
			if($scope.filters.length<=0)
				$scope.getfilters($scope.org_id,$scope.root_id,$scope.type_id);
			$scope.expand=true;
		}
		
		/*$scope.getfilters=function(org_id,root_id,type_id){
			
		}*/
		
		$scope.hidefilters=function (){
			$scope.expand=false;
		}
		
    $scope.ageSlider = {
        filterIndex: 0,
        min: $scope.minAge,
        max: $scope.maxAge,
        config:{
            range: true,
            min: $scope.minAge,
            max: $scope.maxAge,
            values: [ 18, 100 ],
            slide: function (event, ui) {
                $scope.ageSlider.min = ui.values[ 0 ];
                $scope.ageSlider.max = ui.values[ 1 ];
                $scope.filters[$scope.ageSlider.filterIndex].values = [ui.values[ 0 ], ui.values[ 1 ]];
                $scope.$apply();
            },
            change: function (event, ui) {
                $scope.ageSlider.min = ui.values[ 0 ];
                $scope.ageSlider.max = ui.values[ 1 ];
                $scope.filters[$scope.ageSlider.filterIndex].values = [ui.values[ 0 ], ui.values[ 1 ]];
                $scope.activeFilters.age = angular.copy(ui.values);
                $scope.calculateMatches();
                $scope.safeApply();
            }
        }
    };

    /**
     * handles activeFiltersDirty update and populates changes to activeFilters
     * @param valueId
     */
    $scope.filterChanged = function (filterId, valueId) {
        if ($scope.activeFiltersDirty[valueId] === true) {
            // push this value to activeFilters
            pushValueToFilters(filterId, valueId);
            $scope.activeFiltersValues.push(valueId);
        } else {
            // remove this value from activeFilters
            excludeValueFromFilters(filterId, valueId);
            $scope.activeFiltersValues.remove(_.indexOf($scope.activeFiltersValues, valueId));
        }
        $scope.calculateMatches();
    };

    var pushValueToFilters = function (filterId, valueId) {
        if (!$scope.activeFilters[filterId]) {
            $scope.activeFilters[filterId] = [];
        }
        $scope.activeFilters[filterId].push(valueId);
    };

    var excludeValueFromFilters = function (filterId, valueId) {
        $scope.activeFilters[filterId].remove(_.indexOf($scope.activeFilters[filterId], valueId));
    };

    /**
     * contains properties like filterId|valueId
     * that are equal to number of matching candidates for this pair
     * @type {Object}
     */
    $scope.matchingCandidatesNumber = {};

    /**
     * updates $scope.matchingCandidatesNumber on filters changed
     * @param event
     * @return {Boolean}
     */
    $scope.calculateMatches = function () {
        angular.extend(
            $scope.matchingCandidatesNumber,
            calendarFilters.calculateMatches(
                $scope.filters,
                calendarFilters.getSelectedCandidatesGroupedByFilters($scope.filters, $scope.candidates, $scope.activeFilters)
            )
        );

        // filter candidates array to get only matching candidates
        $rootScope.matchingCandidates = calendarFilters.getSelectedCandidates($scope.activeFilters, $scope.candidates);

        return false;
    };

    /**
     * maps corresponding filterId, valueId to property in $scope.matchingCandidatesNumber
     * @param filterId
     * @param valueId
     * @return {*}
     */
    $scope.showMatchingCandidates = function (filterId, valueId) {
        return $scope.matchingCandidatesNumber[filterId + '|' + valueId] || 0;
    };
		
		$scope.showResults = function (){
			var sumvalue=0;
			sumvalue=$scope.matchingCandidatesNumber['age|0'] || 0;
			return sumvalue;
    };

    /**
     * call backend for JSON data on controller load
     * @param org_id
     */
    $scope.getfilters = function (org_id, root_id, type_id) {
        scheduleAPI.getPreviewMatchFilters(org_id, root_id, type_id)        
            .success(function (response) {
                if (response.filter_data){
                  $scope.filters = response.filter_data;
									$scope.loaded=true;
                }

                if (response.matching_candidates) {
                  $scope.candidates = response.matching_candidates;
                  //prepare data to be used with gender
                  angular.forEach($scope.candidates, function (candidate, id) {
                      $scope.candidates[id].filters_values.gender = candidate.gender;
                      $scope.candidates[id].values.push(candidate.gender);
                  });
                }
                //push fake gender filter
                $scope.filters.push(calendarFilters.getGenderFilter());
                $scope.ageSlider.filterIndex = $scope.filters.length;

                // push fake yob filter
                $scope.filters.push(calendarFilters.getAgeFilter($scope.ageSlider.min, $scope.ageSlider.max));
                $scope.activeFilters.age = [ $scope.ageSlider.min, $scope.ageSlider.max ];

                $scope.maxFilterValues = calendarFilters.maxFilterValues($scope.filters);

                //$scope.hasCalendar = true;
								$scope.calculateMatches();
                /*scheduleAPI.getSelectedFilters(num_filters)
                    .success(function (response) {
                        // populate active filters with values from the previous step
                        $scope.activeFilters = _(response).isArray() ? {} : response;

                        // walk thru values in activeFilters and set activeFiltersDirty properties to true
                        angular.forEach($scope.activeFilters, function (activeFilter) {
                            if (!activeFilter.length) return;

                            angular.forEach(activeFilter, function (valueId) {
                                $scope.activeFiltersDirty[valueId] = true;
                                $scope.activeFiltersValues.push(valueId);
                            });
                        });

                        // update numbers for matching filters
                        
                    });*/
            });
    };
});