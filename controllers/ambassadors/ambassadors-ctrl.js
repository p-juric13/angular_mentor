onetoone.Controllers.controller('ambassadors',
function ($scope, $filter, $http, fancybox, config, utils, ambassadorFunctions){
	$scope.title="Mentors";
	$scope.org_id = null;
	$scope.mentors = [];
	
	var msg_modal_options={
		padding:15,
		minHeight : 20,
		closeBtn:false,
		helpers:{
			overlay:{
				closeClick:false
			}
		}
	};

	$scope.message={
		subject:"",
		msg:"",
		to_id:0,
		to_type:2, //ambassador group
		from_id:0,
		from_type:50 //admin group
	}
	
	$scope.state = {
		searchTerm: null,
		pageSizesAvailable: [10, 15, 25, 50],
		filter: {
		    type: 'all',//all, invited, active, signup_started, ghost_mode, away, phone_verified
		    query: ''
		},
		pageSize: 10,
		totalAmbassadors: 0,
		currentPage: 0,
		numberOfPages: function(mentors) {
			return Math.ceil(mentors / $scope.state.pageSize) || 1;
		}
	};

	$scope.init = function (orgId,admin_id) {
        $scope.org_id = orgId;
		$scope.message.from_id=admin_id;
     	ambassadorFunctions.fetchAmbassadors(orgId)
			.success(function(result) {
				angular.extend($scope.mentors, result.mentors);
			});
    };

	$scope.availability={};
	$scope.activeMentees={};
	$scope.selectedMentor={};
	
	// $scope.filter={
	//     type: 'all',//all, invited, active, signup_started, ghost_mode, away, phone_verified
	//     query: ''
	// }
	
	$scope.opendropdown = function (mentor_id){
		var list=angular.element("#dropdownlist_"+mentor_id);
		angular.element(".js-datalist").not(list).addClass('hidden');
    	list.toggleClass('hidden');
		return false;
  	};
	
	$scope.setTypeFilter = function (event, value) {
	    if (event) event.preventDefault();
	    $scope.state.filter.type = value;
	    $scope.state.currentPage = 0;
			$scope.state.filter.query = "";
	};
	
	$scope.setFilterQuery = function (event){
		if (event) event.preventDefault();
		var value = angular.element("#search-term").val();
		$scope.state.filter.query = value;
		$scope.state.currentPage = 0;
	}

	$scope.isTypeFilterActive = function (value) {
	    return $scope.state.filter.type === value;
	};

	$scope.filterMentor = function (mentor){
	    var valid = true,
	    type = $scope.state.filter.type;
	    query = $scope.state.filter.query;

	    // check mentor type
	    if (type !== 'all') {
	      if (type === 'invited') {
	          valid = mentor.status == "Invited";
	      } else if (type === 'active') {
	          valid = mentor.status == "Active";
	      } else if (type === 'signup_started') {
	          valid = mentor.status == "Signup Started" ||  mentor.status == "Queue Tutorial Incomplete";
	      } else if (type === 'ghost_mode') {
	          valid = mentor.status == "Ghost Mode";
	      } else if (type === 'unavailable') {
	          valid = mentor.status == "Unavailable/Away";
	      } else if (type === 'phone_unverified') {
	          valid = mentor.status == "Phone Unverified";
	      } else if (type === 'inactive') {
	      	  valid = mentor.status == "Inactive";
	      }
	    }
		
    if (valid && query && query.length >= 1) {
      var jsonString = $filter('json')(_(mentor).values()).toLowerCase();
      valid = jsonString ? jsonString.search(query.toLowerCase()) > -1 : false;
    }

    return valid;
  };
	
	$scope.showMessagePopup = function (id) {
		$scope.message.to_id=id;
		$scope.message.subject="";
		$scope.message.msg="";
		angular.element('#message_subject').removeClass("error");
		angular.element('#message_msg').removeClass("error");
		fancybox.open("#message_popup");
	};
	$scope.send_message=function(){
		//angular.element('#availability').scope()
		var error=false;
		if($scope.message.subject==""){
			angular.element('#message_subject').addClass("error");
			error=true;
		}
		if($scope.message.msg==""){
			angular.element('#message_msg').addClass("error");
			error=true;
		}
		if(!error){
			$http(utils.httpParams({
	      url: config.ROOT_URL + config.URLS.MESSAGE.SEND_MESSAGE,
	      method: 'POST',
	      data: $scope.message
	    })).success(function (response) {
				if(response>0){
					fancybox.close();
				}else{
					alert('server error');
				}
			});
		}
	}
	//var availPopupScope = null;
	
	$scope.showAvailability = function (id) {
		if (!id) return;
    // cache dom query after the first call
    //availPopupScope = availPopupScope || angular.element('#availability').scope();
    $http(utils.httpParams({
      url: config.ROOT_URL + config.URLS.AMBASSADORS.GET_AVAILABILITY_DATA,
      method: 'GET',
      params: {
        id:id 
      }
    })).success(function (response) {
			$scope.availability=response;
			fancybox.open("#availability_popup");
		});
	};

	$scope.showActiveMentees = function (id,indexnumber) {
		if (!id) return;
    // cache dom query after the first call
    //availPopupScope = availPopupScope || angular.element('#availability').scope();
	    $http(utils.httpParams({
	      url: config.ROOT_URL + config.URLS.AMBASSADORS.GET_ACTIVE_MENTEES_DATA,
	      method: 'GET',
	      params: {
	        id:id 
	      }
	    })).success(function (response) { 
			var tempVar = _.where($scope.mentors, {id: id});
			angular.extend($scope.selectedMentor, tempVar[0]); 
			$scope.activeMentees = response;
			fancybox.open("#current_mentored");
		});
  };
	
	function closedropdown(){
		angular.element(".js-datalist").addClass('hidden');
		return false;
  	};
	
	function changemsgstatus(msg,status,modal_options){
		angular.element("#ajaxstatusmodal").html(msg);
		if(status=="success"){
			angular.element("#ajaxstatusmodal").css("color","green");
		}else if(status=="error"){
			angular.element("#ajaxstatusmodal").css("color","red");
		}else{
			angular.element("#ajaxstatusmodal").css("color","#404040");
		}
		fancybox.open("#ajaxstatusmodal",modal_options);
	}
	//Resend Invitation
	$scope.resendInvitation=function(ambassador_id){
		if (!ambassador_id) return;
    // cache dom query after the first call
    //availPopupScope = availPopupScope || angular.element('#availability').scope();
		closedropdown();
		changemsgstatus("Sending ...","normal",msg_modal_options);
    $http(utils.httpParams({
      url: config.ROOT_URL + config.URLS.ADMINISTRATION.SEND_INVITE_REMINDER,
      method: 'GET',
      params: {
        ambassador_id:ambassador_id 
      }
    })).success(function (response) {
			if(response.result==1){
				changemsgstatus(response.msg,"success",msg_modal_options);
			}else{
				changemsgstatus(response.msg,"error",msg_modal_options);
			}
			setTimeout(fancybox.close,1000);
		}).error(function(data,status,headers,config){
			changemsgstatus(" Error! ","error",msg_modal_options);
			setTimeout(fancybox.close,1000);
		});
	}
	
	//Delete Ambassador
	$scope.deleteAmbassador=function(ambassador_id){
		if (!ambassador_id) return;
    // cache dom query after the first call
    //availPopupScope = availPopupScope || angular.element('#availability').scope();
		closedropdown();
		changemsgstatus("Deleting ...","normal",msg_modal_options);
    $http(utils.httpParams({
      url: config.ROOT_URL + config.URLS.ADMINISTRATION.DELETE_AMBASSADOR,
      method: 'GET',
      params: {
        ambassador_id:ambassador_id 
      }
    })).success(function (response) {
			if(response.result==1){
				changemsgstatus(response.msg,"success",msg_modal_options);
			}else{
				changemsgstatus(response.msg,"error",msg_modal_options);
			}
			angular.forEach($scope.mentors, function(value, key){
				if(value.id==ambassador_id){
					$scope.mentors.splice(key,1);
				}
			});
			//Process Delete
			
			setTimeout(fancybox.close,1000);
		}).error(function(data,status,headers,config){
			changemsgstatus(" Error! ","error",msg_modal_options);
			setTimeout(fancybox.close,1000);
		});
	}
	
	//send reminder Invitation
	$scope.sendProfileReminder=function(ambassador_id){
		if (!ambassador_id) return;
    // cache dom query after the first call
    //availPopupScope = availPopupScope || angular.element('#availability').scope();
		closedropdown();
		changemsgstatus("Sending ...","normal",msg_modal_options);
    $http(utils.httpParams({
      url: config.ROOT_URL + config.URLS.ADMINISTRATION.SEND_PROFILE_REMINDER,
      method: 'GET',
      params: {
        ambassador_id:ambassador_id 
      }
    })).success(function (response) {
			if(response.result==1){
				changemsgstatus(response.msg,"success",msg_modal_options);
			}else{
				changemsgstatus(response.msg,"error",msg_modal_options);
			}
			setTimeout(fancybox.close,1000);
		}).error(function(data,status,headers,config){
			changemsgstatus(" Error! ","error",msg_modal_options);
			setTimeout(fancybox.close,1000);
		});
	}
	
	//Make Ambassador Ghost mode
	$scope.ghostmode=function(ambassador_id){
		if (!ambassador_id) return;
    // cache dom query after the first call
    //availPopupScope = availPopupScope || angular.element('#availability').scope();
		closedropdown();
		changemsgstatus("Hide/Ghost ...","normal",msg_modal_options);
    $http(utils.httpParams({
      url: config.ROOT_URL + config.URLS.ADMINISTRATION.GHOST_MODE,
      method: 'GET',
      params: {
        ambassador_id:ambassador_id 
      }
    })).success(function (response) {
			if(response.result==1){
				changemsgstatus(response.msg,"success",msg_modal_options);
			}else{
				changemsgstatus(response.msg,"error",msg_modal_options);
			}
			angular.forEach($scope.mentors, function(value, key){
				if(value.id==ambassador_id){
					$scope.mentors[key].status='Ghost Mode';
					$scope.mentors[key].action=2;
				}
			});
			setTimeout(fancybox.close,1000);
		}).error(function(data,status,headers,config){
			changemsgstatus(" Error! ","error",msg_modal_options);
			setTimeout(fancybox.close,1000);
		});
	}
	//Re-activate Ambassador
	$scope.reactivate=function(ambassador_id){
		if (!ambassador_id) return;
    // cache dom query after the first call
    //availPopupScope = availPopupScope || angular.element('#availability').scope();
		closedropdown();
		changemsgstatus("Reactivating ...","normal",msg_modal_options);
    $http(utils.httpParams({
      url: config.ROOT_URL + config.URLS.ADMINISTRATION.REACTIVATE_AMBASSADOR,
      method: 'GET',
      params: {
        ambassador_id:ambassador_id 
      }
    })).success(function (response) {
			if(response.result==1){
				changemsgstatus(response.msg,"success",msg_modal_options);
			}else{
				changemsgstatus(response.msg,"error",msg_modal_options);
			}
			angular.forEach($scope.mentors, function(value, key){
				if(value.id==ambassador_id){
					$scope.mentors[key].status='Active';
					$scope.mentors[key].action=1;
				}
			});
			setTimeout(fancybox.close,1000);
		}).error(function(data,status,headers,config){
			changemsgstatus(" Error! ","error",msg_modal_options);
			setTimeout(fancybox.close,1000);
		});
	}
	
	
  $scope.nextPage = function() {
  	$scope.state.currentPage = $scope.state.currentPage + 1;
  };

  $scope.previousPage = function() {
  	$scope.state.currentPage = $scope.state.currentPage - 1;
  };

  $scope.isOnFirstPage = function() {
  	return $scope.state.currentPage === 0;
  }

  $scope.isOnLastPage = function () {
    return $scope.state.currentPage >= $scope.mentors.length / $scope.state.pageSize - 1;
  };
	
	$scope.getSummaryHTML = function(summary){
		var blocksarray = summary.split('|');
		var htmlstr="<ul class='blocks'>";
		angular.forEach(blocksarray,function(blockstr,id){
			var temp = blockstr.split(":");
			htmlstr += "<li><span class='block'>"+temp[0]+":</span><span>"+temp[1]+"</span></li>";
		});
		htmlstr += "</ul>";
		return htmlstr;
	};

});