onetoone.Controllers.controller('AccountSetup', function ($scope, config, accountjson) {
    $scope.state = {
        resetPasswordForm: false
    };

    // form values
    $scope.first_name='';
    $scope.last_name='';
    $scope.email='';
    $scope.gender='';
    $scope.phone_num='';
    $scope.zipcode='';
    $scope.yob='';
    $scope.sms_alerts='';
    $scope.user_id=0;
    $scope.org_uri='';
    $scope.vol_language_list=[];

    $scope.resetPassword = function (event) {
        if (event) event.preventDefault();
        $scope.state.resetPasswordForm = !$scope.state.resetPasswordForm;

        accountjson.inactivateLogin($scope.user_id)
            .success(function () {})
            .error(function () {});
    };

    // poshytip configuration for phone field
    var verifyPhoneNowTipHtml = angular.element('#verifyPhoneNow').children('.tipText').html();
    $scope.PhoneFieldPoshytipConfig = {
        content: function () {
            return verifyPhoneNowTipHtml;
        }
    };
});
