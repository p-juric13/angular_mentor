onetoone.Controllers.controller('notesCtrl',
function ($scope, $timeout, config, string, pendingServerInteractions, outreach ) {
    'use strict';
    	$scope.saveEnabled = 1;
		$scope.activeIndex = 0;
		$scope.notes = [];

		$scope.changeNote = function(index){
			// alert($scope.currentvalue);
			$scope.activeIndex = index;
		}

		$scope.addNew = function() {
			$scope.notes.unshift({
				title: '',
				content: ''
			});

			console.log($scope.notes);

			$scope.activeIndex = 0;
		}

		$scope.saveNote = function() {
			if($scope.saveEnabled) {
				$scope.saveEnabled = 0;
				outreach.saveNotes($scope.outreach.activeListId, $scope.notes)
					.success(function (response) {
						if(!response) { 
							alert("You cannot save an empty note.");
							return;
						}
						angular.extend($scope.notes, response); 
						// $scope.notes = response; 
						$scope.saveEnabled = 1;
					});
			}
		}
		/*function setnotesvalue(newValue, oldValue, scope){
			alert(newValue);
			/*var values = newvalue.toString().split("\n",2);
			$scope.notes[$scope.activeindex].title = values[0];
			$scope.notes[$scope.activeindex].content = values[1];
		}
		
		$scope.$watch($scope.currentvalue, setnotesvalue);*/

		$scope.deleteNote = function(noteId, index) {
			outreach.deleteNote(noteId)
				.success(function (response) {
					alert(index);
					console.log($scope.notes);
					if($scope.notes.length == 0) {
						$scope.notes = [
							{
								title: 'Enter Title',
								content: 'Enter Content'
							}
						];
						$scope.activeIndex = 0;
					}
					if($scope.activeIndex == index) { 
						$scope.activeIndex = 0;
					}
					$scope.notes.splice(index, 1);
				});
		}

		$scope.init = function () {
			$scope.notes = outreach.fetchNotes($scope.outreach.activeListId)
				.success(function (response) { 
					if(response) $scope.notes = response; 
					else $scope.notes = [
						{
							title: 'Enter Title',
							content: 'Enter Content'
						}
					]
				});
		}

		// fire init call when controller loadedx
	    $scope.init();
});
