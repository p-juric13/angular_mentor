onetoone.Controllers.controller('outreach',
function ($scope, $filter, $compile, outreach, $http, pusher, fancybox, notifications, config) {
	
	$scope.ambassadorId = 0;
	$scope.timezoneOffset = 0;
	$scope.currentAttempts = 0;
	$scope.recentAttempts = [];
	$scope.outreach = {
	  activeId: 0,
	  activeListId: 0,
	  filter: {
	      // date: 'today',//today, week, all
	      // type: 'all',//all, new, existing
	      query: ''
	  },
	  mentees: [],
	  // outreach pagination settings
	  currentPage: 0,
	  pageSize: 10,
	  pageSizesAvailable: [10, 15, 25, 50],
	  totalMentees: 0,
	  numberOfPages: function (mentees) {
	      return Math.ceil(mentees / $scope.outreach.pageSize) || 1;
	  },

	  mode: 'list', // list | call | call-details

	  breadcrumbs: []
	};

	$scope.connectionStatus = -1;

    $scope.info = {};

   // $scope.$watch("recentAttempts", function(newValue) {
   // 		console.log("changed");
   // 		$scope.info.call_attempts = newValue;
   // });

	/**
     * Sets breadcrumbs
     */
  $scope.setBreadcrumbs = function (breadcrumbs) {
      $scope.outreach.breadcrumbs = [{
          title: "Outreach List",
          action: $scope.gotoOutreachList
      }];

      if (breadcrumbs) {
          $scope.outreach.breadcrumbs = $scope.outreach.breadcrumbs.concat(breadcrumbs)
      }
  };

  /**
     * Pusher events
     */
    $scope.initPusher = function (channelId) {
      pusher.subscribe(channelId);

      // listen to phones functions
      pusher.bind('outreach/telephony', function (response) {

      	// if(response.msg_ind == 3) {
      	// 	$scope.connectionStatus = -1;
      	// 	console.log($scope.connectionStatus);
      	// 	$scope.$digest();
      	// }

        if (config.EVENT.hasOwnProperty(response.msg_ind)) {
          notifications.add(
            angular.extend(
              angular.copy(config.OUTREACH_EVENT[response.msg_ind]),
              response
            )
          );
        } else if(response.msg_ind == 99) {
        	outreach.getOutreachTx($scope.outreach.activeListId, 10)
        		.success( function (result) { 
        			$scope.info.call_attempts = result;
        			$scope.safeApply();
        		});
        } else {
          throw 'Options for event with id ' + response.msg_ind + ' not found';
        }


      });
    };
	
	
	/**
     * Used to back to outreach screen from other pages
     */
  $scope.gotoOutreachList = function () {
      $scope.setBreadcrumbs();
      $scope.outreach.mode = 'list';
  };
	
	// $scope.filterOptions = {
 //      date: [
	// 			{value:'today', label:'Today'},
	// 			{value:'week', label:'This Week'},
	// 			{value:'all', label:'All'},
	// 		],
 //      type:[
	// 			{value:'all', label:'All Mentees'},
	// 			{value:'new', label:'New Mentees'},
	// 			{value:'existing', label:'Existing Mentees'}
	// 		],
 //      selectedDate: "today",
 //      selectedType: "all"
 //  };
	
	// $scope.filtersdropdown = {
	// 	containerCssClass : 'queuedropdown',
	// };

	// $scope.setTypeFilter = function () {
 //      $scope.outreach.filter.type = $scope.filterOptions.selectedType;
 //  };
  
 //  $scope.setDateFilter = function () {
 //      $scope.outreach.filter.date = $scope.filterOptions.selectedDate;
 //  };
	
	/**
   * Goes through queue items table
   */
  $scope.nextPage = function () {
      $scope.outreach.currentPage = $scope.outreach.currentPage + 1;
  };

  /**
   * Goes through queue items table
   */
  $scope.previousPage = function () {
      $scope.outreach.currentPage = $scope.outreach.currentPage - 1;
  };

  /**
   * Flag function to check pagination state
   */
  $scope.isOnTheFirstPage = function () {
      return $scope.outreach.currentPage == 0;
  };

  /**
   * Flag function to check pagination state
   */
  $scope.isOnTheLastPage = function () {
      return $scope.outreach.currentPage >= $scope.outreach.mentees.length / $scope.outreach.pageSize - 1
  };

	
	
	// Init function 
   $scope.init = function (ambassadorId, timezoneOffset) {
		$scope.ambassadorId = ambassadorId;

		$scope.timezoneOffset = timezoneOffset;

		// response = { msg_ind: 2 };
		// notifications.add(
  //           angular.extend(
  //             angular.copy(config.OUTREACH_EVENT[3]),
  //             response
  //           )
  //         );
		
		// save user timezone offset to state
		outreach.getData(ambassadorId)
		      .success(function (response) {
		          if (_(response).isArray()) {
		              $scope.outreach.mentees = response;
		          }
							
		      });

		$scope.setBreadcrumbs();
		
		var w=angular.element(window);
		
		angular.element('.queue-main').css("min-height",w.height()-372);
		
	  // $('#dateOption').select2('val', 'today');

	  // console.log($('#dateOption').select2('val'));
	};
	
	$scope.filterOutreach = function (item) {
        var valid = true,
            query = $scope.outreach.filter.query;

        if (valid && query) {
            var jsonString = $filter('json')(_(item).values()).toLowerCase();
            valid = jsonString ? jsonString.search(query.toLowerCase()) > -1 : false;
        }

        return valid;
    };

    $scope.setFilterQuery = function (event){
		if (event) event.preventDefault();
		var value = angular.element("#search-term").val();
		$scope.outreach.filter.query = value;
		$scope.outreach.currentPage = 0;
	}

	$scope.openCall = function (menteeId, listId){
		$scope.outreach.activeId = menteeId;
		$scope.outreach.activeListId = listId;
		outreach.createInmemCall(listId)
			.success(function () {
				$scope.outreach.mode = 'call';
			});
	};
});