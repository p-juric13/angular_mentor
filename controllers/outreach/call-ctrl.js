onetoone.Controllers.controller('callCtrl',
function ($scope, $timeout, config, string, pusher, pendingServerInteractions, outreach, colorbox, fancybox, notifications) {
    'use strict';

    $scope.activePhoneNum = 1;
    
    // define and fire init
    $scope.init = function () {

	    outreach.callInfo($scope.outreach.activeId)
	        .success(function (response) {
	            angular.extend($scope.info, response);
	        });

        // $scope.setBreadcrumbs([{
        //     title: "Call Workbook",
        //     action: function () {
        //         $scope.openCall($scope.callParams.queueId);
        //     }}]);
    };

    $scope.initiateCall = function(phoneNumInd) {
    	$scope.activePhoneNum = phoneNumInd;
    	$scope.currentAttempts++;
    	if($scope.currentAttempts > 1) $scope.connectionStatus = -1;
    	outreach.initiateCall($scope.outreach.activeListId, phoneNumInd);
    	// console.log($scope.outreach);
    }

    // Functions related to notifcations checks
    $scope.connectedSuccessfully = function() {
    	outreach.updateOutreachTxStatus($scope.outreach.activeListId, 1)
    		.success(function (response) {
		    	$scope.connectionStatus = 1;
    		});
    }

    $scope.didNotConnect = function() {
    	outreach.incrementMissedCallBlock($scope.outreach.activeId, $scope.activePhoneNum, $scope.outreach.activeListId)
    		.success(function (response) {
		    	$scope.connectionStatus = 0;
    		});
    }

    $scope.markComplete = function() {
    	outreach.markComplete($scope.outreach.activeListId)
    		.success(function(response) {
    			notifications.add(
		          angular.extend(
		            angular.copy(config.OUTREACH_EVENT[5]),
		            {peer_coordinator_list_id: $scope.outreach.activeListId }
		          )
	          );


              // Remove the entry from the outreach listing
              $scope.outreach.mentees = _.filter($scope.outreach.mentees, function(a_mentee) { return a_mentee.list_id != $scope.outreach.activeListId });
            });
    }

    // fire init call when controller loadedx
    $scope.init();
});
