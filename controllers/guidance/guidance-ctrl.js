onetoone.Controllers.controller('guidance', 
function ($scope, $filter, $compile, guidance, $http, $rootScope, config, fancybox, $timeout) {

    $scope.ambassadorId = null;
		
    $scope.candidateRelevantContentSummary = [];

    $scope.guidance = {
        programHierarchyId: null,
        packetDataRow1: [],
        packetDataRow2: [],
        recommendedWidget: [],
        navigationData: [],
        mode: 'upcoming', // upcoming, subcategory_list, detailed_view
        navigationClass: 'active',
        activeCategory: null,
        activeCategoryName: null,
        activeMode: null,
        breadcrumbs: []
    };

    $scope.subcategory = {
        topics: [],
        activeSubcat: {
            'id': null,
            'name': ''
        },
        activeSubcatName: '',
        state: ''
    };

    $scope.content = {
        contentSummary: [], // {{title, description, content_type, content_type_id, status, content_status_id}}
        activeContentItem: {},
        activeContentDetails: []
    };

    $rootScope.catCompletePercent = 0;
    $rootScope.catCompleteWidth = 0;

    $scope.progressBarWidth = null;
    $scope.numComplete = null;

    /**
     * Initializes guidance environment
     *
     * @param {int} ambassadorId
     * @param {int} programHierarchyId
     */
    $scope.init = function (ambassadorId, programHierarchyId, prequeue_id, contentId) {
        $scope.ambassadorId = ambassadorId;
        $scope.guidance.programHierarchyId = programHierarchyId;

        if(contentId != 0) $scope.goToContentModule(contentId);

        guidance.getRecommendedContent(ambassadorId)
            .success(function (response) {
                var iterLength = response.length - 1;
                for(var x=0; x <= iterLength; x++) {
                    if(x <= 3) $scope.guidance.packetDataRow1.push(response[x]);
                    else $scope.guidance.packetDataRow2.push(response[x]);
                    if(x <= 4) $scope.guidance.recommendedWidget.push(response[x]);
                }
            });

        guidance.getNavigation(programHierarchyId)
            .success(function (response) {  
                $scope.guidance.navigationData = response;
            });

        $scope.setBreadcrumbs();
    };

    /**
     * Sets breadcrumbs
     */
    $scope.setBreadcrumbs = function (breadcrumbs) {
        $scope.guidance.breadcrumbs = [{
            title: "Guidance",
            action: $scope.gotoGuidanceHome
        }];

        if (breadcrumbs) {
            $scope.guidance.breadcrumbs = $scope.guidance.breadcrumbs.concat(breadcrumbs);
        }
    };

    /**
     * Used to back to queue screen from other pages
     */
    $scope.gotoGuidanceHome = function () {
        $scope.setBreadcrumbs();
        $scope.guidance.mode = 'upcoming';
    };

    /*$scope.formatDescription = function(goalsArr) {
        if(typeof goalsArr !== 'object') {
            return "<p>No goals have yet been set</p>";
        }

        goalsArrLength = goalsArr.length;

        return_var = '<p>';

        for (var i=0; i < goalsArrLength; i++) {
            return_var += "<span style='font-weight:bold'>" + goalsArr[i].category_name + "</span>- " + goalsArr[i].goal_name + ": " + goalsArr[i].goal_desc + "<br />";
        }
        return_var += "</p>";
        return return_var;
    };

    $scope.viewUpcomingContent = function(candidateProgramId,event) {
        if (event) event.preventDefault();

        guidance.getCandidateRelevantData(candidateProgramId, $scope.ambassadorId)
            .success(function (response) {
                console.log("success");
                $scope.candidateRelevantContentSummary = response;
                $scope.numberRelevantCompleted();
                fancybox.open('#materialmodal', {});
            });
    };*/

    $scope.switchMain = function(newMode, selectedCategory, categoryName) {
        if(!!newMode) $scope.guidance.activeMode = newMode;
        if(!!selectedCategory) $scope.guidance.activeCategory = selectedCategory;
        if(!!categoryName) $scope.guidance.activeCategoryName = categoryName;

        $scope.guidance.mode = $scope.guidance.activeMode;

        $scope.setBreadcrumbs([{
            title: $scope.guidance.activeCategoryName,
            action: $scope.switchMain
        }]);
    };

    /*$scope.determineClass = function(selectedCategory) {
        if(selectedCategory == $scope.guidance.activeCategory) return "selected"
        else return "active";
    };*/

    $scope.determineSubClass = function(selectedCategory) {
      if(selectedCategory == $scope.guidance.activeCategory) return "selected"
      else return "active";
    };
		
	/*$scope.determineClass = function(){
		if($scope.guidance.mode=='upcoming'){
			return "active";
		} else {
			return "";
		}
    };*/

    $scope.capitalizeFirstLetterType = function(type)
    {
        return type.charAt(0).toUpperCase() + type.slice(1);
    };

    $scope.numberCompleted = function() {
        var numComplete = _.countBy($scope.content.contentSummary, function(item) { return item.content_status == 1 ? 'complete' : 'incomplete'});
        if(!numComplete.complete) $scope.numComplete = 0;
        else $scope.numComplete = numComplete.complete;

        // Adjust progressbar width
        $scope.progressBarWidth = Math.round(365*($scope.numComplete/$scope.content.contentSummary.length))+1;
    };

    $scope.numberRelevantCompleted = function() {
        var numRelevantComplete = _.countBy($scope.candidateRelevantContentSummary, function(item) {return item.content_status == 1 ? 'complete' : 'incomplete'});
        if(!numRelevantComplete.complete) $scope.numRelevantComplete = 0;
        else $scope.numRelevantComplete = numRelevantComplete.complete;

        // Adjust progressbar width
        $scope.progressBarRelevantWidth = Math.round(365*($scope.numRelevantComplete/$scope.candidateRelevantContentSummary.length))+1;
    }

    $scope.updateContentStatus = function(contentStatusId, contentStatus) {
        if(contentStatusId === null) { 
            guidance.insContentStatus($scope.ambassadorId, $scope.content.activeContentItem.content_id, contentStatus)
                .success(function(response) {
                    $scope.content.activeContentItem.content_status_id = response;
                    // Update the existing contentSummary array with the updated values
                    angular.extend(_($scope.content.contentSummary).findWhere( { content_id: $scope.content.activeContentItem.content_id }), $scope.content.activeContentItem);
                    $scope.numberCompleted();
                    $scope.numberRelevantCompleted();
                });
        } else {
            guidance.updContentStatus(contentStatusId, contentStatus)
                .success(function () {
                    // Update the existing contentSummary array with the updated values
                    angular.extend(_($scope.content.contentSummary).findWhere( { content_id: $scope.content.activeContentItem.content_id }), $scope.content.activeContentItem);
                    $scope.numberCompleted();
                    $scope.numberRelevantCompleted();
                });
        }
        
    };

    $scope.determineClassContent = function(statusInd) {
        if(statusInd == 1) return "checked";
        return false;
    };


    
    $scope.updateContentSummaryList = function (subcatId, flipToDetailedView, contentTypeId, contentType) {
        guidance.getContentSummaryData(subcatId, $scope.ambassadorId)
            .success(function (response) {
                $scope.content.contentSummary = response;
                $scope.numberCompleted();
                if(flipToDetailedView == 1) $scope.presentDetailedView(contentTypeId, contentType);
            });
    };

    $scope.presentDetailedView = function(contentId, contentType, event) {
        if (event) event.preventDefault();

        $scope.guidance.mode = 'detailed_view';

        $scope.content.activeContentItem = _.find($scope.content.contentSummary, function(contentBit) { if(contentBit['content_type_id'] == contentId && contentBit['content_type'] == contentType) return contentBit});
        if($scope.content.activeContentItem.subcat_id && $scope.content.activeContentItem.subcat_id != $scope.subcategory.activeSubcat.id) $scope.updateContentSummaryList($scope.content.activeContentItem.subcat_id);

        // Pull the details for the selected content type and load on the stage
        guidance.getContentItemDetails(contentType, contentId)
            .success(function(response) {
                $scope.content.activeContentDetails = response;
            });

        if(!!$scope.guidance.activeCategoryName) {
            $scope.setBreadcrumbs([
                {title: $scope.guidance.activeCategoryName,
                action: $scope.switchMain
                },
                {title: $scope.content.activeContentItem.title,
                action: null}
            ]);
        } else {
            $scope.setBreadcrumbs([
                {title: $scope.content.activeContentItem.title,
                action: null}
            ]);
        }
    };

    $scope.presentDetailedViewDirect = function(contentObj, event) {
        if (event) event.preventDefault();
        $scope.guidance.mode = 'detailed_view';
        $scope.content.activeContentItem = contentObj;
        $scope.updateContentSummaryList(contentObj.subcat_id);
        $scope.subcategory.activeSubcat.id = contentObj.subcat_id;
        // Pull the details for the selected content type and load on the stage
        guidance.getContentItemDetails(contentObj.content_type, contentObj.content_type_id)
            .success(function(response) {
                $scope.content.activeContentDetails = response;
            });

        $scope.setBreadcrumbs([{
            title: $scope.content.activeContentItem.title,
            action: null
        }]);
    }

    $scope.goToContentModule = function(contentId) {
        guidance.getContentTypeSubcatId(contentId)
            .success(function (response) {
                var contentType = response.content_type,
                    subcatId = response.subcat_id,
                    contentTypeId = response.content_type_id;

                $scope.updateContentSummaryList(subcatId, 1, contentTypeId, contentType);
                // $scope.presentDetailedView(contentId, contentType);
            });
    };




});
