onetoone.Controllers.controller('subcatCtrl',
function ($scope, $filter, $compile, guidance, $http, $rootScope, config) {
    'use strict';

    $scope.$watch('guidance.activeCategory', function(newValue, oldValue) { 
        $scope.updateSubcategoryList(newValue);
    }, true);

    /**
     * Initializes subcategory environment
     *
     */
    $scope.init = function () {
        $scope.updateSubcategoryList($scope.guidance.activeCategory);
    };

    $scope.updateSubcategoryList = function (categoryId) {
        guidance.getSubcategoryData($scope.ambassadorId, categoryId)
            .success(function (response) {
                $scope.subcategory.topics = response;
                $scope.determineCatCompletePercent();
            });
    };

    $scope.viewContent = function(subcategoryId, subcategoryName, numContent, event) {
        if (event) event.preventDefault();
        if(numContent == 0) return;

        $scope.subcategory.activeSubcat.id = subcategoryId;
        $scope.subcategory.activeSubcat.name = subcategoryName;
    };

    $scope.determineSubcatClass = function(completedContent, numContent) {
        if (numContent > 0 && completedContent == numContent) return "checked";
    };

    $scope.determineSubcatProgress = function(completedContent, numContent) {
         if (numContent > 0) return (completedContent/numContent)*100 + 1;
         else return 0;
    };

    $scope.determineIcon = function() {
        if($scope.guidance.activeCategory == 1) return 'icon-fork-knife';
        else if($scope.guidance.activeCategory == 2) return 'icon-barbell';
        else if($scope.guidance.activeCategory == 3) return 'icon-syringe';
        else return 'icon-note-pad';
    };

    $scope.determineIconBg = function() {
        if($scope.guidance.activeCategory == 1) return ' bg-success';
        else if($scope.guidance.activeCategory == 2) return ' bg-lightred';
        else if($scope.guidance.activeCategory == 3) return '  bg-primary';
        else return ' bg-mustard';
    };

    $scope.determineCatCompletePercent = function() {
        var completed_content = _.pluck($scope.subcategory.topics, 'completed_content');
        var completed_total = 0;

        $.each(completed_content,function() {
            completed_total += parseInt(this);
        });

        var num_content = _.pluck($scope.subcategory.topics, 'num_content');
        var num_total = 0;
        $.each(num_content,function() {
            num_total += parseInt(this);
        });

        if(num_total > 0) { 
            $rootScope.catCompleteWidth = Math.round(365*(completed_total/num_total))+1;
            $rootScope.catCompletePercent = Math.round((completed_total/num_total)*100);

        }
        else { 
            $rootScope.catCompleteWidth = 0;
            $rootScope.catCompletePercent = 0;
        }
    }

    // fire init call when controller loaded
    $scope.init();

});
