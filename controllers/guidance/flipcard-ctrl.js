onetoone.Controllers.controller('flipcardCtrl', 
function ($scope, $filter, $compile, guidance, $http, fancybox, config) {
    'use strict';

    $scope.flipcard_set = [];
    
    // flipcard element
    $scope.currentFlipcard = {};

    $scope.flipcardProperties = {
        'flipped_ind': false,
        'currentFlipcardIndex': 0,
        'flipcardDeckSize': 0
    };

    $scope.$watch('content.activeContentDetails', function(newValue, oldValue) { 
        if(newValue != oldValue) $scope.init();
    }, true);

    /**
     * Initializes subcategory environment
     *
     */
    $scope.init = function () {
        guidance.getFlipcardData($scope.content.activeContentDetails.id) //$scope.content.activeContentDetails.id
            .success(function (response) {
                $scope.flipcard_set = response;
                $scope.flipcardProperties.flipcardDeckSize = $scope.flipcard_set.length;
                $scope.setFlipcardData();
                // console.log($scope.flipcard_set);
                $('.quickFlip').quickFlip();
                // angular.element('#flipcards-container').flip();
            });
    };

    $scope.flipCard = function() {
        //  angular.element('#flipcards-container').flip({
        //     direction:'tb',
        //     content:'this is my new content'
        // });
        $scope.flipcardProperties.flipped_ind = !$scope.flipcardProperties.flipped_ind;
        $('.quickFlip').quickFlipper();
    };

    $scope.nextCard = function(event) {
        event.preventDefault();
        if($scope.flipcardProperties.currentFlipcardIndex == $scope.flipcardProperties.flipcardDeckSize - 1) { 
            alert("You've reached the end of the deck. You can mark yourself complete below.");
            return;
        }
        if($scope.flipcardProperties.flipped_ind) $('.quickFlip').quickFlipper();
        $scope.flipcardProperties.currentFlipcardIndex++;
        $scope.setFlipcardData();
    };

    $scope.previousCard = function(event) {
        event.preventDefault();

        if($scope.flipcardProperties.currentFlipcardIndex == 0) return;

        if($scope.flipcardProperties.flipped_ind) $('.quickFlip').quickFlipper();
        $scope.flipcardProperties.currentFlipcardIndex--;
        $scope.setFlipcardData();
    };

    $scope.setFlipcardData = function() {
        if(!!$scope.flipcard_set[$scope.flipcardProperties.currentFlipcardIndex]) {
            $scope.currentFlipcard.front_title = $scope.flipcard_set[$scope.flipcardProperties.currentFlipcardIndex].flipcard_front_title;
            $scope.currentFlipcard.front_text = $scope.flipcard_set[$scope.flipcardProperties.currentFlipcardIndex].flipcard_front_text;
            $scope.currentFlipcard.back_title = $scope.flipcard_set[$scope.flipcardProperties.currentFlipcardIndex].flipcard_back_title;
            $scope.currentFlipcard.back_text = $scope.flipcard_set[$scope.flipcardProperties.currentFlipcardIndex].flipcard_back_text;
        }
    };

    // fire init call when controller loaded
    $scope.init();
});
