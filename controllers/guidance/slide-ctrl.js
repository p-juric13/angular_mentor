onetoone.Controllers.controller('slideCtrl', 
function ($scope, $filter, $compile, guidance, $http, fancybox, config) {
    'use strict';

    $scope.slide = [];
    

    // calendar element
    $scope.slideConfig = {
        'data': null,
        'templates': null
    }

    $scope.$watch('content.activeContentDetails', function(newValue, oldValue) { 
        // there is a value to save
        if (!newValue || !newValue.module_id) return false;

        // this is not an init change
        if (!_(oldValue).isObject()) return false;

        // change actually happened
        if (_(newValue).isEqual(oldValue)) return false;

        $scope.init();
    }, true);

    /**
     * Initializes subcategory environment
     *
     */
    $scope.init = function () {
        if(!$scope.content.activeContentDetails.module_id) return false;

        // console.log($scope.content.activeContentDetails);
        guidance.getSlideTemplates()
            .success(function (response) {
                $scope.template = response;
                // console.log($scope.template);
            });

        guidance.getSlideData($scope.content.activeContentDetails.module_id)
            .success(function (response) {
                $scope.slideConfig.data = response;
                guidance.getSlideTemplates()
                    .success(function (response2) {
                        $scope.slideConfig.templates = response2;
                        angular.element('#quiz-container').jquizzy($scope.slideConfig);
                        // $scope.$apply;
                    });
            });

        // $scope.slideConfig.data = $scope.slide;

        // var t = setTimeout(function(){console.log($scope.slideConfig.data)}, 10000);

        // jquizzyHelper.init(slideElm, $scope.slideConfig);
        // angular.element('#quiz-container').jquizzy($scope.slideConfig);
    };

    // $scope.fetchValues = function () {
    //     alert("testing");
    //     // $scope.ambassadorId = ambassadorId;
    //     // $scope.guidance.programHierarchyId = programHierarchyId;

    //     // guidance.getPacketData(ambassadorId)
    //     //     .success(function (response) {
    //     //         $scope.guidance.packetData = response;
    //     //     });

    //     // guidance.getNavigation(programHierarchyId)
    //     //     .success(function (response) {  
    //     //         $scope.guidance.navigationData = response;
    //     //     });
    // };

    // fire init call when controller loaded
    $scope.init();
});
