onetoone.Controllers.controller('contentCtrl',
function ($scope, $filter, $compile, guidance, $http, fancybox, config) {
    'use strict';

    $scope.$watch('subcategory.activeSubcat.id', function(newValue, oldValue) { 
        if(newValue !== null) $scope.updateContentSummaryList(newValue);
    }, true);

    $scope.numberCompleted = function() {
        var numComplete = _.countBy($scope.content.contentSummary, function(item) { return item.content_status == 1 ? 'complete' : 'incomplete'});
        if(!numComplete.complete) $scope.numComplete = 0;
        else $scope.numComplete = numComplete.complete;

        // Adjust progressbar width
        $scope.progressBarWidth = Math.round(365*($scope.numComplete/$scope.content.contentSummary.length))+1;
    };

});
