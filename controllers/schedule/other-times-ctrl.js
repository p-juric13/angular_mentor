onetoone.Controllers.controller('otherTimes', function ($scope, $rootScope, config, timezones, timeslots, fancybox) {
    'use strict';

    // $scope.day = "";
    // $scope.selected_start_time_hr_day = "";
    // $scope.selected_start_time_min_day = "";
    // $scope.selected_end_time_hr_day = "";
    // $scope.selected_end_time_min_day = "";
    // $scope.selected_time_am_pm_day = "";

    $scope.selected_day_obj = [];

    $scope.day_array = [
        'Sunday',
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday'
    ];

    $scope.time_hr_array = [
        '1',
        '2',
        '3',
        '4',
        '5',
        '6',
        '7',
        '8',
        '9',
        '10',
        '11',
        '12'
    ];

    $scope.time_min_array = [
        '00',
        '30'
    ];

    $scope.time_am_pm_array = [
        'AM',
        'PM'
    ];

    $scope.noMatchingTimes = function (event) {
        if (event) event.preventDefault();

        fancybox.open('#noMatchingTimes', {});
    };

     $scope.addDayTime = function (event) {
        event.preventDefault();

        if(!$scope.selected_day || !$scope.selected_start_time_hr_day || !$scope.selected_start_time_min_day || !$scope.selected_end_time_hr_day || !$scope.selected_end_time_min_day || !$scope.selected_time_am_pm_day || $scope.selected_day == "" || $scope.selected_start_time_hr_day == "" || $scope.selected_start_time_min_day == "" || $scope.selected_end_time_hr_day == "" || $scope.selected_end_time_min_day == "" || $scope.selected_time_am_pm_day == "") { 
            $scope.error_msg = "Please select a day, start time, and end time to add a time."
            return;
        }

        $scope.error_msg = null;

        $scope.selected_day_obj.push({
            day: $scope.selected_day,
            start_time_hr: $scope.selected_start_time_hr_day,
            start_time_min: $scope.selected_start_time_min_day,
            end_time_hr: $scope.selected_end_time_hr_day,
            end_time_min: $scope.selected_end_time_min_day, 
            am_pm: $scope.selected_time_am_pm_day           
        });

        // Clear input fields after push
        $scope.selected_day = "";
        $scope.selected_start_time_hr_day = "";
        $scope.selected_start_time_min_day = "";
        $scope.selected_end_time_hr_day = "";
        $scope.selected_end_time_min_day = "";
        $scope.selected_time_am_pm_day = "";

    };

    $scope.noMatchingTimesProcess = function (event) {
        if (event) event.preventDefault();

        var confirmation = window.confirm("Please confirm that the times are accurately listed.");

        if(confirmation == true) {
            // $scope.otherTime1 = $scope.selected_day1 + ' ' + $scope.selected_start_time_hr_day1 + ':' + $scope.selected_start_time_min_day1;
            $("#set_candidate_schedule_no_matches").submit();
            // $.fancybox.close();
        } else return;
    };

});