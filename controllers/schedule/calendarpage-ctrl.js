onetoone.Controllers.controller('CalendarPage', function ($scope, $rootScope, scheduleAPI, config, calendar, timezones, timeslots, fullcalendarHelper) {
    'use strict';
		
    // array of all timestamps
    //$scope.timeslots_slice = [];

    // calendar events object
	$scope.candidatetimes = null;
    $scope.otheravailabletimes = null;
		
	$scope.othertime = false;
	$scope.othertimeshowflag = false;
	$scope.changesubmode = function(){
		$scope.othertime = $scope.othertime?false:true;
		$scope.$broadcast ('changeothertime');
	}
});
