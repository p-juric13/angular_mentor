onetoone.Controllers.controller('scheduleCalendar', function ($scope, $rootScope, scheduleAPI, config, calendar, timezones, timeslots, fancybox, fullcalendarHelper) {
    'use strict';
		
    // object keyed by candidate_id containing array of timeslots_ids
    $scope.ambassador_availability = [];

    // array of all timestamps
    $scope.timeslots_slice = [];

    // calendar events object
    $scope.calendarEvents = [];

    // placeholder for result object to store all the data needed to be sent
    $scope.result = {
        dates: [],
        slots: [],
        ambassadors: [],
        filters: [],
        otherdates: [],
        otherslots: []
    };

    $scope.confirmed = false;

		
		
    // calendar element
    var calendarElm = angular.element('#calendar-angular');

    // calendar config object
    $scope.calendarConfig = {
        height: 810,
        axisFormat: 'h(:mm)TT',
        columnFormat: {
            week: 'dddd'
        },
        select: function (start, end, allDay, ev, overlayflag) {
          fullcalendarHelper.selectHandler.call(this, start, end, allDay, ev, overlayflag, calendarElm);
        },

        dayClick: function (date) {
			if($scope.othertime){
				fullcalendarHelper.otherAvailableTimeClickHandler.call(this, date, calendarElm, $scope.calendarEvents);
			}else{
				fullcalendarHelper.dayClickHandler.call(this, date, calendarElm, $scope.calendarEvents);
			}
        },

        eventDrop: function (event, dayDelta, minuteDelta, allDay, revertFunc) {
          fullcalendarHelper.eventDropHandler.call(this, event, dayDelta, minuteDelta, allDay, revertFunc, $scope.calendarEvents);
        },

        eventMouseover: function (event) {
          fullcalendarHelper.eventMouseoverHandler.call(this, event, calendarElm);
        },

        viewDisplay: function (view) {
          fullcalendarHelper.viewDisplay.call(this, calendarElm, $scope.calendarEvents);
        }
    };

    /**
     * initiates fullcalendar plugin
     */
    $scope.initCalendar = function () {
        //Default View Options
        var options = {};
        angular.extend(options, fullcalendarHelper.defaultOptions, $scope.calendarConfig);

        fullcalendarHelper.init(calendarElm, options);
    };

    /**
     * update method that is called on load and whenever the events array is changed.
     */
    $scope.updateCalendar = function update () {
        //extend the options to suite the custom directive.
    	var options = {};
    	angular.extend(options, fullcalendarHelper.defaultOptions, $scope.calendarConfig);
    	if($scope.othertime){
    		options.disableResizing = false;
    		options.eventColor='#FF7700';
    	}else{
    		options.disableResizing = true;
    		options.eventColor='#0E88AF';
    	}
        fullcalendarHelper.init(calendarElm, options);
    };

    $scope.renderCalendarEvents = function () {
        $scope.calendarEvents = calendar.generateEvents($scope.matchingCandidates, $scope.ambassador_availability, $scope.timeslots_slice);
        fullcalendarHelper.removeEvents(calendarElm);
        $scope.updateCalendar();
    };

    /**
     * candidates matched by currently selected filters
     */
    $scope.$watch('matchingCandidates', function (newValue) {
        if (!newValue) return;

        $scope.renderCalendarEvents();
    }, true);

    // obtains data from filder ctrl instance
    $scope.filtersCtrl = function () {
        var filtersCtrl = angular.element('#selfScheduleCalendarCtrl');
        if (!filtersCtrl.length) {
            filtersCtrl = angular.element('#scheduleFiltersCtrl');
        }
        return filtersCtrl.scope();
    };

    // form submit handler
    $scope.submitCalendar = function (event) {
        if (event) event.preventDefault();

        // if ($scope.confirmed === true) {
        //     // let the form be submitted
        //     angular.element('#set_candidate_schedule').trigger('submit');
        //     return true;
        // }

        // Retrieving calendar slots
        var slotObjects = calendarElm.fullCalendar('clientEvents');

        var matchingSlots = _.reject(slotObjects, function(a_slot) {
            return a_slot['otherslot'] == true;
        });

        if (matchingSlots.length < 1) {

            alert("Please give at least one overlapping timeslot.\n"
                + "This allows to make sure the mentor calls when he/she and the mentee are available.");
            return false;
        }

        $scope.result = calendar.populateSlotsAndAmbassadors(slotObjects, $scope.filtersCtrl().activeFilters);

        // Checking for overlapping slots
        // If any overlapping events were found, cancel form submission and display alert
        if (calendar.overlappingSlotsExist()) {
            if (event) event.preventDefault();
            alert("You cannot have any overlapping time slots");
            return false;
        }

        fancybox.open('#confirmBox', {});
    };

    $scope.noMatchingTimes = function (event) {
        if (event) event.preventDefault();

        fancybox.open('#noMatchingTimes', {});
    }

    $scope.processRequest = function (event) {
        if (event) event.preventDefault();
        // $scope.confirmed = true;
        $scope.submitCalendar();
    };

    $scope.resetCalendar = function (event) {
        if (event) event.preventDefault();
        fullcalendarHelper.reset(calendarElm);
    };

    var default_timeslots = [];

    /**
     * handles data initialisation
     */
    $scope.load = function (candidateId, typeId, orgId) {
				$scope.candidatetimes = null;
				$scope.otheravailabletimes = null;
				//$scope.othertime = false;
				$scope.othertimeshowflag = true;
				
        $rootScope.weekModeCalendarData = $rootScope.weekModeCalendarData || false;

        var successCallback = function (response) {
            $rootScope.weekModeCalendarData = response;
            $scope.ambassador_availability = response.ambassador_availability;

            $scope.timeslots_slice = response.timeslots_slice;
            default_timeslots = angular.copy(response.timeslots_slice);

            $scope.initCalendar();
            $scope.renderCalendarEvents();
						
        };

        if ($rootScope.weekModeCalendarData) {
            successCallback($rootScope.weekModeCalendarData);
        } else {
            scheduleAPI.getSchedulingData(candidateId, typeId, orgId).success(successCallback);
        }
    };

    /* --- timezones logic --- */
    // all the available timezones
    $scope.timezones = timezones.list();

    // get user utc offset
    $scope.userTzOffset = timezones.offset();

    $scope.$on('timezones.Ready', function () {
      // default timezone is -5
      $scope.timezone = timezones.getDefault();

      $scope.updateTimeslots();
      $scope.updateCalendar();
      $scope.renderCalendarEvents();
    });

    $scope.updateTimeslots = function () {
        $scope.timeslots_slice = timeslots.shift(default_timeslots, ($scope.timezone.offset - $scope.userTzOffset) * 3600000);
    };

    $scope.$watch('timezone', function (newValue, oldValue) {
        if (!newValue) return;
        $scope.updateTimeslots();
        $scope.renderCalendarEvents();
    });
		
		$scope.$on('changeothertime', function(e) {
			if($scope.othertime){
				var slotObjects = calendarElm.fullCalendar('clientEvents');
				for(var i=0; i<slotObjects.length; i++){
					if(slotObjects[i].editable === false){
						slotObjects.splice(i, 1);
						i = i-1;
					}
				}
				$scope.candidatetimes = slotObjects;
				$scope.updateCalendar();
                $scope.renderCalendarEvents();
				
				if($scope.otheravailabletimes){
					fullcalendarHelper.renderEvents(calendarElm, $scope.otheravailabletimes, true);
				}
				fullcalendarHelper.renderEvents(calendarElm, $scope.candidatetimes, false , false ,'#0E88AF');
			} else {
				var slotObjects = calendarElm.fullCalendar('clientEvents');
				for(var i=0; i<slotObjects.length; i++){
					if(slotObjects[i].editable === false){
						slotObjects.splice(i, 1);
						i = i-1;
					}
				}

				$scope.otheravailabletimes = slotObjects;
				$scope.updateCalendar();
                $scope.renderCalendarEvents();
				if($scope.candidatetimes) {
					fullcalendarHelper.renderEvents(calendarElm, $scope.candidatetimes);
				}
				if($scope.otheravailabletimes) {
					fullcalendarHelper.renderEvents(calendarElm, $scope.otheravailabletimes, true , false, '#FF7700');
				}
			}
    });
});
