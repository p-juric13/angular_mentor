onetoone.Directives.directive('resize', function ($window) {
	'use strict';
    return function (scope, element) {
        var w = angular.element($window);
        scope.getWindowDimensions = function () {
            return { 'h': w.height(), 'w': w.width() };
        };
        scope.$watch(scope.getWindowDimensions, function (newValue, oldValue) {
						var documentwidth = $(document).width() - 60;
						var maxnumber = Math.floor(documentwidth/250);
						maxnumber = maxnumber > 5? 5: maxnumber;
						$(element).css("max-width", maxnumber*250+"px");
        }, true);

        w.bind('resize', function () {
            scope.$apply();
        });
    }
});
onetoone.Directives.directive('filterHeight', ['$timeout', function ($timeout) {
	'use strict';
  return {
      link: function ($scope, element, attrs) {
          $scope.$on('FilterValueLoaded', function () {
              $timeout(function () { // You might need this timeout to be sure its run after DOM render.
								var maxheight = 0;
								var i=0;
								var documentwidth = $(document).width() - 60;
								var maxnumber = Math.floor(documentwidth/250);
								maxnumber = maxnumber > 5? 5: maxnumber;
								$(".da-form-row").each(function(){
									var heightvalue = parseInt($(this).height());
									if(maxheight < heightvalue){
										maxheight = heightvalue;
									}
									i++;
								});
								$(".da-form-row").css("height", (maxheight+20)+"px");
								$(".third-step-widget").css("max-width", maxnumber*250+"px");
								maxnumber = maxnumber > i? i:maxnumber;
								$(".third-step-widget").css("width", maxnumber*250+"px");
              }, 0, false);
          })
      }
  };
}]);
onetoone.Controllers.controller('scheduleFilters',
function ($scope, $rootScope, _, config, scheduleAPI, calendarFilters) {
    //
    calendarFilters.initScope($scope);

    /**
     * yob slider config
     * @type {Object}
     */
    $scope.ageSlider = {
        filterIndex: 0,
        min: $scope.minAge,
        max: $scope.maxAge,
        config: {
            range: true,
            min: $scope.minAge,
            max: $scope.maxAge,
            values: [ 18, 100 ],
            slide: function (event, ui) {
                $scope.ageSlider.min = ui.values[ 0 ];
                $scope.ageSlider.max = ui.values[ 1 ];
                $scope.filters[$scope.ageSlider.filterIndex].values = [ui.values[ 0 ], ui.values[ 1 ]];
                $scope.$apply();
            },
            change: function (event, ui) {
                $scope.ageSlider.min = ui.values[ 0 ];
                $scope.ageSlider.max = ui.values[ 1 ];
                $scope.filters[$scope.ageSlider.filterIndex].values = [ui.values[ 0 ], ui.values[ 1 ]];
                $scope.activeFilters.age = angular.copy(ui.values);
                $scope.calculateMatches();
                $scope.safeApply();
            }
        }
    };

    /**
     * handles activeFiltersDirty update and populates changes to activeFilters
     * @param valueId
     */
    $scope.filterChanged = function (filterId, valueId) {
        if ($scope.activeFiltersDirty[valueId] === true) {
            // push this value to activeFilters
            pushValueToFilters(filterId, valueId);
            $scope.activeFiltersValues.push(valueId);
        } else {
            // remove this value from activeFilters
            excludeValueFromFilters(filterId, valueId);
            $scope.activeFiltersValues.remove(_.indexOf($scope.activeFiltersValues, valueId));
        }
        $scope.calculateMatches();
    };

    var pushValueToFilters = function (filterId, valueId) {
        if (!$scope.activeFilters[filterId]) {
            $scope.activeFilters[filterId] = [];
        }
        $scope.activeFilters[filterId].push(valueId);
    };

    var excludeValueFromFilters = function (filterId, valueId) {
        $scope.activeFilters[filterId].remove(_.indexOf($scope.activeFilters[filterId], valueId));
    };

    /**
     * contains properties like filterId|valueId
     * that are equal to number of matching candidates for this pair
     * @type {Object}
     */
    $scope.matchingCandidatesNumber = {};

    /**
     * updates $scope.matchingCandidatesNumber on filters changed
     * @param event
     * @return {Boolean}
     */
    $scope.calculateMatches = function () {
        angular.extend(
            $scope.matchingCandidatesNumber,
            calendarFilters.calculateMatches(
                $scope.filters,
                calendarFilters.getSelectedCandidatesGroupedByFilters($scope.filters, $scope.candidates, $scope.activeFilters)
            )
        );

        // filter candidates array to get only matching candidates
        $rootScope.matchingCandidates = calendarFilters.getSelectedCandidates($scope.activeFilters, $scope.candidates);

        return false;
    };

    /**
     * maps corresponding filterId, valueId to property in $scope.matchingCandidatesNumber
     * @param filterId
     * @param valueId
     * @return {*}
     */
    $scope.showMatchingCandidates = function (filterId, valueId) {
        return $scope.matchingCandidatesNumber[filterId + '|' + valueId] || 0;
    };

    /**
     * call backend for JSON data on controller load
     * @param org_id
     * @param candidate_id
     */
    $scope.initData = function (org_id, candidate_id, root_id, type_id, num_filters) {
        scheduleAPI.getFiltersData(org_id, candidate_id, root_id, type_id, !!num_filters)        
            .success(function (response) {
                if (response.filter_data) {
                    $scope.filters = response.filter_data;
                }

                if (response.matching_candidates) {
                    $scope.candidates = response.matching_candidates;
                    //prepare data to be used with gender
                    angular.forEach($scope.candidates, function (candidate, id) {
                        $scope.candidates[id].filters_values.gender = candidate.gender;
                        $scope.candidates[id].values.push(candidate.gender);
                    });
                }

                //push fake gender filter
                $scope.filters.push(calendarFilters.getGenderFilter());
                $scope.ageSlider.filterIndex = $scope.filters.length;

                // push fake yob filter
                $scope.filters.push(calendarFilters.getAgeFilter($scope.ageSlider.min, $scope.ageSlider.max));
                $scope.activeFilters.age = [ $scope.ageSlider.min, $scope.ageSlider.max ];

                $scope.maxFilterValues = calendarFilters.maxFilterValues($scope.filters);
								$scope.$broadcast('FilterValueLoaded');
                $scope.hasCalendar = true;
                scheduleAPI.getSelectedFilters(num_filters, candidate_id)
                    .success(function (response) {
                        // populate active filters with values from the previous step
                        $scope.activeFilters = _(response).isArray() ? {} : response;

                        // walk thru values in activeFilters and set activeFiltersDirty properties to true
                        angular.forEach($scope.activeFilters, function (activeFilter) {
                            if (!activeFilter.length) return;

                            angular.forEach(activeFilter, function (valueId) {
                                $scope.activeFiltersDirty[valueId] = true;
                                $scope.activeFiltersValues.push(valueId);
                            });
                        });

                        // update numbers for matching filters
                        $scope.calculateMatches();
                    });
            });
    };
});