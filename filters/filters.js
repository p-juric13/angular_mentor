/* Filters */
onetoone.Filters
.filter('filterValueStars', function () {
    'use strict';
    return function (value) {
        var starTemplate = '<div class="star"></div>',
            stars = [];

        if (value.red_label >= 1) {
            stars.push('<div class="rating">');
            stars.push(starTemplate);
            stars.push('</div>');
        }


        if (value.yellow_label >= 1) {
            stars.push('<div class="rating">');
            stars.push(starTemplate);
            stars.push(starTemplate);
            stars.push('</div>');
        }


        if (value.green_label >= 1) {
            stars.push('<div class="rating">');
            stars.push(starTemplate);
            stars.push(starTemplate);
            stars.push(starTemplate);
            stars.push('</div>');
        }

        return stars.join(' ');
    }
})

/**
 * returns number -> length of passed array
 */
.filter('getArrayLength', function (_) {
    'use strict';
    return function (array) {
        return _.isArray(array) ? array.length : 0;
    }
})

/**
 * Creates string with html that should be displayed in tooltip for queue item history
 *
 * @return {string}
 */
.filter("callHistoryTooltipHtml", function (_, config) {
    'use strict';
    return function (contactHistory) {
        var html = "<div><ul>",
            callStatus;

        _(contactHistory).each(function (item) {
            callStatus = config.CALL_STATUSES[item.call_status];

            html += '<li class="' + callStatus.id + '">' +
                '<div class="header">' +
                    '<span class="date">' + moment(item.date).format('ddd, MMM D YYYY') + '</span>' +
                    '<span class="status">' + callStatus.label + '</span>' +
                '</div>' +
                '<div class="content">' + item.note + '</div>' +
            '</li>';
        });

        html += "</ul></div>";

        return html;
    };
})

.filter('startFrom',function(){
	'use strict';
	return function (input, start) {
		start = +start;
		return input.slice(start);
	}
});