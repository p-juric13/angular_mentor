angular.module('Constants', [])
    .constant('config', {
        ROOT_URL: '/',

        URLS: {
            ACCOUNT: {
                ROOT: 'account/',
                SETUP: 'account_setup'
            },
            ACCOUNTJSON: {
                ROOT: 'accountjson/',
                INACTIVATE_LOGIN: 'inactivate_login'
            },
            TIMEZONES: 'getscheduledata/timezonesandoffset.json',
            SCHEDULE: {
                GET_FILTERS: 'getscheduledata/filteringdata.json',
                GET_SELECTED_FILTERS: 'getscheduledata/schedulingcriteria.json',
                GET_DATA: 'getscheduledata/schedulingdata.json',
                GET_TYPE_DATA: 'schedule/typejson/'
            },
            SELF_SCHEDULE: {
                GET_FILTERS: 'getscheduledata/selfschedulefilteringdata.json',
                GET_DATA: 'getscheduledata/selfschedulingdata.json'
            },
            QUEUE: {
                GET_DATA: 'getqueuedata/candidatedata/',
                INITIATE_CALL: 'queuetelephony/initiate_call_queue/',
                CALL_INFO: 'getqueuedata/queuecalldetails.json',
                CALL_DETAILS: 'getqueuedata/queueviewdetails.json',
                GET_HISTORY: 'getqueuedata/historydetails.json',
                GET_TRANSACTIONS: 'getqueuedata/transactiondetails.json',
                GET_PROGRAM_TOPICS: 'getqueuedata/programtopicslist.json',
                GET_TALKINGPOINTS_QUESTIONS: 'getqueuedata/talkingpointsquestions.json',
                CHECK_CALL_LENGTH: 'getqueuedata/checkcalllengths.json',
                SAVE: 'queuefunctions/log_call_info',
                SAVE_RESCHEDULED_CALL: 'queuefunctions/reschedule_this_call',
                SCHEDULE_NEW_CALL: 'queuefunctions/schedule_next_call',
                MARK_CALL_COMPLETE: 'queuefunctions/mark_call_complete',
                GET_MENTEE_AVAILABILITY: 'getqueuedata/menteeavailability.json',
                ACTIONS: {
                    ROOT: 'queuefunctions/',
                    NEXT_BEST_TIME: 'next_best_time/:queue_id',
                    CONNECTED_CALL: 'connected_call/:queue_id',
                    SUCCESSFUL_CALL: 'successful_call/:queue_id',
                    RESCHEDULE_CALL: 'reschedule_call/:queue_id',
                    RESCHEDULE_CALL_BLOCK: 'reschedule_call_block',
                    SCHEDULE_NEXT_CALL_BLOCK: 'schedule_next_call_block',
                    REQUEUE_CALL: 'requeue_call/:queue_id',
                    LOG_INCOMPLETE_CALL_TRANSACTION: 'log_incomplete_call_transaction/:queue_id',
                    CREATE_INMEM_CALL: 'create_inmem_call/:id',
                    SAVE_CANDIDATE_PHONE: 'save_candidate_phone'
                }
            },
			OUTREACH: {
				GET_DATA: 'getoutreachdata/prospective_mentees',
                GET_PROSPECTIVE_MENTEE_DATA: 'getoutreachdata/prospective_mentee_details',
                CREATE_INMEM_CALL: 'outreachfunctions/create_inmem_call',
                INITIATE_CALL: 'outreachtelephony/initiate_outreach_call/',
                INCREMENT_MISSED_ATTEMPT: 'outreachfunctions/increment_missed_attempt_block',
                FETCH_NOTES: 'getoutreachdata/notes_related_to_list',
                SAVE_NOTES: 'outreachfunctions/save_notes',
                DELETE_NOTE: 'outreachfunctions/delete_note',
                GET_OUTREACH_TX: 'getoutreachdata/outreach_attempts',
                UPDATE_OUTREACH_TX_STATUS: 'outreachfunctions/update_outreach_transaction_status',
                MARK_OUTREACH_COMPLETE: 'outreachfunctions/mark_outreach_complete',
			},
            MENTEESLIST: {
                GET_MENTEE_TASK_LIST: 'getmenteesdata/build_task_list',
                GET_MENTEE_NAMES: 'getmenteesdata/mentee_names',
                GET_MENTEE_CORE_INFO: 'getmenteesdata/mentee_core_info',
                GET_MENTEE_ACTION_PLAN: 'getmenteesdata/mentee_action_plans',
                GET_MENTEE_SUMMARY_CALLS: 'getmenteesdata/mentee_summary_calls',
                GET_MENTEE_SUMMARY_TX: 'getmenteesdata/mentee_summary_tx',
                GET_AT_A_GLANCE: 'getmenteesdata/at_a_glance',
                GET_ACTION_PLAN_INFO: 'getmenteesdata/action_plan_info',
            },
            GUIDANCE: {
                GET_PACKET_DATA: 'getguidancedata/packetdata',
                GET_NAVIGATION_DATA: 'getguidancedata/navigationdata',
                GET_CANDIDATERELEVANT_DATA: 'getguidancedata/candidaterelevanttopics',
                GET_SUBCATEGORY_DATA: 'getguidancedata/subcategorydata',
                GET_CONTENTSUMMARY_DATA: 'getguidancedata/contentsummarydata',
                GET_CONTENTDETAIL_DATA: 'getguidancedata/contentdetaildata',
                SET_CONTENTSTATUS: 'setguidancedata/setambassadorcontentstatus',
                GET_SLIDEMODULEDATA: 'getguidancedata/slidemoduledata',
                GET_SLIDETEMPLATEDATA: 'getguidancedata/slidetemplates',
                GET_FLIPCARDDATA: 'getguidancedata/flipcardata',
                GET_CONTENTTYPESUBCATID: 'getguidancedata/contenttypesubcatid',
                GET_RECOMMENDEDCONTENT: 'getguidancedata/recommendedcontent',
				FROMQUEUE : 'guidance/fromqueue'
            },
            AMBASSADORS: {
                AMBASSADORS_DATA: 'getambassadorsdata/ambassadorsdata/',
                GET_AVAILABILITY_DATA: 'administration/get_availability/',
                GET_ACTIVE_MENTEES_DATA: 'administration/get_active_mentees/'
            },
			CANDIDATES:{
					CANDIDATE_DATA: 'getcandidatedata/candidatesdata/',
                    CALL_HISTORY_DATA: 'getcandidatedata/callhistorydata/',
                    MARK_PROGRAM_COMPLETE: 'getcandidatedata/mark_program_complete/',
                    REMOVE_CANDIDATE_PROGRAM: 'getcandidatedata/remove_candidate_program/',
                    CANDIDATE_CORE_INFO: 'getcandidatedata/candidate_core_info',
                    CANDIDATE_CALL_DETAILS: 'getcandidatedata/callhistorydetails',
                    GET_NOTES: 'getcandidatedata/candidatenotes',
                    SAVE_NOTE: 'setcandidatedata/save_note',
                    SAVE_CANDIDATE_INFO: 'setcandidatedata/save_candidate_info',
                    CANDIDATE_AVAILABILITY: 'getcandidatedata/mentee_availability',
                    SAVE_CANDIDATE_AVAILABILITY: 'setcandidatedata/save_candidate_availability',
			},
            MESSAGE:{
                SEND_MESSAGE: 'accountjson/send_message_new',
            },
            ADMINISTRATION:{
                SEND_INVITE_REMINDER: 'administration/send_invite_reminder_json/',
                DELETE_AMBASSADOR: 'administration/delete_ambassador_json/',
                SEND_PROFILE_REMINDER: 'administration/send_profile_reminder_json/',
                GHOST_MODE:'administration/ghost_mode_json/',
                REACTIVATE_AMBASSADOR:'administration/reactivate_ambassador_json/',
            },
            GOALS: {
                ROOT: "queuefunctions/",
                OVERVIEW: "goals_overview",
                ADD_STEP: "add_step",
                REMOVE_STEP: "remove_step",
                SAVE_STEP: "save_step_data",
                FETCH_ACTION_PLAN: "fetch_action_plan",
                SAVE_ACTION_PLAN: "save_action_plan",
                REMOVE_ACTION_PLAN: "remove_action_plan",
                UPDATE_PRIORITIES: "set_goals_priorities"
            }
        },

        //
        EVENT: {
          1: {
            message: 'Dialing now…',
            forceOne: true
          },
          2: {
            message: 'Now calling the peer…',
            forceOne: true
          },
          3: {
            templateUrl: '/assets/partials/notifications/event-mentee-possible-no-answer.html',
            showClose: false,
            forceOne: true
          },
          4: {
            templateUrl: '/assets/partials/notifications/event-mentee-disconnected.html',
            showClose: false,
            forceOne: true
          },
          5: {
            severity: 'error',
            message: 'We lost you from the call.',
            forceOne: true
          },
          6: {
            message: 'We may have lost connection with the mentee. If you want to re-dial, just hit "2" on the call to re-dial.',
            forceOne: true
          },
          7: {
            templateUrl: '/assets/partials/notifications/event-call-less-than-three-minutes.html',
            showClose: false,
            forceOne: true
          },
          8: {
            message: 'The call was disconnected, but either you or the mentee has re-initiated.',
            // templateUrl: '/assets/partials/notifications/event-redialed-call.html',
            // showClose: false,
            forceOne: true
          },
          9: {
            templateUrl: '/assets/partials/notifications/event-mentee-call-ended.html',
            showClose: false,
            forceOne: true
            // message: 'The call has ended. Please fill in relevant notes and responses to questions before closing this box.',
            // forceOne: true
          },
          10: {
            message: 'We have both of you on the line again.',
            forceOne: true
          },
          11: {
            message: 'We have scheduled your next call. Be sure to fill in notes and answer the questions. Once you are done, hit the checkmark to complete the call',
            forceOne: true
          },
          12: {
            message: 'We have rescheduled this call. We will send you a reminder as the scheduled time draws nearer. For now, you can close this window.',
            forceOne: true
          },
          13: {
            message: 'We have updated the mentee\'s phone number. You can "Call Now" to dial out to him/her.',
            forceOne: true
          },
          14: {
            message: 'This was your last call with the mentee, so another call cannot be scheduled. Be sure to appropriately wrap things up with the mentee! We will e-mail you next steps.',
            forceOne: true
          },
          // 15: {
          //   message: 'You cannot schedule the next call without having spoken to the mentee. If you are looking to reschedule this call, please select "Reschedule This Call."',
          //   forceOne: true
          // },
          16: {
            message: 'We have scheduled your next call. Please note that your next call is the last call with this mentee before he/she graduates! Please prepare accordingly.',
            forceOne: true
          },
          // 17: {
          //   message: 'You have already scheduled another call. You cannot schedule the same call twice. If you are having issues, please contact yourfriends@1to1mentor.org.',
          //   forceOne: true
          // },
          18: {
            message: 'You have already scheduled the next call, and so cannot reschedule this call. If you are having issues, please contact yourfriends@1to1mentor.org.',
            forceOne: true
          },
          19: {
            message: 'You have already rescheduled this call. You cannot mark this call complete.',
            forceOne: true
            // templateUrl: '/assets/partials/notifications/event-call-rescheduled-call-notice.html',
            // showClose: true,
            // forceOne: true
          },
          20: {
            message: 'We had an issue in rescheduling this call. Please call or e-mail us and we will help resolve it. Thanks!',
            forceOne: true
          },
          21: {
            message: 'Be sure to go to "Call Snapshot" section of the workbook and reschedule the call for a later date and time.',
            forceOne: true
          }
        },

        OUTREACH_EVENT: {
          1: {
            message: 'Dialing now…',
            forceOne: true
          },
          2: {
            message: 'Now calling the prospective peer…',
            forceOne: true
          },
          3: {
            templateUrl: '/assets/partials/notifications/outreach/connected-with-prospective-mentee.html',
            showClose: false,
            forceOne: true
          },
          4: {
            message: 'Now calling the peer…',
            forceOne: true
          },
          5: {
            message: 'Thanks! You may now close this workbook.',
            forceOne: true
          },
        },

        STRING: {
          FILL_IN_CALL_SNAPSHOT: 'Please take a moment to capture relevant notes and answer the questions in the Call Snapshot.',
          CALL_LESS_THEN_3_MINS_NOT_SUCCESSFUL: 'Please take a moment to capture relevant notes and answer the questions in the Call Snapshot.'
        },

        DELAYED_TASKS_EXECUTION_TIMEOUT: 5000,

        FILTERS: {
            AGE: {
                MAX: 100,
                MIN: 18
            }
        },

        SCHEDULE: {
            ALLOWED_WEEKS_AHEAD: 2
        },

        TIMEZONES: [{
                is_default: 1,
                name: "America/New_York",
                offset: -5
            },
            {
                is_default: 0,
                name: "America/Chicago",
                offset: -6
            },
            {
                is_default: 0,
                name: "America/Boise",
                offset: -7
            },
            {
                is_default: 0,
                name: "America/Los_Angeles",
                offset: -8
            },
            {
                is_default: 0,
                name: "America/Anchorage",
                offset: -9
            },
            {
                is_default: 0,
                name: "Pacific/Honolulu",
                offset: -10
            }],

        STRING_DELIMITER: '$$',
        STRING_BLD_HD_DELIMITER: '$%$',
        STRING_SUB_HD_DELIMITER: '$@$',
        STRING_BULLET_DELIMITER: '$*$',
				STRING_HINT_DELIMITER: '$&$',

        SCHEDULE_HOURS: [
            { label: '08:00am', id: 1 },
            { label: '08:30am', id: 2 },
            { label: '09:00am', id: 3 },
            { label: '09:30am', id: 4 },
            { label: '10:00am', id: 5 },
            { label: '10:30am', id: 6 },
            { label: '11:00am', id: 7 },
            { label: '11:30am', id: 8 },
            { label: '12:00pm', id: 9 },
            { label: '12:30pm', id: 10 },
            { label: '01:00pm', id: 11 },
            { label: '01:30pm', id: 12 },
            { label: '02:00pm', id: 13 },
            { label: '02:30pm', id: 14 },
            { label: '03:00pm', id: 15 },
            { label: '03:30pm', id: 16 },
            { label: '04:00pm', id: 17 },
            { label: '04:30pm', id: 18 },
            { label: '05:00pm', id: 19 },
            { label: '05:30pm', id: 20 },
            { label: '06:00pm', id: 21 },
            { label: '06:30pm', id: 22 },
            { label: '07:00pm', id: 23 },
            { label: '07:30pm', id: 24 },
            { label: '08:00pm', id: 25 },
            { label: '08:30pm', id: 26 },
            { label: '09:00pm', id: 27 },
            { label: '09:30pm', id: 28 },
            { label: '10:00pm', id: 29 }
          ],

        NUMBER_OF_SCHEDULE_TIMESLOTS_TO_PICK_AT_ONCE: 4,
        NUMBER_OF_HISTORY_ITEMS_IN_CALL_POPUP: 3,

        PUSHER: {
          KEY: '311e6a3d066975a61d05'
        },

        CALL_STATUSES: {
            "-4": {
                id: "missed",
                label: "Missed"
            },
            "-3": {
                id: "mentee-dna",
                label: "Mentee DNA"
            },
            "-2": {
                id: "rescheduled",
                label: "Rescheduled"
            },
            "-1": {
                id: "cancelled",
                label: "Cancelled"
            },
            "0" : {
                id: "pending",
                label: "Pending Confirmation"
            },
            "1" : {
                id: "coming-up",
                label: "Coming up"
            },
            "2" : {
                id: "completed",
                label: "Completed"
            },
            "100" : {
                id: "rescheduled",
                label: "Deferred"
            },
            "200" : {
                id: "rescheduled",
                label: "Initiated"
            }
        },
        DEFAULT_RECORDNUM: 20
    });