/**
 * adds support for keyup event
 */
onetoone.Directives.directive('onKeyup', function () {
    'use strict';

    return function (scope, elm, attrs) {

        function applyKeyup () {
            scope.$apply(attrs.onKeyup);
        }

        var minlength = scope.$eval(attrs.ngMinlength),
            maxlength = scope.$eval(attrs.ngMaxlength),
            timer;

        elm.bind('keyup', function (evt) {
            //if no key restriction specified, always fire
            if ((!minlength || elm.val().length >= minlength)
                && (!maxlength || elm.val().length <= maxlength)
                ) {
                if (timer) clearTimeout(timer);
                timer = setTimeout(function () {
                    applyKeyup();
                }, 250);
            } else {
                //do nothing
            }
        });

    };

});