/**
 * binds function to blur event
 */
onetoone.Directives.directive('onBlur', function () {
    'use strict';

    return function (scope, elem, attrs) {
        elem.bind('blur', function () {
            scope.$apply(attrs.onBlur);
        });
    };
});