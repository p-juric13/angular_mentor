/**
 * shows loading notification in case there active $http requests in current scope
 */
onetoone.Directives.directive('resourceLoadingNotification', function factory ($http) {
    'use strict';

    return {
        template: '<div class="loading" style="margin-left:auto; margin-right:auto; width:100%" ng-hide="isLoadingAlertHidden()"><img src="assets/images/loaders/angular_loader.gif" alt="Loading..."></div>',
        replace: true,
        restrict: 'A',
        link: function postLink (scope, iElement, iAttrs) {
            scope.isLoadingAlertHidden = function () {
                return $http.pendingRequests.length === 0;
            };
        }
    };
});