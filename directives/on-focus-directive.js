/**
 * binds function to focus event fire
 * @todo need some precise tests
 */
onetoone.Directives.directive('onFocus', function ($timeout) {
    'use strict';

    return function (scope, elem, attrs) {
        elem.bind('focus', function () {
            scope.$apply(attrs.onFocus);

            $timeout(function () {
                elem[0].focus();
            }, 0, false);
        });
    };
});