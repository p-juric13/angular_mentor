/**
 * shows loading progress in case there active $http requests in current scope
 */
onetoone.Directives.directive('loadingProgress', function ($http) {
    'use strict';

    return {
        restrict: 'A',
        link: function (scope, iElement, iAttrs) {
						scope.isLoadingProgressHidden = function () {
                return $http.pendingRequests.length === 0;
            };
						scope.$watch(scope.isLoadingProgressHidden, function(value) {
								if(value){
									NProgress.done();
								}else{
									NProgress.start();
								}
						});
				}
    };
});