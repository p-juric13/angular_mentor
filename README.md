# /assets/js

This is root for js sources (as you can guess from name).

All the styles that are here already will be refactored to be included into compass project.

All the js files that exist out of this folder will be moved to this folder.

If you have library like chosen that includes styles, images and js sources - split it. CSS will be compiled into singe css by compass, all the images have to go to /assets/images folder, all js files should be placed here.

## . (root)

In directory root are only global site custom scripts placed with reusable components.

## /libs

Here go all the js _only_ dependencies (no styles and images).

Pattern to add new library is: /assets/js/libs/`library name`/`library version`/..

With multiple library versions it will be:

* /libs/jquery/1.8.0/

* /libs/jquery/1.9.0/

**ALL** the dependencies have to be structured that way. In a rare case when library has no version - place it in /libs/`library`/`1.0`/

## /angular-app

AngularJS application files (see directory README.md)

## /angular-app-tests

AngularJS application tests (see directory README.md)